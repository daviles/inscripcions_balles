<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EquiposBalles extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		//$this->load->helper('form');
		//$this->load->library('form_validation');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		/*$datos['login'] = 'null';
		$this->load->view('login', $datos);*/
	}

	public function equipsPerDivisio($idDivisio)
	{
		$this->load->model('equipos');

		$divisio = $this->equipos->getDadesDivisio($idDivisio);
		$equips = $this->equipos->getEquipsByidDivisio($idDivisio);
		$equipsPista = $this->equipos->getEquipsByidDivisioPista($idDivisio);	

		$datos['idDivisio'] = $divisio->idDivisio;
		$datos['nomDivisio'] = $divisio->nomDivisio;
		$datos['equipsPerDivisio'] = $equips;
		$datos['equipsPerDivsioPista'] = $equipsPista;

		$this->load->view('equipsPerDivisio', $datos);
	}

	public function mostraDadesEquip($idEquip)
	{
		//echo $idEquip;die();
		$this->load->model('equipos');
		$datos = array();
		$arrDadesEquip = $this->equipos->getDadesEquip($idEquip);
		$pagament = $this->equipos->getPagament($idEquip);
		$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($idEquip);
		$jugadors = $this->equipos->getDadesTriptic($idEquip);
		if ($jugadors){
			foreach ($jugadors as $jugador) {
				$edad = $this->calcularEdad($jugador->dataNeixament);
				$jugador->dataNeixament = $edad;
				if($jugador->isEntrenador == 1){
					$jugador->isEntrenador = 'Entrenador';
				}else{
					$jugador->isEntrenador = 'Jugador';
				}
				if($jugador->telefon == 0){
					$jugador->telefon = '-';
				}
			}
		}
		$datos['equip'] = $arrDadesEquip;
		$datos['idEquip'] = $idEquip;	
		$datos['pagament'] = $pagament;
		$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
		$datos['jugadors'] = $jugadors;

		$this->load->view('mostraDadesEquip', $datos);
	}

	public function afegirPagament($idEquip,$isPagat)
	{
		$this->load->model('equipos');

		//echo $idEquip;die();;
		$datos = array();
		$this->equipos->afegirPagament($idEquip,$isPagat);
		$pagament = $this->equipos->getPagament($idEquip);
		$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($idEquip);
		$arrDadesEquip = $this->equipos->getDadesEquip($idEquip);
		$jugadors = $this->equipos->getDadesTriptic($idEquip);
		foreach ($jugadors as $jugador) {
			$edad = $this->calcularEdad($jugador->dataNeixament);
			$jugador->dataNeixament = $edad;
			if($jugador->isEntrenador == 1){
				$jugador->isEntrenador = 'Entrenador';
			}else{
				$jugador->isEntrenador = 'Jugador';
			}
			if($jugador->telefon == 0){
				$jugador->telefon = '-';
			}
		}

		$datos['equip'] = $arrDadesEquip;
		$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
		$datos['pagament'] = $pagament;
		$datos['jugadors'] = $jugadors;
	
		$this->load->view('mostraDadesEquip', $datos);


	}

	public function calcularEdad ($fecha_nacimiento) {
	   $aFecha = explode( '/', $fecha_nacimiento);
		$edad = floor(( (date("Y") - $aFecha[2] ) * 372 + ( date("m") - $aFecha[1] ) * 31 + Date("d" ) - $aFecha[0] )/372) ;
		return $edad;
	}

	public function printTriptic($idEquip) {

		$this->load->model('equipos');
		$arrDadesJugador = array();
		$arrDadesEquip = array();
		$arrDadesJugador = $this->equipos->getDadesTriptic($idEquip);
		$arrDadesEquip = $this->equipos->getDadesEquip($idEquip);
		$equipAmbPistaJoc = $this->equipos->getIfEquipAmbPistaJoc($idEquip);

		$datos['arrDadesEquip'] = $arrDadesEquip;
		$datos['arrDadesJugador'] = $arrDadesJugador;
		$datos['equipAmbPistaJoc'] = $equipAmbPistaJoc;

		//echo "<pre>";print_r($arrDadesJugador);echo "</pre>";

		$this->load->view('printTriptic', $datos);

	}

	public function printRebut($idEquip) {
		$this->load->model('equipos');
		$arrDadesRebut = array();
		$arrDadesRebut = $this->equipos->getDadesRebut($idEquip);
		$idDivisio = $this->equipos->getIdDivsiobyIdEquip($idEquip);
		$pistaJoc = $this->equipos->getIdPistaJocbyIdEquip($idEquip);
		$preferencia = $this->equipos->getPreferenciaByIdEquip($idEquip);

		$datos['arrDadesRebut'] = $arrDadesRebut;
		$datos['idDivisio'] = $idDivisio;
		$datos['pistaJoc'] = $pistaJoc;
		$datos['idEquip'] = $idEquip;
		$datos['preferencia'] = $preferencia;
		
		$this->load->view('printRebut', $datos);

	}

	public function printRebutManual() {
		$this->load->model('equipos');
		$fitxes = $this->input->post('fitxa');
		$nomEquip = $this->input->post('nomEquip');
		$import = $this->input->post('import'); 

		$datos['fitxes'] = $fitxes;
		$datos['nomEquip'] = $nomEquip;
		$datos['import'] = $import;

		$this->load->view('printRebutManual', $datos);

	}

	public function delCaractersEspecials($string)
	{

	    //$string = trim($string);

	    $string = str_replace(
	        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
	        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
	        $string
	    );

	    $string = str_replace(
	        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
	        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
	        $string
	    );

	    $string = str_replace(
	        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
	        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
	        $string
	    );

	    $string = str_replace(
	        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
	        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
	        $string
	    );

	    $string = str_replace(
	        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
	        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
	        $string
	    );

	    $string = str_replace(
	        array('ñ', 'Ñ', 'ç', 'Ç'),
	        array('n', 'N', 'c', 'C',),
	        $string
	    );

	    //Esta parte se encarga de eliminar cualquier caracter extraño
	    $string = str_replace(
	        array("\\", "¨", "º", "-", "~",
	             "#", "@", "|", "!", "\"",
	             "·", "$", "%", "&", "/",
	             "(", ")", "?", "'", "¡",
	             "¿", "[", "^", "`", "]",
	             "+", "}", "{", "¨", "´",
	             ">", "< ", ";", ",", ":",
	             "."),
	        '',
	        $string
	    );


	    return $string;
	}

	public function comprobar_documento_identificacion($dni) {
		if(strlen($dni)<9) {
			$this->form_validation->set_message('comprobar_documento_identificacion', 'el %s no pot tenir menys de 9 dígits');
			return FALSE;
		}
	 
		$dni = strtoupper($dni);
	 
		$letra = substr($dni, -1, 1);
		$numero = substr($dni, 0, 8);
	 
		// Si es un NIE hay que cambiar la primera letra por 0, 1 ó 2 dependiendo de si es X, Y o Z.
		$numero = str_replace(array('X', 'Y', 'Z'), array(0, 1, 2), $numero);	
	 
		$modulo = $numero % 23;
		$letras_validas = "TRWAGMYFPDXBNJZSQVHLCKE";
		$letra_correcta = substr($letras_validas, $modulo, 1);
	 
		if($letra_correcta!=$letra) {
			$this->form_validation->set_message('comprobar_documento_identificacion', 'La lletra no és correcta');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	public function checkDateFormat($date) {
		/*if (($rawData = strptime($date, '%d/%m/%G')) !== false) {
		  	return true;
		} else {
		  	$this->form_validation->set_message('checkDateFormat', '2data no valida (dd/mm/YYYY)');
			return false;
		}*/
		if (preg_match("/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/", $date)) {
		if(checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4)))

			return true;
		else
			$this->form_validation->set_message('checkDateFormat', 'data no vàlida');
			return false;
		} else {
			$this->form_validation->set_message('checkDateFormat', 'forma de data no vàlida (dd/mm/YYYY)');
			return false;
		}
	} 

	public function crearJugador() {
			$this->session->unset_userdata('error');
			$this->session->unset_userdata('success');
			$this->load->model('equipos');
		//if($this->input->is_ajax_request()) {
			$this->load->library('upload');
			$arrDadesEquip = $this->equipos->getDadesEquip($_SESSION['USUARIO']['idEquip']);
			$pagament = $this->equipos->getPagament($_SESSION['USUARIO']['idEquip']);
			$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
			$jugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
			$jugadorsHistoric = $this->equipos->getDadesTripticHistoric($_SESSION['USUARIO']['idEquip']);
			if($jugadors){
				foreach ($jugadors as $jugador) {
					$edad = $this->calcularEdad($jugador->dataNeixament);
					$jugador->dataNeixament = $edad;
					if($jugador->isEntrenador == 1){
						$jugador->isEntrenador = 'Entrenador';
					}else{
						$jugador->isEntrenador = 'Jugador';
					}
					if($jugador->telefon == 0){
						$jugador->telefon = '-';
					}
				}
			}
			$datos['arrDadesEquip'] = $arrDadesEquip;
			$datos['pagament'] = $pagament;
			$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
			$datos['jugadors'] = $jugadors;
			$datos['jugadorsHistoric'] = $jugadorsHistoric;

		if($this->input->post())
        {

			//$this->form_validation->set_rules('dni', 'dni', 'required');
			$this->form_validation->set_rules('dni', 'dni', 'required|callback_comprobar_documento_identificacion');
			$this->form_validation->set_rules('nom', 'nom', 'required');
			$this->form_validation->set_rules('cognoms', 'cognoms', 'required');
			$this->form_validation->set_rules('email', 'email', 'required|valid_email');
			$this->form_validation->set_rules('telefon', 'telefon', 'required|numeric|min_length[9]|max_length[9]');
			$this->form_validation->set_rules('dataNeixament', 'dataNeixament', 'required|callback_checkDateFormat');
			//$this->form_validation->set_rules('dataNeixament', 'dataNeixament', 'required');
			$this->form_validation->set_rules('poblacio', 'poblacio', 'required');
			$this->form_validation->set_rules('dorsal','dorsal','numeric');
			//$this->form_validation->set_rules('fileUpload', 'fileUpload', 'required');
			//$this->form_validation->set_rules('fileUploadDNI', 'fileUploadDNI', 'required');

			$this->form_validation->set_message('required', 'camp %s obligatori');
			$this->form_validation->set_message('valid_email', 'No es un email valid');
			$this->form_validation->set_message('numeric', 'el %s nomes admet numeros');
			$this->form_validation->set_message('min_length', 'el %s ha de tenir 9 digits');
			$this->form_validation->set_message('max_length', 'el %s ha de tenir 9 digits');



			if($this->form_validation->run() === FALSE) {
				//echo "entra aqui";
				$data  = array (
					"dni" 				=> form_error("dni"),
					"nom" 				=> form_error("nom"),
					"cognoms" 			=> form_error("cognoms"),
					"email" 			=> form_error("email"),
					"telefon" 			=> form_error("telefon"),
					"dataNeixament" 	=> form_error("dataNeixament"),
					"poblacio" 			=> form_error("poblacio"),
					//"fileUpload" 		=> form_error("fileUpload"),
					//"fileUploadDNI" 	=> form_error("fileUploadDNI"),
					"res" 				=> "error"
				);
				$datos['reset'] = FALSE;
				$datos['res'] = "error";
				//echo "<pre>";print_r($data);echo "</pre>";
				
				$this->load->view('formInscripcio', $datos);
			} else {
				//echo "entra";
				$data  = array (
					"res" 		=> "success"
				);
				$datos['reset'] = TRUE;
				$datos['res'] = "success";
				$dni = strtoupper($this->input->post("dni"));
				$nom = strtoupper($this->input->post("nom"));
				$cognoms = strtoupper($this->input->post("cognoms"));
				$email = $this->input->post("email");
				$telefon = $this->input->post("telefon");
				$dorsal = $this->input->post("dorsal");
				$dataNeixament = $this->input->post("dataNeixament");
				$poblacio = strtoupper($this->input->post("poblacio"));
				$isEntrenador = $this->input->post("entrenador");
				$foto = $this->input->post('fileUpload');
				$dniAdj = $this->input->post('fileUploadDNI');
				$dataIntroduccio = date('Y-m-d H:i:s');
				$isPrimerCapita = 0;
				$isSegonCapita = 0;

				$nom = $this->delCaractersEspecials($nom);
				$cognoms = $this->delCaractersEspecials($cognoms);

					
						//echo "i es 1";die("entra");
						$config['upload_path'] = 'public/equipos/images/'.$_SESSION['USUARIO']['idEquip'].'/foto/';
					

				        $config['allowed_types'] = 'jpg|jpeg';
				        $config['max_size'] = '10000';
				        $config['max_width'] = '10024';
				        $config['max_height'] = '10008';
				 
				        $this->load->library('upload', $config);
				        $this->upload->initialize($config);
				        //SI LA IMAGEN FALLA AL SUBIR MOSTRAMOS EL ERROR EN LA VISTA UPLOAD_VIEW
				        if (!$this->upload->do_upload('fileUpload')) {
				        	//echo "entra en error";
				            $error = array('error' => $this->upload->display_errors());
				            $this->session->set_flashdata('error',$error['error']);

    						$this->load->view('formInscripcio',$datos);
    						return;
				            //$this->load->view('upload_view', $error);
				        } else {
				            $file_info = $this->upload->data();
				            //USAMOS LA FUNCIÓN create_thumbnail Y LE PASAMOS EL NOMBRE DE LA IMAGEN,
				            //ASÍ YA TENEMOS LA IMAGEN REDIMENSIONADA
				            //$this->_create_thumbnail($file_info['file_name']);
				           // $data = array('upload_data' => $this->upload->data());
				           // $titulo = $this->input->post('titulo');
				           
				           $foto = $config['upload_path'].$file_info['file_name'];
				           //$foto = $config['upload_path'].$dni;		            
				         
				        }

				        $config['upload_path'] = 'public/equipos/images/'.$_SESSION['USUARIO']['idEquip'].'/dni/';

				 
				        $this->load->library('upload', $config);
				        $this->upload->initialize($config);
				        
				        //SI LA IMAGEN FALLA AL SUBIR MOSTRAMOS EL ERROR EN LA VISTA UPLOAD_VIEW
				        if (!$this->upload->do_upload('fileUploadDNI')) {
				        	//echo "entra en error";
				            $error = array('error' => $this->upload->display_errors());
				            $this->session->set_flashdata('error',$error['error']);
    						$this->load->view('formInscripcio',$datos);
    						return;
				            //$this->load->view('upload_view', $error);
				        } else {
				        //EN OTRO CASO SUBIMOS LA IMAGEN, CREAMOS LA MINIATURA Y HACEMOS 
				        //ENVÍAMOS LOS DATOS AL MODELO PARA HACER LA INSERCIÓN
				            $file_info = $this->upload->data();
				            //USAMOS LA FUNCIÓN create_thumbnail Y LE PASAMOS EL NOMBRE DE LA IMAGEN,
				            //ASÍ YA TENEMOS LA IMAGEN REDIMENSIONADA
				            //$this->_create_thumbnail($file_info['file_name']);
				           // $data = array('upload_data' => $this->upload->data());
				           // $titulo = $this->input->post('titulo');
				           
				           $dniAdj = $config['upload_path'].$file_info['file_name'];
				           //$dniAdj = $config['upload_path'].$dni;			            
				         
				        }
				
			    $this->equipos->insertJugador($_SESSION['USUARIO']['idEquip'],$dni,$nom,$cognoms,$email,$dorsal,$dataNeixament,$poblacio,$telefon,$isPrimerCapita,$isSegonCapita,$isEntrenador,$foto,$dniAdj,$dataIntroduccio);
			    $this->equipos->setPagamentFalse($_SESSION['USUARIO']['idEquip']);
			    $this->session->set_flashdata('success', 'Jugador Inscrit correctament');
			    $pagament = $this->equipos->getPagament($_SESSION['USUARIO']['idEquip']);
				$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
				$jugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
				$jugadorsHistoric = $this->equipos->getDadesTripticHistoric($_SESSION['USUARIO']['idEquip']);
				$arrDadesEquip = $this->equipos->getDadesEquip($_SESSION['USUARIO']['idEquip']);
				foreach ($jugadors as $jugador) {
					$edad = $this->calcularEdad($jugador->dataNeixament);
					$jugador->dataNeixament = $edad;
					if($jugador->isEntrenador == 1){
						$jugador->isEntrenador = 'Entrenador';
					}else{
						$jugador->isEntrenador = 'Jugador';
					}
					if($jugador->telefon == 0){
						$jugador->telefon = '-';
					}
				}
				$datos['arrDadesEquip'] = $arrDadesEquip;
				$datos['pagament'] = $pagament;
				$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
				$datos['jugadors'] = $jugadors;
				$datos['jugadorsHistoric'] = $jugadorsHistoric;
				    //echo "OK".$foto."-".$dniAdj;

				$this->load->library('email'); // load email library
				$config['protocol'] = 'smtp';
				$config['smtp_host'] = 'smtp.basquetsabadell.com'; //change this
				$config['smtp_port'] = '587';
				//$config['smtp_crypto'] = 'tls';
				$config['smtp_user'] = 'laballes@basquetsabadell.com'; //change this
				$config['smtp_pass'] = 'Whvelico2007'; //change this
				$config['mailtype'] = 'html';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				$config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard

				$this->email->initialize($config);
			    $this->email->from('laballes@basquetsabadell.com', 'Inscripcions Balles');
			    $this->email->to('laballes@basquetsabadell.com');
			    //$this->email->cc('test2@gmail.com'); 
			    
			    $nomEquip = $this->equipos->getNomEquip();
			    $this->email->subject('Inscripcio nou jugador equip '.$nomEquip->nomEquip);
			    $this->email->message("L'equip : ".$_SESSION['USUARIO']['idEquip']." -> ".$nomEquip->nomEquip." ha inscrit un jugador en data : ".date('Y-m-d H:i:s'));
			    //$this->email->attach('/path/to/file1.png'); // attach file
			    //$this->email->attach('/path/to/file2.pdf');
			    if ($this->email->send()){
			        echo "Mail Sent!";
			  	} else {
			        show_error($this->email->print_debugger());
				}

				$this->load->view('redirect', $datos);
								//echo $dni." - ".$foto." - ".$dni;
					}
					
						//echo "todo ok!";

					/**/
			}

	}

	public function preferenciaEquipo(){
		$this->session->unset_userdata('error');
		$this->session->unset_userdata('success');
		$this->load->model('equipos');
		
		if($this->input->post())
        {
        	$this->load->model('equipos');
        	$preferencia = $this->input->post('preferencia');
        	$idEquip = $_SESSION['USUARIO']['idEquip'];
        	if($preferencia == ''){ $preferencia = NULL; }
        	$this->equipos->setPreferenciaHorari($preferencia, $idEquip);
        	$arrDadesEquip = $this->equipos->getDadesEquip($_SESSION['USUARIO']['idEquip']);
			$pagament = $this->equipos->getPagament($_SESSION['USUARIO']['idEquip']);
			$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
			$jugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
			$jugadorsHistoric = $this->equipos->getDadesTripticHistoric($_SESSION['USUARIO']['idEquip']);
			foreach ($jugadors as $jugador) {
				$edad = $this->calcularEdad($jugador->dataNeixament);
				$jugador->dataNeixament = $edad;
				if($jugador->isEntrenador == 1){
					$jugador->isEntrenador = 'Entrenador';
				}else{
					$jugador->isEntrenador = 'Jugador';
				}
				if($jugador->telefon == 0){
					$jugador->telefon = '-';
				}
			}
			$datos['arrDadesEquip'] = $arrDadesEquip;
			$datos['pagament'] = $pagament;
			$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
			$datos['jugadors'] = $jugadors;
			$datos['jugadorsHistoric'] = $jugadorsHistoric;
        	$datos['res'] = "success";
        	$datos['reset'] = FALSE;

        	$this->session->set_flashdata('success', 'Preferència Horária establerta');
        	$this->load->view('formInscripcio', $datos);
        }

	}

	public function datosEquipo() {

		$this->session->unset_userdata('error');
		$this->session->unset_userdata('success');
		$this->load->model('equipos');
		
		if($this->input->post())
        {
        	$this->load->model('equipos');
        	$divisio = $this->input->post('divisio');
        	$samarreta = $this->input->post('samarreta');
        	$pantalo = $this->input->post('pantalo');
        	$idEquip = $_SESSION['USUARIO']['idEquip'];
        	//if($preferencia == ''){ $preferencia = NULL; }
        	$this->equipos->setDadesEquip($divisio, $samarreta, $pantalo, $idEquip);
        	$arrDadesEquip = $this->equipos->getDadesEquip($_SESSION['USUARIO']['idEquip']);
			$pagament = $this->equipos->getPagament($_SESSION['USUARIO']['idEquip']);
			$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
			$jugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
			$jugadorsHistoric = $this->equipos->getDadesTripticHistoric($_SESSION['USUARIO']['idEquip']);
			foreach ($jugadors as $jugador) {
				$edad = $this->calcularEdad($jugador->dataNeixament);
				$jugador->dataNeixament = $edad;
				if($jugador->isEntrenador == 1){
					$jugador->isEntrenador = 'Entrenador';
				}else{
					$jugador->isEntrenador = 'Jugador';
				}
				if($jugador->telefon == 0){
					$jugador->telefon = '-';
				}
			}
			$datos['arrDadesEquip'] = $arrDadesEquip;
			$datos['pagament'] = $pagament;
			$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
			$datos['jugadors'] = $jugadors;
			$datos['jugadorsHistoric'] = $jugadorsHistoric;
        	$datos['res'] = "success";
        	$datos['reset'] = FALSE;

        	$this->session->set_flashdata('success', 'Dades Equip desades correctament');
        	$this->load->view('formInscripcio', $datos);
        }

	}

	public function datosEquipoPista() 
	{
		$this->session->unset_userdata('error');
		$this->session->unset_userdata('success');
		$this->load->model('equipos');	

		$arrDadesEquip = $this->equipos->getDadesEquip($_SESSION['USUARIO']['idEquip']);
		$pagament = $this->equipos->getPagament($_SESSION['USUARIO']['idEquip']);
		$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
		$jugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
		$jugadorsHistoric = $this->equipos->getDadesTripticHistoric($_SESSION['USUARIO']['idEquip']);
		if($jugadors){
			foreach ($jugadors as $jugador) {
				$edad = $this->calcularEdad($jugador->dataNeixament);
				$jugador->dataNeixament = $edad;
				if($jugador->isEntrenador == 1){
					$jugador->isEntrenador = 'Entrenador';
				}else{
					$jugador->isEntrenador = 'Jugador';
				}
				if($jugador->telefon == 0){
					$jugador->telefon = '-';
				}
			}
		}
		$datos['arrDadesEquip'] = $arrDadesEquip;
		$datos['pagament'] = $pagament;
		$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
		$datos['jugadors'] = $jugadors;
		$datos['jugadorsHistoric'] = $jugadorsHistoric;

		if($this->input->post()) {
        	$this->form_validation->set_rules('nomPista', 'nomPista', 'required');
	        $this->form_validation->set_rules('horaPista', 'horaPista', 'required');
	        $this->form_validation->set_rules('poblacioPista', 'poblacioPista', 'required');
	        $this->form_validation->set_rules('direccioPista', 'direccioPista', 'required');

	        $this->form_validation->set_message('required', 'camp %s obligatori');

	        if($this->form_validation->run() === FALSE) {
				//echo "entra aqui";
				$data  = array (
					"nomPista" 			=> form_error("nomPista"),
					"horaPista" 		=> form_error("horaPista"),
					"poblacioPista" 	=> form_error("poblacioPista"),
					"direccioPista" 	=> form_error("direccioPista"),
					"res" 				=> "error"
				);
				$datos['reset'] = FALSE;
				$datos['res'] = "error";
				//echo "<pre>";print_r($data);echo "</pre>";
				
				$this->load->view('formInscripcio', $datos);
			} else {
				$data  = array (
					"res" 		=> "success"
				);
				$datos['reset'] = TRUE;
				$datos['res'] = "success";
				$nomPista = strtoupper($this->input->post("nomPista"));
				$horaPista = strtoupper($this->input->post("horaPista"));
				$poblacioPista = strtoupper($this->input->post("poblacioPista"));
				$direccioPista = $this->input->post("direccioPista");
				
				$nomPista = $this->delCaractersEspecials($nomPista);
				$poblacioPista = $this->delCaractersEspecials($poblacioPista);
			    $this->equipos->insertDadesPistaJoc($_SESSION['USUARIO']['idEquip'],$nomPista,$horaPista,$poblacioPista,$direccioPista);
			    //$this->equipos->insertJugador($_SESSION['USUARIO']['idEquip'],$dni,$nom,$cognoms,$email,$dorsal,$dataNeixament,$poblacio,$telefon,$isPrimerCapita,$isSegonCapita,$isEntrenador,$foto,$dniAdj,$dataIntroduccio);
			    $this->session->set_flashdata('success', 'Pista desada correctament');
			    $pagament = $this->equipos->getPagament($_SESSION['USUARIO']['idEquip']);
				$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
				$jugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
				$jugadorsHistoric = $this->equipos->getDadesTripticHistoric($_SESSION['USUARIO']['idEquip']);
				$arrDadesEquip = $this->equipos->getDadesEquip($_SESSION['USUARIO']['idEquip']);
				
				foreach ($jugadors as $jugador) {
					$edad = $this->calcularEdad($jugador->dataNeixament);
					$jugador->dataNeixament = $edad;
					if($jugador->isEntrenador == 1){
						$jugador->isEntrenador = 'Entrenador';
					}else{
						$jugador->isEntrenador = 'Jugador';
					}
					if($jugador->telefon == 0){
						$jugador->telefon = '-';
					}
				}
				$datos['arrDadesEquip'] = $arrDadesEquip;
				$datos['pagament'] = $pagament;
				$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
				$datos['jugadors'] = $jugadors;
				$datos['jugadorsHistoric'] = $jugadorsHistoric;

				$this->load->view('formInscripcio', $datos);

			}

        }
	}

	public function establecerCapitanes(){

		$this->session->unset_userdata('error');
		$this->session->unset_userdata('errorCapita');
		$this->session->unset_userdata('success');
		$this->load->model('equipos');
		
		if($this->input->post())
        {
        	$ret = 'success';
        	$this->load->model('equipos');
        	$primer = $this->input->post('primer');
        	$segon = $this->input->post('segon');
        	if($primer == $segon){
        		$this->session->set_flashdata('errorCapita', 'Els dos capitans no poden ser el mateix jugador');
        		$ret = 'error';
        	}
        	$idEquip = $_SESSION['USUARIO']['idEquip'];
        	//if($preferencia == ''){ $preferencia = NULL; }
        	if($ret != 'error'){
        		$this->equipos->setCapitans($primer, $segon, $idEquip);	
        	}
        	
        	$arrDadesEquip = $this->equipos->getDadesEquip($_SESSION['USUARIO']['idEquip']);
			$pagament = $this->equipos->getPagament($_SESSION['USUARIO']['idEquip']);
			$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
			$jugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
			$jugadorsHistoric = $this->equipos->getDadesTripticHistoric($_SESSION['USUARIO']['idEquip']);
			foreach ($jugadors as $jugador) {
				$edad = $this->calcularEdad($jugador->dataNeixament);
				$jugador->dataNeixament = $edad;
				if($jugador->isEntrenador == 1){
					$jugador->isEntrenador = 'Entrenador';
				}else{
					$jugador->isEntrenador = 'Jugador';
				}
				if($jugador->telefon == 0){
					$jugador->telefon = '-';
				}
			}
			$datos['arrDadesEquip'] = $arrDadesEquip;
			$datos['pagament'] = $pagament;
			$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
			$datos['jugadors'] = $jugadors;
			$datos['jugadorsHistoric'] = $jugadorsHistoric;
        	$datos['res'] = "success";
        	$datos['reset'] = FALSE;
        	if($primer != $segon){
        		$this->session->set_flashdata('success', 'Capitans desats correctament');
        	}
        	$this->load->view('formInscripcio', $datos);
        }

	}

	public function exportJugadorsExcel()
	{
		$this->load->helper('mysql_to_excel_helper');
		$this->load->model('equipos');
		$jugadors = $this->equipos->getJugadorsExportExcel();
		//to_excel($jugadors, "exportJugadors");
		$datos['registros'] = $jugadors;

		$this->load->view('exportJugadorsExcel', $datos);


	}

	public function addJugadorsTemporadaAnterior()
	{
		$this->session->unset_userdata('error');
		$this->session->unset_userdata('success');
		$this->load->model('equipos');
		$arrDadesEquip = $this->equipos->getDadesEquip($_SESSION['USUARIO']['idEquip']);
		$pagament = $this->equipos->getPagament($_SESSION['USUARIO']['idEquip']);
		$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
		$jugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
		$jugadorsHistoric = $this->equipos->getDadesTripticHistoric($_SESSION['USUARIO']['idEquip']);
		if($jugadors){
			foreach ($jugadors as $jugador) {
				$edad = $this->calcularEdad($jugador->dataNeixament);
				$jugador->dataNeixament = $edad;
				if($jugador->isEntrenador == 1){
					$jugador->isEntrenador = 'Entrenador';
				}else{
					$jugador->isEntrenador = 'Jugador';
				}
				if($jugador->telefon == 0){
					$jugador->telefon = '-';
				}
			}
		}
		$datos['reset'] = TRUE;
		$datos['res'] = "success";
		$datos['arrDadesEquip'] = $arrDadesEquip;
		$datos['pagament'] = $pagament;
		$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
		$datos['jugadors'] = $jugadors;
		$datos['jugadorsHistoric'] = $jugadorsHistoric;
		$arrJugadorsH = $this->input->post("jugadorsH");
		$i = 0;
		if($arrJugadorsH) {
			foreach ($arrJugadorsH as $jug) {
				$this->equipos->addJugadorsTemporadaAnterior($jug);
				$i++;
			}
			$arrDadesEquip = $this->equipos->getDadesEquip($_SESSION['USUARIO']['idEquip']);
			$pagament = $this->equipos->getPagament($_SESSION['USUARIO']['idEquip']);
			$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
			$jugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
			$jugadorsHistoric = $this->equipos->getDadesTripticHistoric($_SESSION['USUARIO']['idEquip']);
			if($jugadors){
				foreach ($jugadors as $jugador) {
					$edad = $this->calcularEdad($jugador->dataNeixament);
					$jugador->dataNeixament = $edad;
					if($jugador->isEntrenador == 1){
						$jugador->isEntrenador = 'Entrenador';
					}else{
						$jugador->isEntrenador = 'Jugador';
					}
					if($jugador->telefon == 0){
						$jugador->telefon = '-';
					}
				}
			}
			$datos['reset'] = TRUE;
			$datos['res'] = "success";
			$datos['arrDadesEquip'] = $arrDadesEquip;
			$datos['pagament'] = $pagament;
			$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
			$datos['jugadors'] = $jugadors;
			$datos['jugadorsHistoric'] = $jugadorsHistoric;

			$this->load->library('email'); // load email library
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'smtp.basquetsabadell.com'; //change this
			$config['smtp_port'] = '587';
			//$config['smtp_crypto'] = 'tls';
			$config['smtp_user'] = 'laballes@basquetsabadell.com'; //change this
			$config['smtp_pass'] = 'Whvelico2007'; //change this
			$config['mailtype'] = 'html';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard

			$this->email->initialize($config);
		    $this->email->from('laballes@basquetsabadell.com', 'Inscripcions Balles Historic');
		    $this->email->to('laballes@basquetsabadell.com');
		    //$this->email->cc('test2@gmail.com'); 
		    
		    $nomEquip = $this->equipos->getNomEquip();
		    $this->email->subject('Inscripcio jugadors temporada anterior,  equip '.$nomEquip->nomEquip);
		    $this->email->message("L'equip : ".$_SESSION['USUARIO']['idEquip']." -> ".$nomEquip->nomEquip." ha inscrit ".$i." jugadors de la temporada anterior en data : ".date('Y-m-d H:i:s'));
		    //$this->email->attach('/path/to/file1.png'); // attach file
		    //$this->email->attach('/path/to/file2.pdf');
		    if ($this->email->send()){
		        //echo "Mail Sent!";
		  	} else {
		        show_error($this->email->print_debugger());
			}

			$this->session->set_flashdata('success', 'Afegits '.$i.' jugadors anteriors');

		}
							//echo $dni." - ".$foto." - ".$dni;
		$this->load->view('formInscripcio', $datos);
	}

	public function afegirObservacio($idEquip) {
		$this->session->unset_userdata('error');
		$this->session->unset_userdata('success');
		$this->load->model('equipos');
		
		if($this->input->post())
        {
        	$this->load->model('equipos');
        	$observacio = $this->delCaractersEspecials($this->input->post('observacio'));
        	if($observacio == ''){$observacio = 'cap';}
        	$this->equipos->addObservacio($idEquip,$observacio);
			$datos = array();
			$arrDadesEquip = $this->equipos->getDadesEquip($idEquip);
			$pagament = $this->equipos->getPagament($idEquip);
			$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($idEquip);
			$jugadors = $this->equipos->getDadesTriptic($idEquip);
			if ($jugadors){
				foreach ($jugadors as $jugador) {
					$edad = $this->calcularEdad($jugador->dataNeixament);
					$jugador->dataNeixament = $edad;
					if($jugador->isEntrenador == 1){
						$jugador->isEntrenador = 'Entrenador';
					}else{
						$jugador->isEntrenador = 'Jugador';
					}
					if($jugador->telefon == 0){
						$jugador->telefon = '-';
					}
				}
			}
			$datos['equip'] = $arrDadesEquip;
			$datos['idEquip'] = $idEquip;	
			$datos['pagament'] = $pagament;
			$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
			$datos['jugadors'] = $jugadors;

	        $datos['res'] = "success";
	        $datos['reset'] = FALSE;

	        $this->session->set_flashdata('success', 'Observacio afegida correctament');
	        $this->load->view('mostraDadesEquip', $datos);
        }	
	}

	public function canviarDivisio() {
		$this->session->unset_userdata('error');
		$this->session->unset_userdata('success');
		$this->load->model('equipos');

		if($this->input->post())
		{
			$idDivisio = $this->input->post('divisio');
			$idEquip = $this->input->post('idEquip');
			if ($idDivisio != '0' )
			{
				$this->equipos->updateDivisio($idEquip,$idDivisio);
			}
		}

		$this->mostraDadesEquip($idEquip);

	}

	public function exportarDadesEquipExcel($idDivisio){
		$this->load->helper('mysql_to_excel_helper');
		$this->load->model('equipos');
		$equips = $this->equipos->getEquipsPerDivisioExportExcel($idDivisio);
		//to_excel($jugadors, "exportJugadors");
		$datos['registros'] = $equips;
		$datos['idDivisio'] = $idDivisio;

		$this->load->view('exportDadesEquipsPerDivisio', $datos);

	}

	public function lastInscripcions(){
		$this->load->model('equipos');
		$jugadors = $this->equipos->getLastJugadors();

		$datos['jugadors'] = $jugadors;

		$this->load->view('lastInscripcions', $datos);
	}

	public function esportivitat(){
		$this->load->model('equipos');
		$equips = $this->equipos->getAllEquips();
		$esportivitat = $this->equipos->getEsportivitat();
		$esportivitatEquips = $this->equipos->getEsportivitatPerEquips();
		$datos['esportivitat'] = $esportivitat;
		$datos['esportivitatEquips'] = $esportivitatEquips;
		$datos['equips'] = $equips;

		$this->load->view('esportivitat', $datos);
	}

	public function llistatEsportivitatPerEquips() {
		$this->load->model('equipos');
		$this->load->model('equipos');
		$equips = $this->equipos->getAllEquips();
		$esportivitat = $this->equipos->getEsportivitat();

		$datos['esportivitat'] = $esportivitat;
		$datos['equips'] = $equips;

		$this->load->view('esportivitatPerEquips', $datos);

	}

	public function selectJugadorsEquip(){
		$this->load->model('equipos');
		$id = $this->input->post('id');

		$jugadors = $this->equipos->getJugadorsByIdEquip($id);
		
		//echo "<pre>";print_r($jugadors);
		foreach ($jugadors as $jugador) {
				$id=$jugador->idJugador;
				$data=$jugador->nomJugador." ".$jugador->cognomsJugador;
				echo '<option value="'.$id.'">'.$data.'</option>';
		}

	}

	public function sancionar() {
		$this->load->model('equipos');
		$idEquip = $this->input->post('equip');
		$idJugador = $this->input->post('jugador');
		$data = $this->input->post('dataPartit');
		$sancio = $this->input->post('sancio');
		$quantitat = $this->input->post('quantitat');
		$this->equipos->setSancio($idEquip,$idJugador,$data,$sancio,$quantitat);

		$equips = $this->equipos->getAllEquips();
		$esportivitat = $this->equipos->getEsportivitat();
		$esportivitatEquips = $this->equipos->getEsportivitatPerEquips();
		//echo "<pre>"; var_dump($esportivitatEquips);
		$datos['esportivitat'] = $esportivitat;
		$datos['esportivitatEquips'] = $esportivitatEquips;
		$datos['equips'] = $equips;

		$this->load->view('esportivitat', $datos);


	}

	public function insertTecnicaBanqueta() {
		$this->load->model('equipos');
		$idEquip = $this->input->post('equip');
		$data = $this->input->post('dataPartit');
		$quantitat = $this->input->post('quantitat');

		$this->equipos->setSancio($idEquip,null, $data,'banqueta',$quantitat);

		$equips = $this->equipos->getAllEquips();
		$esportivitat = $this->equipos->getEsportivitat();
		$esportivitatEquips = $this->equipos->getEsportivitatPerEquips();
		//echo "<pre>"; var_dump($esportivitatEquips);

		$datos['esportivitat'] = $esportivitat;
		$datos['esportivitatEquips'] = $esportivitatEquips;
		$datos['equips'] = $equips;

		$this->load->view('esportivitat', $datos);
	}

	public function veureFitxaJugadorEsportivitat($idJugador) {
		$this->load->model('equipos');

		$esportivitat = $this->equipos->getEsportivitatByIdJugador($idJugador);
		$jugador = $this->equipos->getInfoJugador($idJugador);
		$lineas = $this->equipos->getEsportivitatPerDates($idJugador);
		
		$datos['jugador'] = $jugador[0];
		$datos['esportivitat'] = $esportivitat[0];
		$datos['lineas'] = $lineas;

		$this->load->view('fitxaJugadorEsportivitat', $datos);

	}

	public function observacionsEsportivitat() {
		$this->load->model('equipos');

		$idJugador = $this->input->post('idJugador');
		$observacions = $this->input->post('observacions');

		$this->equipos->setObservacionsEsportivitat($idJugador, $observacions);

		$esportivitat = $this->equipos->getEsportivitatByIdJugador($idJugador);
		$jugador = $this->equipos->getInfoJugador($idJugador);
		$lineas = $this->equipos->getEsportivitatPerDates($idJugador);

		$datos['jugador'] = $jugador[0];
		$datos['esportivitat'] = $esportivitat[0];
		$datos['lineas'] = $lineas;

		$this->load->view('fitxaJugadorEsportivitat', $datos);
	}

	public function eliminarSancio($idEsportivitat, $idJugador) {
		$this->load->model('equipos');
		$this->equipos->eliminarSancio($idEsportivitat, $idJugador);

		$esportivitat = $this->equipos->getEsportivitatByIdJugador($idJugador);
		$jugador = $this->equipos->getInfoJugador($idJugador);
		$lineas = $this->equipos->getEsportivitatPerDates($idJugador);
		
		$datos['jugador'] = $jugador[0];
		$datos['esportivitat'] = $esportivitat[0];
		$datos['lineas'] = $lineas;

		$this->load->view('fitxaJugadorEsportivitat', $datos);

	}

	public function votacioAdmin() {
		$this->load->model('equipos');
		//$equips = $this->equipos->getAllEquips();
		//$esportivitat = $this->equipos->getEsportivitat();

		$votosJugadoresPrimera = $this->equipos->obtenerVotosJugadores(1);
		$votosJugadoresSegunda = $this->equipos->obtenerVotosJugadores(2);
		$votosJugadoresTercera = $this->equipos->obtenerVotosJugadores(5);
		$votosJugadoresPrimeraFemenina = $this->equipos->obtenerVotosJugadores(3);
		$votosJugadoresSegonaFemenina = $this->equipos->obtenerVotosJugadores(4);
		$divisions = $this->equipos->getDivisions();
		$datos['votosJugadoresPrimera'] = $votosJugadoresPrimera;
		$datos['votosJugadoresSegunda'] = $votosJugadoresSegunda;
		$datos['votosJugadoresTercera'] = $votosJugadoresTercera;
		$datos['votosJugadoresPrimeraFemenina'] = $votosJugadoresPrimeraFemenina;
		$datos['votosJugadoresSegonaFemenina'] = $votosJugadoresSegonaFemenina;
		$datos['divisions'] = $divisions;
		//$datos['equips'] = $equips;

		$this->load->view('votacioAdmin', $datos);
	}

	public function votacio() {

		$this->load->model('equipos');
		//$this->equipos->updatetable();die();		
		$idDivisio = $this->equipos->getIdDivsiobyIdEquip($_SESSION['USUARIO']['idEquip']);
		$divisio = $this->equipos->getDivisioByIdDivisio($idDivisio->idDivisio);
		$votosJugadores = $this->equipos->obtenerVotosJugadores($idDivisio->idDivisio);
		$numVotacio = $this->equipos->getAllVotacioByIdEquip($_SESSION['USUARIO']['idEquip']);
		$datos['idDivisio'] = $idDivisio;
		$datos['divisio'] = $divisio;
		$datos['votosJugadores'] = $votosJugadores;
		$datos['numVotacio'] = $numVotacio;
		
		//$datos['equips'] = $equips;

		$this->load->view('votacioEquip', $datos);
	}

	public function select_dependientes_proceso_equipo() {
		$this->load->model('equipos');
		$id = $this->input->post('id');
		$numEquip = $this->input->post('numEquip');

		$this->load->view('select_dependientes_proceso_equipo');
	}

	public function select_dependientes_proceso() {
		$this->load->model('equipos');
		/*$id = $this->input->post('id');
		$numEquip = $this->input->post('numEquip');*/

		$this->load->view('select_dependientes_proceso');
	}

	public function obtenerVotosJugadores($idDivisio){
		conecta();
		$dades = array();
		$queryJugadors = mysql_query("SELECT votacio.idJugador, votacio.votos, concat(jugador.nomJugador,' ',jugador.cognomsJugador) as jugador , equip.nomEquip
				from votacio
				inner join jugador on jugador.idJugador = votacio.idJugador
				inner join equip on equip.idEquip = jugador.idEquip
				WHERE jugador.actiu = 'ACT' and votacio.actiu = 'ACT'  and votacio.idDivisio = ".$idDivisio." order by votacio.votos DESC "  );
		while ($desti = mysql_fetch_assoc($queryJugadors)){
			array_push($dades, $desti);
		}
		//$row = mysql_fetch_assoc ( $queryJugadors );
		return $dades;
	}

	public function votacioAllstar() {
		$this->load->model('equipos');
		$idJugador = $this->input->post('jugadors');
		$idDivisio = $this->input->post('divisio');
	
		$vot = $this->equipos->checkIfJugadorHasVotacio($idJugador);
		//

		$numVotacio = $this->equipos->getAllVotacioByIdEquip($_SESSION['USUARIO']['idEquip']);
		if($vot)
		{
			$this->equipos->updateVotacioJugador($idJugador,$vot[0]->votos+1);
		}
		else
		{
			$this->equipos->createVotacioJugador($idJugador,$idDivisio);
		}
	
		if($_SESSION['USUARIO']['rol'] != 1)
		{
			if(!$numVotacio) /*|| $numVotacio[0]->numEquip == 0)*/{
				$this->equipos->updateVotsCapitaEquip($_SESSION['USUARIO']['idEquip'], 1, $idJugador, $idDivisio, $_SESSION['USUARIO']['idUser']);
			}else{
				if($numVotacio[0]->numRestaEquips== 0)
				{
					$this->equipos->updateVotsCapitaRestaEquip1($_SESSION['USUARIO']['idEquip'], $numVotacio[0]->numRestaEquips+1, $idJugador);
				}
				else if($numVotacio[0]->numRestaEquips == 1)
				{
					$this->equipos->updateVotsCapitaRestaEquip2($_SESSION['USUARIO']['idEquip'], $numVotacio[0]->numRestaEquips+1, $idJugador);
				}
				else if($numVotacio[0]->numRestaEquips == 2)
				{
					$this->equipos->updateVotsCapitaRestaEquip3($_SESSION['USUARIO']['idEquip'], $numVotacio[0]->numRestaEquips+1, $idJugador);
				}
				else if($numVotacio[0]->numRestaEquips == 3)
				{
					$this->equipos->updateVotsCapitaRestaEquip4($_SESSION['USUARIO']['idEquip'], $numVotacio[0]->numRestaEquips+1, $idJugador);
				}
				else if($numVotacio[0]->numRestaEquips == 4)
				{
					$this->equipos->updateVotsCapitaRestaEquip5($_SESSION['USUARIO']['idEquip'], $numVotacio[0]->numRestaEquips+1, $idJugador);
				}
				else if($numVotacio[0]->numRestaEquips == 5)
				{
					$this->equipos->updateVotsCapitaRestaEquip6($_SESSION['USUARIO']['idEquip'], $numVotacio[0]->numRestaEquips+1, $idJugador);
				}
				else if($numVotacio[0]->numRestaEquips == 6)
				{
					$this->equipos->updateVotsCapitaRestaEquip7($_SESSION['USUARIO']['idEquip'], $numVotacio[0]->numRestaEquips+1, $idJugador);
				}
				else if($numVotacio[0]->numRestaEquips == 7)
				{
					$this->equipos->updateVotsCapitaRestaEquip8($_SESSION['USUARIO']['idEquip'], $numVotacio[0]->numRestaEquips+1, $idJugador);
				}
				else if($numVotacio[0]->numRestaEquips == 8)
				{
					$this->equipos->updateVotsCapitaRestaEquip9($_SESSION['USUARIO']['idEquip'], $numVotacio[0]->numRestaEquips+1, $idJugador);
				}
			}
		}
		
		//$datos['equips'] = $equips;
		//afegirPagament($idEquip,$isPagat);
		if($_SESSION['USUARIO']['rol'] != 1)
		{
			$idDivisio = $this->equipos->getIdDivsiobyIdEquip($_SESSION['USUARIO']['idEquip']);
			$divisio = $this->equipos->getDivisioByIdDivisio($idDivisio->idDivisio);
			$votosJugadores = $this->equipos->obtenerVotosJugadores($idDivisio->idDivisio);
			$numVotacio = $this->equipos->getAllVotacioByIdEquip($_SESSION['USUARIO']['idEquip']);
			
			$datos['idDivisio'] = $idDivisio;
			$datos['divisio'] = $divisio;
			$datos['votosJugadores'] = $votosJugadores;
			$datos['numVotacio'] = $numVotacio;
			$this->load->view('votacioEquip', $datos);
			//header("location:../public/votacio.php");
		}
		else
		{
			$votosJugadoresPrimera = $this->equipos->obtenerVotosJugadores(1);
			$votosJugadoresSegunda = $this->equipos->obtenerVotosJugadores(2);
			$votosJugadoresTercera = $this->equipos->obtenerVotosJugadores(5);
			$votosJugadoresPrimeraFemenina = $this->equipos->obtenerVotosJugadores(3);
			$votosJugadoresSegonaFemenina = $this->equipos->obtenerVotosJugadores(4);
			$divisions = $this->equipos->getDivisions();
			$datos['votosJugadoresPrimera'] = $votosJugadoresPrimera;
			$datos['votosJugadoresSegunda'] = $votosJugadoresSegunda;
			$datos['votosJugadoresTercera'] = $votosJugadoresTercera;
			$datos['votosJugadoresPrimeraFemenina'] = $votosJugadoresPrimeraFemenina;
			$datos['votosJugadoresSegonaFemenina'] = $votosJugadoresSegonaFemenina;
			$datos['divisions'] = $divisions;
			$this->load->view('votacioAdmin', $datos);
			//header("location:../public/votacioAdmin.php");
		}
	}

	public function activateEquipTemporadaAnterior()
	{

		$this->load->model('equipos');
					
		$idEquip = $this->input->post("equip");
		
		$this->equipos->activateEquipTemporadaAnterior($idEquip);
		
		$this->load->view('redirect', $datos);
	}

	public function editJugador($idJugador) 
	{
		$this->load->model('equipos');

		$jugador = $this->equipos->getInfoJugador($idJugador);
		
		$datos['jugador'] = $jugador[0];

		$this->load->view('editJugador', $datos);
	}

	public function deleteJugador($idJugador,$idEquip) 
	{
		$this->load->model('equipos');

		$jugador = $this->equipos->deleteJugador($idJugador);
		
		$datos['jugador'] = $jugador[0];

		$this->mostraDadesEquip($idEquip);
	}

	public function editJugadorForm($idJugador)
	{
		$this->session->unset_userdata('error');
		$this->session->unset_userdata('success');
		$this->load->model('equipos');
		$dataNeixament = $this->input->post('dataNeixament');
		$email = $this->input->post('email');
		$dorsal = $this->input->post('dorsal'); 

		$jugador = $this->equipos->getInfoJugador($idJugador);

		if($jugador[0]->dataNeixament != $dataNeixament) {
			if($dataNeixament != '')
			{
				$this->equipos->updateDataNeixament($idJugador, $dataNeixament);
				$this->session->set_flashdata('success','Data de Neixament Actualitzada');
			}
			else
			{
				$this->session->set_flashdata('error','Data de Neixament no pot estar buida');
			}
		}

		if($jugador[0]->email != $email) {
			if($email != '')
			{
				$this->equipos->updateEmail($idJugador, $email);	
				$this->session->set_flashdata('success','Email actualitzat');
			}
			else
			{
				$this->session->set_flashdata('error','email no pot estar buit');
			}
		}

		if($jugador[0]->dorsal != $dorsal) {
			if($dorsal != '')
			{
				$this->equipos->updateDorsal($idJugador, $dorsal);
				$this->session->set_flashdata('success','Dorsal Actualitzat');
			}
			else
			{
				$this->session->set_flashdata('error','dorsal no pot estar buit');
			}
		}

		$this->editJugador($idJugador);


	}
}
