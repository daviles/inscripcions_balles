<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent :: __construct();
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$datos['login'] = 'null';
		$this->load->view('login', $datos);
	}

	public function crearPass(){
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$cad = "";
		for($i=0;$i<7;$i++) {
			$cad .= substr($str,rand(0,62),1);
		}
		return $cad;
	}

	public function calcularEdad ($fecha_nacimiento) {
	   $aFecha = explode( '/', $fecha_nacimiento);
		$edad = floor(( (date("Y") - $aFecha[2] ) * 372 + ( date("m") - $aFecha[1] ) * 31 + Date("d" ) - $aFecha[0] )/372) ;
		return $edad;
	}

	public function logout(){
	 	session_destroy();
	 	$parametros_cookies = session_get_cookie_params();
		setcookie(session_name(),0,1,$parametros_cookies["path"]);
	 	redirect(base_url());
	}

	public function loginUser($log = null)
	{
		$datos['res'] = '';
		$datos['reset'] = TRUE;
		$this->load->model('usuarios');
		$this->load->model('equipos');
		$this->load->model('arbitres');
		$passEquip = $this->crearPass();

		if($log != 1)
		{
			$user = $this->input->post('username');
			$pass = $this->input->post('password');
			
			$user = $this->usuarios->loginUsuario($user,$pass);
			
			if($user)
			{
				$_SESSION ['USUARIO'] = array (
						'idUser' => $user->idUser,
						'nomUser' => $user->nomUser,
						'idEquip' => $user->idEquip,
						'rol' => $user->rol,
						'idDivisio' => $this->equipos->getIdDivsiobyIdEquip($user->idEquip)->idDivisio
					);

				$equip = $this->equipos->getNomEquip();
				$nomEquip = $equip->nomEquip;
				$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
				$arrDadesEquip = $this->equipos->getDadesEquip($_SESSION['USUARIO']['idEquip']);
				$arrDadesJugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
				$numEquips = $this->equipos->getNumEquipsInscrits();
				$numJugadorsTotals = $this->equipos->getNumJugadorsTotals();
				$equiposBuscador = $this->equipos->getEquiposBuscador();
				$equipsTotal = $this->equipos->mostraEquips();
				$equipsInactius = $this->equipos->getEquipsNoActius();
				/*$pagament = $this->equipos->getPagament($_SESSION['USUARIO']['idEquip']);
				$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
				$jugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
				foreach ($jugadors as $jugador) {
					$edad = $this->calcularEdad($jugador->dataNeixament);
					$jugador->dataNeixament = $edad;
					if($jugador->isEntrenador == 1){
						$jugador->isEntrenador = 'Entrenador';
					}else{
						$jugador->isEntrenador = 'Jugador';
					}
					if($jugador->telefon == 0){
						$jugador->telefon = '-';
					}
				}
				$datos['pagament'] = $pagament;
				$datos['numJugadorsEquip'] = $numJugadorsEquip->num;*/
				//$datos['jugadors'] = $jugadors;

				$datos['login'] = 'correct';
				$datos['nomEquip']  = $nomEquip;
				$datos['numJugadorsEquip']  = $numJugadorsEquip->num;
				$datos['arrDadesEquip'] = $arrDadesEquip;
				$datos['arrDadesJugadors'] = $arrDadesJugadors;
				$datos['numEquips'] = $numEquips->num;
				$datos['numJugadorsTotals']  = $numJugadorsTotals->num;
				$datos['query']  = $equiposBuscador;
				$datos['equipsTotal'] = $equipsTotal;
				$datos['pass'] = $passEquip;
				$datos['equipsInactius'] = $equipsInactius;

	 			if ($user->rol == 1)
				{
					$this->load->view('adminBalles', $datos);
				}
				elseif ($user->rol == 2)
				{

					$pagament = $this->equipos->getPagament($_SESSION['USUARIO']['idEquip']);
					$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
					$jugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
					$jugadorsHistoric = $this->equipos->getDadesTripticHistoric($_SESSION['USUARIO']['idEquip']);
					if($jugadors){
						foreach ($jugadors as $jugador) {
							$edad = $this->calcularEdad($jugador->dataNeixament);
							$jugador->dataNeixament = $edad;
							if($jugador->isEntrenador == 1){
								$jugador->isEntrenador = 'Entrenador';
							}else{
								$jugador->isEntrenador = 'Jugador';
							}
							if($jugador->telefon == 0){
								$jugador->telefon = '-';
							}
						}
					}
					$datos['pagament'] = $pagament;
					$datos['numJugadorsEquip'] = $numJugadorsEquip->num;
					$datos['jugadors'] = $jugadors;
					$datos['jugadorsHistoric'] = $jugadorsHistoric;
					$this->load->view('formInscripcio', $datos);	
				}
				elseif ($user->rol == 3) {
                    $arbitre = $this->arbitres->getArbitreByIdUser($user->idUser);
                    $partits = $this->arbitres->showPartitsArbitre($arbitre[0]->idArbitre);
                    $delegats = $this->arbitres->showPartitsDelegat($arbitre[0]->idArbitre);
                    $datos['arbitre'] = $arbitre[0];
                    $datos['partits'] = $partits;
                    $datos['delegats'] = $delegats;
	 			    $this->load->view('adminArbitres', $datos);
                }
			}
			else
			{	
				//unset ( $_SESSION ['USUARIO'] );
				$datos['login'] = 'no_correct';
				$this->load->view('login', $datos);
			}
		}
		else
		{
			$equip = $this->equipos->getNomEquip();
			$nomEquip = $equip->nomEquip;
			$numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
			$arrDadesEquip = $this->equipos->getDadesEquip($_SESSION['USUARIO']['idEquip']);
			$arrDadesJugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
			$numEquips = $this->equipos->getNumEquipsInscrits();
			$numJugadorsTotals = $this->equipos->getNumJugadorsTotals();
			$equiposBuscador = $this->equipos->getEquiposBuscador();
			$equipsTotal = $this->equipos->mostraEquips();
			$equipsInactius = $this->equipos->getEquipsNoActius();

			$datos['login'] = 'correct';
			$datos['nomEquip']  = $nomEquip;
			$datos['numJugadorsEquip']  = $numJugadorsEquip->num;
			$datos['arrDadesEquip'] = $arrDadesEquip;
			$datos['arrDadesJugadors'] = $arrDadesJugadors;
			$datos['numEquips'] = $numEquips->num;
			$datos['numJugadorsTotals']  = $numJugadorsTotals->num;
			$datos['query']  = $equiposBuscador;
			$datos['equipsTotal'] = $equipsTotal;
			$datos['pass'] = $passEquip;
			$datos['equipsInactius'] = $equipsInactius;
 			if ($_SESSION['USUARIO']['rol'] == 1)
			{
				$this->load->view('adminBalles', $datos);
			}
			elseif ($_SESSION['USUARIO']['rol'] == 3)
            {
                $arbitre = $this->arbitres->getArbitreByIdUser($_SESSION['USUARIO']['idUser']);
                $partits = $this->arbitres->showPartitsArbitre($arbitre[0]->idArbitre);
                $delegats = $this->arbitres->showPartitsDelegat($arbitre[0]->idArbitre);
                $datos['arbitre'] = $arbitre[0];
                $datos['partits'] = $partits;
                $datos['delegats'] = $delegats;
                $this->load->view('adminArbitres', $datos);
			}
			else
            {
                $pagament = $this->equipos->getPagament($_SESSION['USUARIO']['idEquip']);
                $numJugadorsEquip = $this->equipos->getNumJugadorsEquip($_SESSION['USUARIO']['idEquip']);
                $jugadors = $this->equipos->getDadesTriptic($_SESSION['USUARIO']['idEquip']);
                $jugadorsHistoric = $this->equipos->getDadesTripticHistoric($_SESSION['USUARIO']['idEquip']);

                foreach ($jugadors as $jugador) {
                    $edad = $this->calcularEdad($jugador->dataNeixament);
                    $jugador->dataNeixament = $edad;
                    if($jugador->isEntrenador == 1){
                        $jugador->isEntrenador = 'Entrenador';
                    }else{
                        $jugador->isEntrenador = 'Jugador';
                    }
                    if($jugador->telefon == 0){
                        $jugador->telefon = '-';
                    }
                }
                $datos['pagament'] = $pagament;
                $datos['numJugadorsEquip'] = $numJugadorsEquip->num;
                $datos['jugadors'] = $jugadors;
                $datos['jugadorsHistoric'] = $jugadorsHistoric;
                $this->load->view('formInscripcio', $datos);
            }
		}


	}


	public function crearEquipo()
	{
		$this->load->model('equipos');
		  
		$nomEquip = $this->input->post('nomEquip');
		$usuari = $this->input->post('usuari');
		$pass = $this->input->post('pass');
		$this->equipos->crearEquipo($nomEquip,$usuari,$pass);
		$this->loginUser(1);
	}

	public function equipsPerDivisio($idDivisio)
	{
		echo $idDivisio;
	}
}
