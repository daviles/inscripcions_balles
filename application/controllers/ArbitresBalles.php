<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ArbitresBalles extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		//$this->load->helper('form');
		//$this->load->library('form_validation');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		/*$datos['login'] = 'null';
		$this->load->view('login', $datos);*/
	}

	public function delCaractersEspecials($string)
	{

	    //$string = trim($string);

	    $string = str_replace(
	        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
	        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
	        $string
	    );

	    $string = str_replace(
	        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
	        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
	        $string
	    );

	    $string = str_replace(
	        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
	        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
	        $string
	    );

	    $string = str_replace(
	        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
	        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
	        $string
	    );

	    $string = str_replace(
	        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
	        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
	        $string
	    );

	    $string = str_replace(
	        array('ñ', 'Ñ', 'ç', 'Ç'),
	        array('n', 'N', 'c', 'C',),
	        $string
	    );

	    //Esta parte se encarga de eliminar cualquier caracter extraño
	    $string = str_replace(
	        array("\\", "¨", "º", "-", "~",
	             "#", "@", "|", "!", "\"",
	             "·", "$", "%", "&", "/",
	             "(", ")", "?", "'", "¡",
	             "¿", "[", "^", "`", "]",
	             "+", "}", "{", "¨", "´",
	             ">", "< ", ";", ",", ":",
	             "."),
	        '',
	        $string
	    );


	    return $string;
	}

	public function crearPass(){
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$cad = "";
		for($i=0;$i<7;$i++) {
			$cad .= substr($str,rand(0,62),1);
		}
		return $cad;
	}

	public function showTarifes() {

		$this->load->model('arbitres');

		$tarifes = $this->arbitres->loadTarifes();

		$datos['tarifes'] = $tarifes;

		$this->load->view('llistatTarifes', $datos);
	}

	public function newTarifa() {
		
		$this->load->model('arbitres');

		if($this->input->post())
        {
        	$tipus = $this->input->post("tipus");
        	$price = $this->input->post("price");

        	$this->arbitres->insertTarifa($tipus,$price);
        }

        $tarifes = $this->arbitres->loadTarifes();

		$datos['tarifes'] = $tarifes;

		$this->load->view('llistatTarifes', $datos);

	}

	public function deleteTarifa($idTarifa) {

		$this->load->model('arbitres');

		$tarifes = $this->arbitres->deleteTarifa($idTarifa);

		$tarifes = $this->arbitres->loadTarifes();

		$datos['tarifes'] = $tarifes;

		$this->load->view('llistatTarifes', $datos);

	}

	public function llistatArbitres() {

		$this->load->model('arbitres');

		$arbitres = $this->arbitres->loadArbitres();

		$pass = $this->crearPass();

		$datos['arbitres'] = $arbitres;

		$datos['pass'] = $pass;


		$this->load->view('llistatArbitres', $datos);

	}

	public function newArbitre() {

		$this->load->model('arbitres');
		$this->load->model('usuarios');

		if($this->input->post())
        {

			$nom = strtoupper($this->input->post("nom"));
			$cognoms = strtoupper($this->input->post("cognoms"));
			$email = $this->input->post("email");
			$telefon = $this->input->post("telefon");
			$isArbitre = $this->input->post("arbitre");
			$isAnotador = $this->input->post("anotador");
			$dilluns = $this->input->post("dilluns");
			$dimarts = $this->input->post("dimarts");
			$dimecres = $this->input->post("dimecres");
			$dijous = $this->input->post("dijous");
			$divendres = $this->input->post("divendres");
			$dissabte = $this->input->post("dissabte");
			$diumenge = $this->input->post("diumenge");
			$newUser = $this->input->post('newUser');
			$user = $this->input->post('user');
			$pass = $this->input->post('pass');
			$observacions = $this->input->post("observacions");

			if ($newUser == '1') {
				$this->usuarios->createUser($user,$pass, '3');
				$idUser = $this->db->insert_id();//mysql_insert_id();
		
			}

			if($isArbitre == NULL){ $isArbitre = '0';}
			if($isAnotador == NULL){ $isAnotador = '0';}
			if($dilluns == NULL){ $dilluns = '0';}
			if($dimarts == NULL){ $dimarts = '0';}
			if($dimecres == NULL){ $dimecres = '0';}
			if($dijous == NULL){ $dijous = '0';}
			if($divendres == NULL){ $divendres = '0';}
			if($dissabte == NULL){ $dissabte = '0';}
			if($diumenge == NULL){ $diumenge = '0';}

			$user = $this->delCaractersEspecials($user);
			$observacions = $this->delCaractersEspecials($observacions);
			$this->arbitres->createArbitre($nom, $cognoms, $email, $telefon, $isArbitre, $isAnotador, $dilluns,$dimarts, $dimecres, $dijous, $divendres, $dissabte, $diumenge, $idUser, $observacions);

        }

        $this->llistatArbitres();
	}

	public function importarPartits() {
		
		$this->load->model('arbitres');
        $arbitres = $this->arbitres->getNomAbritres();
        $pistes = $this->arbitres->loadPistes();
       // var_dump($pistes);
		$partitsMasculins = $this->arbitres->loadPartitsMasculins();
		$partitsFemenins = $this->arbitres->loadPartitsFemenins();

		$franjaDelegats = $this->arbitres->loadFranjaDelegats();

		$datos['partits'] = $partitsMasculins;
		$datos['partitsFem'] = $partitsFemenins;
		$datos['franjaDelegats'] = $franjaDelegats;
		$datos['arbitres'] = $arbitres;
		$datos['pistes'] = $pistes;

		$this->load->view('importarPartits', $datos);
	}

	public function to_mysql()
  	{
        $this->session->unset_userdata('error');
        $this->session->unset_userdata('success');
         $this->load->helper("url");
        //obtenemos el archivo subido mediante el formulario
        $file = $_FILES['excel']['name'];
        //comprobamos si existe un directorio para subir el excel
        //si no es así, lo creamos
        if(!is_dir("./excel_files/"))
          mkdir("./excel_files/", 0777);

        //comprobamos si el archivo ha subido para poder utilizarlo
        if ($file && copy($_FILES['excel']['tmp_name'],"./excel_files/".$file))
        {

          //queremos obtener la extensión del archivo
          $trozos = explode(".", $file);

          //solo queremos archivos excel
          if($trozos[1] != "xlsx" && $trozos[1] != "xls") return;

          /** archivos necesarios */
          require_once APPPATH . 'libraries/PHPEXCEL/Classes/PHPExcel.php';
          require_once APPPATH . 'libraries/PHPEXCEL/Classes/PHPExcel/Reader/Excel2007.php';

          //creamos el objeto que debe leer el excel
          $objReader = new PHPExcel_Reader_Excel2007();

          $objPHPExcel = $objReader->load(APPPATH.'../excel_files/'.$file);

          //número de filas del archivo excel
          $rows = $objPHPExcel->getActiveSheet()->getHighestRow();

          //var_dump($rows);

          //obtenemos el nombre de la tabla que el usuario quiere insertar el excel
          $table_name = 'partits';

          //obtenemos los nombres que el usuario ha introducido en el campo text del formulario,
          //se supone que deben ser los campos de la tabla de la base de datos.
          $fields = 'local,visitant,data,hora,pista,isFemeni';
          //$fields_table = explode(",", $this->security->xss_clean($this->input->post("fields")));
          $fields_table = explode(",", $fields);

          //inicializamos sql como un array
          $sql = array();

          //array con las letras de la cabecera de un archivo excel
          $letras = array(
            "A","B","C","D","E","F"
          );

          //recorremos el excel y creamos un array para después insertarlo en la base de datos
          for($i = 1;$i <= $rows; $i++)
          {
            //ahora recorremos los campos del formulario para ir creando el array de forma dinámica
            for($z = 0; $z < count($fields_table); $z++)
            {
              $sql[$i][trim($fields_table[$z])] = $objPHPExcel->getActiveSheet()->getCell($letras[$z].$i)->getCalculatedValue();
            }
          }

          /*echo "<pre>";
          var_dump($sql); exit();
          */

          //cargamos el modelo
          $this->load->model("arbitres");
          //insertamos los datos del excel en la base de datos
          $import_excel = $this->arbitres->excel('partits',	$sql);

          //comprobamos si se ha guardado bien
          if($import_excel == TRUE)
          {
            $this->session->set_flashdata('success','Arxiu importat Correctament');
            //echo "El archivo ha sido importado correctamente";
            $this->importarPartits();
          }else{
            $this->session->set_flashdata('error','Error en la importació');
          }

          //finalmente, eliminamos el archivo pase lo que pase
          unlink("./excel_files/".$file);

        }else{
            $this->session->set_flashdata('error','Has de pujar un arxiu');
        }
  }

  public function editarPartit($idPartit) {

  	$this->load->model("arbitres");

	$partit = $this->arbitres->getPartit($idPartit);

	$arbitres = $this->arbitres->getNomAbritres();

	$tarifes = $this->arbitres->loadTarifes();
	
	$datos['arbitres'] = $arbitres;

	$datos['partit'] = $partit[0];

	$datos['tarifes'] = $tarifes;

	$this->load->view('editPartit', $datos);

  }

    public function editarPartitArbitre($idPartit,$idArbitre) {


        $this->load->model("arbitres");

        $partit = $this->arbitres->getPartit($idPartit);

        $arbitre = $this->arbitres->loadArbitre($idArbitre);

        $arbitres = $this->arbitres->getNomAbritres();

        $tarifes = $this->arbitres->loadTarifes();

        $datos['arbitre'] = $arbitre[0];

        $datos['arbitres'] = $arbitres;

        $datos['partit'] = $partit[0];

        $datos['tarifes'] = $tarifes;

        $this->load->view('editPartitArbitre', $datos);

    }

    public function updateArbitre($idPartit,$idArbitre) {

        $this->load->model('arbitres');
        $this->load->helper('url');

        if ($idArbitre == 0) {
            $idArbitre = null;
        }
        $this->arbitres->updateArbitre($idPartit,$idArbitre);

        redirect('/arbitresBalles/importarPartits', 'refresh');

    }

    public function updateAnotador($idPartit,$idArbitre) {

        $this->load->model('arbitres');
        $this->load->helper('url');
        if ($idArbitre == 0) {
            $idArbitre = null;
        }

        $this->arbitres->updateAnotador($idPartit,$idArbitre);

        redirect('/arbitresBalles/importarPartits', 'refresh');


    }

    public function updatePistaPartit($idPartit, $idPista) {

        $this->load->model('arbitres');
        $this->load->helper('url');
        if ($idPista == 0) {
            $pista = null;

        } else {
            $pistaJoc = $this->arbitres->getPista($idPista);

            $pista = $pistaJoc[0]->nomPista;

        }

        $this->arbitres->updatePistaPartit($idPartit,$pista);

        redirect('/arbitresBalles/importarPartits', 'refresh');
    }

    public function AcceptarPartit($idPartit,$idArbitre,$isArbitre, $isAnotador) {


	    $this->load->model('arbitres');

	    if ($isArbitre == true) {
            $ret = $this->arbitres->acceptarPartitArbitre($idPartit,$idArbitre);
        }

        if ($isAnotador == true) {
            $ret = $this->arbitres->acceptarPartitAnotador($idPartit,$idArbitre);
        }

        if ($ret) {
            $this->session->set_flashdata('success','Partit Acceptat');
        } else {
            $this->session->set_flashdata('error','Error, Partit no acceptat');
        }

        $partit = $this->arbitres->getPartit($idPartit);

        $arbitre = $this->arbitres->loadArbitre($idArbitre);

        $arbitres = $this->arbitres->getNomAbritres();

        $tarifes = $this->arbitres->loadTarifes();

        $datos['arbitre'] = $arbitre[0];

        $datos['arbitres'] = $arbitres;

        $datos['partit'] = $partit[0];

        $datos['tarifes'] = $tarifes;

        $this->load->view('editPartitArbitre', $datos);
    }

  public function newPartit()
  {
      $this->load->model("arbitres");
      $this->load->model("equipos");

      $equips = $this->equipos->getAllEquips();

      $arbitres = $this->arbitres->getNomAbritres();

      $tarifes = $this->arbitres->loadTarifes();

      $datos['arbitres'] = $arbitres;

      $datos['tarifes'] = $tarifes;

      $datos['equips'] = $equips;

      $this->load->view('newPartitForm', $datos);
  }

  public function editPartitForm($idPartit)
	{
		$this->session->unset_userdata('error');
		$this->session->unset_userdata('success');
		$this->load->model('arbitres');
		
		if($this->input->post())
        {
        	$local = $this->input->post('local');
        	$visitant = $this->input->post('visitant');
        	$data = $this->input->post('dataPartit');
        	$hora = $this->input->post('horaPartit');
        	$pista = $this->input->post('pista');
        	$isSuspes = $this->input->post('suspes');
        	$idArbitre = $this->input->post('arbitre');
        	$idTarifaArbitre = $this->input->post('tarifaArbitre');
        	$idAnotador = $this->input->post('anotador');
        	$idTarifaAnotador = $this->input->post('tarifaAnotador');
        	$isFemeni = $this->input->post('isFemeni');
        	//$idDelegat = $this->input->post('delegat');
        	//$idTarifaDelegat = $this->input->post('tarifaDelegat');


        	$ret = $this->arbitres->updatePartit($idPartit,$local,$visitant,$data,$hora,$pista,$isSuspes,$idArbitre,$idTarifaArbitre,$idAnotador,$idTarifaAnotador,$isFemeni);

        }

        if ($ret) {
        	$this->session->set_flashdata('success','Partit Actualitzat');
        } else {
        	$this->session->set_flashdata('error','Error, partit no actualitzat');
        }
		

		$this->editarPartit($idPartit);

	}

    public function newPartitForm()
    {
        $this->session->unset_userdata('error');
        $this->session->unset_userdata('success');
        $this->load->model('arbitres');

        if($this->input->post())
        {
            $local = $this->input->post('local');
            $visitant = $this->input->post('visitant');
            $data = $this->input->post('dataPartit');
            $hora = $this->input->post('horaPartit');
            $pista = $this->input->post('pista');
            $isSuspes = $this->input->post('suspes');
            $idArbitre = $this->input->post('arbitre');
            $idTarifaArbitre = $this->input->post('tarifaArbitre');
            $idAnotador = $this->input->post('anotador');
            $idTarifaAnotador = $this->input->post('tarifaAnotador');
            $isFemeni = $this->input->post('idFemeni');
            //$idDelegat = $this->input->post('delegat');
            //$idTarifaDelegat = $this->input->post('tarifaDelegat');


            $ret = $this->arbitres->createPartit($local,$visitant,$data,$hora,$pista,$isSuspes,$idArbitre,$idTarifaArbitre,$idAnotador,$idTarifaAnotador,$isFemeni);

        }

        if ($ret) {
            $this->session->set_flashdata('success','Partit Creat');
        } else {
            $this->session->set_flashdata('error','Error, partit no actualitzat');
        }


        $this->newPartit();

    }

    public function assignarDelegat()
    {
        $this->load->model("arbitres");
        $this->load->model("equipos");

        $equips = $this->equipos->getAllEquips();

        $arbitres = $this->arbitres->getNomAbritres();

        $tarifes = $this->arbitres->loadTarifes();

        $datos['arbitres'] = $arbitres;

        $datos['tarifes'] = $tarifes;

        $datos['equips'] = $equips;

        $this->load->view('assignarDelegat', $datos);
    }

    public function assignarDelegatForm ()
    {
        $this->session->unset_userdata('error');
        $this->session->unset_userdata('success');
        $this->load->model('arbitres');

        if($this->input->post())
        {
            $data = $this->input->post('dataPartit');
            $hora = $this->input->post('horaPartit');
            $pista = $this->input->post('pista');
            $idDelegat = $this->input->post('delegat');
            $idTarifaDelegat = $this->input->post('tarifaDelegat');


            $ret = $this->arbitres->createFranjaDelegat($data,$hora,$pista,$idDelegat,$idTarifaDelegat);

        }

        if ($ret) {
            $this->session->set_flashdata('success','Franja Creada');
        } else {
            $this->session->set_flashdata('error','Error, Franja no creada');
        }


        $this->assignarDelegat();

    }

	public function showPartitsArbitre($idArbitre) {
		$this->load->model('arbitres');

		$partits = $this->arbitres->showPartitsArbitre($idArbitre);

		$delegats = $this->arbitres->showPartitsDelegat($idArbitre);
		$total = 0;		$totalArbitre = 0;		$totalAnotador = 0;		$totalDelegat = 0;
		$countArbitre = 0; $countDelegat = 0; $countAnotador = 0;$countArbitreFem = 0; $countAnotadorFem = 0;
        if($partits) {
            foreach ($partits as $partit) {
                $totalArbitre += $partit->arbitre;
                if ($partit->arbitre != 0) {
                    if ($partit->isFemeni == 1) {
                        $countArbitreFem ++;
                    } else {
                        $countArbitre ++;
                    }
                }
                $totalAnotador += $partit->anotador;
                if ($partit->anotador != 0) {
                    if ($partit->isFemeni == 1) {
                        $countAnotadorFem ++;
                    } else {
                        $countAnotador ++;
                    }
                }
            }
        }

        if ($delegats) {
            foreach ($delegats as $delegat) {
                $totalDelegat = $delegat->delegat;
                if ($delegat->delegat != 0) { $countDelegat ++; }
            }
        }

		$total = $totalArbitre + $totalAnotador + $totalDelegat;
		$arbitre = $this->arbitres->loadArbitre($idArbitre);
        $dataInicial = '0';$dataFinal='0';
		$datos['partits'] = $partits;
		$datos['delegats'] = $delegats;
		$datos['totalArbitre'] = $totalArbitre;
		$datos['totalAnotador'] = $totalAnotador;
		$datos['totalDelegat'] = $totalDelegat;
		$datos['total'] = $total;
		$datos['arbitre'] = $arbitre[0];
        $datos['countArbitre'] = $countArbitre;
        $datos['countArbitreFem'] = $countArbitreFem;
        $datos['countAnotador'] = $countAnotador;
        $datos['countAnotadorFem'] = $countAnotadorFem;
		$datos['countDelegat'] = $countDelegat;
		$datos['dataInicial'] = $dataInicial;
		$datos['dataFinal'] = $dataFinal;

		$this->load->view('showPartitsArbitre', $datos);

	}

	public function showPartitsArbitreByDate() {
		$this->load->model('arbitres');

		if($this->input->post())
        {
        	$idArbitre = $this->input->post('idArbitre');
        	$dataInicial = $this->input->post('dataInicial');
        	$dataFinal = $this->input->post('dataFinal');
        	//echo $dataInicial;

            if($dataFinal == '' or $dataFinal == ''){
                $partits = $this->arbitres->showPartitsArbitre($idArbitre);
                $delegats = $this->arbitres->showPartitsDelegat($idArbitre);
            } else {
                $dataInicial = explode("/", $dataInicial);
                $dataInicial = $dataInicial['2']."-".$dataInicial['1']."-".$dataInicial['0'];
                $dataFinal = explode("/", $dataFinal);
                $dataFinal = $dataFinal['2']."-".$dataFinal['1']."-".$dataFinal['0'];
                $partits = $this->arbitres->showPartitsArbitreByDate($idArbitre, $dataInicial, $dataFinal);
                $delegats = $this->arbitres->showPartitsDelegatByDate($idArbitre, $dataInicial, $dataFinal);
            }
        }


        $total = 0;		$totalArbitre = 0;		$totalAnotador = 0;		$totalDelegat = 0;
        $countArbitre = 0; $countDelegat = 0; $countAnotador = 0; $countArbitreFem = 0; $countAnotadorFem = 0;
        if($partits) {
            foreach ($partits as $partit) {
                $totalArbitre += $partit->arbitre;
                if ($partit->arbitre != 0) {
                    if ($partit->isFemeni == 1) {
                        $countArbitreFem ++;
                    } else {
                        $countArbitre ++;
                    }
                }
                $totalAnotador += $partit->anotador;
                if ($partit->anotador != 0) {
                    if ($partit->isFemeni == 1) {
                        $countAnotadorFem ++;
                    } else {
                        $countAnotador ++;
                    }
                }
            }
        }

        if ($delegats) {
            foreach ($delegats as $delegat) {
                $totalDelegat = $delegat->delegat;
                if ($delegat->delegat != 0) { $countDelegat ++; }
            }
        }

        $total = $totalArbitre + $totalAnotador + $totalDelegat;
        $arbitre = $this->arbitres->loadArbitre($idArbitre);

        $datos['partits'] = $partits;
        $datos['delegats'] = $delegats;
        $datos['totalArbitre'] = $totalArbitre;
        $datos['totalAnotador'] = $totalAnotador;
        $datos['totalDelegat'] = $totalDelegat;
        $datos['total'] = $total;
        $datos['arbitre'] = $arbitre[0];
        $datos['countArbitre'] = $countArbitre;
        $datos['countArbitreFem'] = $countArbitreFem;
        $datos['countAnotador'] = $countAnotador;
        $datos['countAnotadorFem'] = $countAnotadorFem;
        $datos['countDelegat'] = $countDelegat;
        $datos['dataInicial'] = $dataInicial;
        $datos['dataFinal'] = $dataFinal;

		$this->load->view('showPartitsArbitre', $datos);

	}

	public function enviarMailArbitre($idArbitre, $dataInicial, $dataFinal) {

        $this->load->model('arbitres');

        // Recuperem dades pagament

        if($dataInicial == '0' or $dataFinal == '0'){
            $partits = $this->arbitres->showPartitsArbitre($idArbitre);
            $delegats = $this->arbitres->showPartitsDelegat($idArbitre);
        } else {
            //$dataInicial = explode("/", $dataInicial);
            //$dataInicial = $dataInicial['2']."-".$dataInicial['1']."-".$dataInicial['0'];
            //$dataFinal = explode("/", $dataFinal);
            //$dataFinal = $dataFinal['2']."-".$dataFinal['1']."-".$dataFinal['0'];
            $partits = $this->arbitres->showPartitsArbitreByDate($idArbitre, $dataInicial, $dataFinal);
            $delegats = $this->arbitres->showPartitsDelegatByDate($idArbitre, $dataInicial, $dataFinal);
        }


        $total = 0;		$totalArbitre = 0;		$totalAnotador = 0;		$totalDelegat = 0;
        $countArbitre = 0; $countDelegat = 0; $countAnotador = 0; $countArbitreFem = 0; $countAnotadorFem = 0;
        if($partits) {
            foreach ($partits as $partit) {
                $totalArbitre += $partit->arbitre;
                if ($partit->arbitre != 0) {
                    if ($partit->isFemeni == 1) {
                        $countArbitreFem ++;
                    } else {
                        $countArbitre ++;
                    }
                }
                $totalAnotador += $partit->anotador;
                if ($partit->anotador != 0) {
                    if ($partit->isFemeni == 1) {
                        $countAnotadorFem ++;
                    } else {
                        $countAnotador ++;
                    }
                }
            }
        }

        if ($delegats) {
            foreach ($delegats as $delegat) {
                $totalDelegat = $delegat->delegat;
                if ($delegat->delegat != 0) { $countDelegat ++; }
            }
        }

        $total = $totalArbitre + $totalAnotador + $totalDelegat;
        $arbitre = $this->arbitres->loadArbitre($idArbitre);



        $this->load->library('email'); // load email library
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.basquetsabadell.com'; //change this
        $config['smtp_port'] = '587';
        //$config['smtp_crypto'] = 'tls';
        $config['smtp_user'] = 'laballes@basquetsabadell.com'; //change this
        $config['smtp_pass'] = 'Whvelico2007'; //change this
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard

        $this->email->initialize($config);
        $this->email->from('laballes@basquetsabadell.com', 'Balles');
        $this->email->to($arbitre[0]->mail);
        //$this->email->cc('test2@gmail.com');
        //$message = 'mail de pagaments de la balles';
        $message = '<!DOCTYPE HTML>
                        <html lang="es-ES">
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
                            <title>Pagaments Balles</title>
                        </head>
                        <body>
                            <h3>'.$arbitre[0]->nomArbitre.' '.$arbitre[0]->cognomsArbitre.'</h3>
                           <h3>Relació de partits : </h3>
                           <p>Arbitre : '.$countArbitre.' partits -  import : '.$totalArbitre.' €</p>
                           <p>Anotador : '.$countAnotador.' -  import : '.$totalAnotador.' €</p>
                           <p>Delegat : '.$countDelegat.' -  import : '.$totalDelegat.' €</p>
                           <h3>Total a Cobrar : '.$total.' €</h3>
                        <table id="partits" class="" border=1 cellspacing=2 cellpadding=4>
                              <thead>
                                  <tr>
                                    <th>local</th>
                                    <th>visitant</th>
                                    <th>data</th>
                                    <th>hora</th>
                                    <th>pista</th>  
                                    <th>Arbitre</th>
                                    <th>Anotador</th>                              
                                  </tr>
                              </thead>                        
                        <tbody>';
                            if($partits){
                              foreach ($partits as $partit) {
              $message .=     '<tr><td>'.$partit->local.'</td>
                                <td>'.$partit->visitant.'</td>
                                <td>'.$partit->data.'</td>
                                <td>'.$partit->hora.'</td>
                                <td>'.$partit->pista.'</td>
                                <td>'.$partit->arbitre.' €</td>
                                <td>'.$partit->anotador.' €</td></tr>';
                              }
                            }
        $message .=  '</tbody></table><h3>Franjes Delegat</h3>';

        $message .= '<table id="delegats" class="" border=1 cellspacing=2 cellpadding=4>
                              <thead>
                                  <tr>
                                    <th>data</th>
                                    <th>hora</th>
                                    <th>pista</th>  
                                    <th>delegat</th>                              
                                  </tr>
                              </thead>                        
                        <tbody>';
                            if($delegats){
                              foreach ($delegats as $delegat) {
              $message .=     '<tr>                            
                                <td>'.$delegat->data.'</td>
                                <td>'.$delegat->hora.'</td>
                                <td>'.$delegat->pista.'</td>
                                <td>'.$delegat->delegat.' €</td>
                                </tr>';
                              }
                            }

            $message .=  '</tbody></table></body></html>';
        $this->email->subject('Pagaments Balles : '.$arbitre[0]->nomArbitre.' '.$arbitre[0]->cognomsArbitre);
        $this->email->message($message);
        //$this->email->message("L'equip : ".$_SESSION['USUARIO']['idEquip']." -> ".$nomEquip->nomEquip." ha inscrit un jugador en data : ".date('Y-m-d H:i:s'));
        //$this->email->attach('/path/to/file1.png'); // attach file
        //$this->email->attach('/path/to/file2.pdf');
        if ($this->email->send()){
            echo "Mail Sent!";
        } else {
            show_error($this->email->print_debugger());
        }

        $this->showPartitsArbitre($idArbitre);
    }

    public function enviarMailPagaments() {
        $this->load->model('arbitres');

        $arbitres = $this->arbitres->loadArbitres();
        $correct = array();$fail = array();
        $dataInicial = '0';
        $dataFinal = '0';

        if($this->input->post()) {
            $dataInicial = $this->input->post('dataInicial');
            $dataFinal = $this->input->post('dataFinal');
            $subject = $this->input->post('subject');
            $messageForm = $this->input->post('mensajeMail');
            $dataInicial = explode("/", $dataInicial);
            $dataInicial = $dataInicial['2']."-".$dataInicial['1']."-".$dataInicial['0'];
            $dataFinal = explode("/", $dataFinal);
            $dataFinal = $dataFinal['2']."-".$dataFinal['1']."-".$dataFinal['0'];
        }

        foreach ($arbitres as $arbitre) {

            if($dataInicial == '0' or $dataFinal == '0'){
                $partits = $this->arbitres->showPartitsArbitre($arbitre->idArbitre);
                $delegats = $this->arbitres->showPartitsDelegat($arbitre->idArbitre);
            } else {
                $partits = $this->arbitres->showPartitsArbitreByDate($arbitre->idArbitre, $dataInicial, $dataFinal);
                $delegats = $this->arbitres->showPartitsDelegatByDate($arbitre->idArbitre, $dataInicial, $dataFinal);
            }


            $total = 0;		$totalArbitre = 0;		$totalAnotador = 0;		$totalDelegat = 0;
            $countArbitre = 0; $countDelegat = 0; $countAnotador = 0;
            if($partits) {
                foreach ($partits as $partit) {
                    $totalArbitre += $partit->arbitre;
                    if ($partit->arbitre != 0) { $countArbitre ++; }
                    $totalAnotador += $partit->anotador;
                    if ($partit->anotador != 0) { $countAnotador ++; }
                }
            }

            if ($delegats) {
                foreach ($delegats as $delegat) {
                    $totalDelegat = $delegat->delegat;
                    if ($delegat->delegat != 0) { $countDelegat ++; }
                }
            }

            $total = $totalArbitre + $totalAnotador + $totalDelegat;

            $this->load->library('email'); // load email library
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'smtp.basquetsabadell.com'; //change this
            $config['smtp_port'] = '587';
            //$config['smtp_crypto'] = 'tls';
            $config['smtp_user'] = 'laballes@basquetsabadell.com'; //change this
            $config['smtp_pass'] = 'Whvelico2007'; //change this
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard

            $this->email->initialize($config);
            $this->email->from('laballes@basquetsabadell.com', 'Balles');

            $this->email->to($arbitre->mail);

            $message = '<!DOCTYPE HTML>
                        <html lang="es-ES">
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
                            <title>Pagaments Balles</title>
                        </head>
                        <body>
                            <div>'.$messageForm.'</div>
                            <h3>'.$arbitre->nomArbitre.' '.$arbitre->cognomsArbitre.'</h3>
                           <h3>Relació de partits : </h3>
                           <p>Arbitre : '.$countArbitre.' partits -  import : '.$totalArbitre.' €</p>
                           <p>Anotador : '.$countAnotador.' -  import : '.$totalAnotador.' €</p>
                           <p>Delegat : '.$countDelegat.' -  import : '.$totalDelegat.' €</p>
                           <h3>Total a Cobrar : '.$total.' €</h3>
                        <table id="partits" class="" border=1 cellspacing=2 cellpadding=4>
                              <thead>
                                  <tr>
                                    <th>local</th>
                                    <th>visitant</th>
                                    <th>data</th>
                                    <th>hora</th>
                                    <th>pista</th>  
                                    <th>Arbitre</th>
                                    <th>Anotador</th>                              
                                  </tr>
                              </thead>                        
                        <tbody>';
            if($partits){
                foreach ($partits as $partit) {
                    $message .=     '<tr><td>'.$partit->local.'</td>
                                <td>'.$partit->visitant.'</td>
                                <td>'.$partit->data.'</td>
                                <td>'.$partit->hora.'</td>
                                <td>'.$partit->pista.'</td>
                                <td>'.$partit->arbitre.' €</td>
                                <td>'.$partit->anotador.' €</td></tr>';
                }
            }
            $message .=  '</tbody></table><h3>Franjes Delegat</h3>';

            $message .= '<table id="delegats" class="" border=1 cellspacing=2 cellpadding=4>
                              <thead>
                                  <tr>
                                    <th>data</th>
                                    <th>hora</th>
                                    <th>pista</th>  
                                    <th>delegat</th>                              
                                  </tr>
                              </thead>                        
                        <tbody>';
            if($delegats){
                foreach ($delegats as $delegat) {
                    $message .=     '<tr>                            
                                <td>'.$delegat->data.'</td>
                                <td>'.$delegat->hora.'</td>
                                <td>'.$delegat->pista.'</td>
                                <td>'.$delegat->delegat.' €</td>
                                </tr>';
                }
            }

            $message .=  '</tbody></table></body></html>';

            $this->email->subject($subject);
            $this->email->message($message);

            if($this->email->send()){
                //echo "Mail Sent!";
                array_push($correct, $arbitre->nomArbitre." ".$arbitre->cognomsArbitre." - ".$arbitre->mail);
            } else {
                //show_error($this->email->print_debugger());
                array_push($fail, $arbitre->nomArbitre." ".$arbitre->cognomsArbitre." - ".$arbitre->mail);
            }
        }

        $this->session->set_flashdata('success',$correct);
        $this->session->set_flashdata('error',$fail);
        $this->importarPartits();

    }

    public function filtrarPartitsDates() {
        $this->load->model('arbitres');

        $arbitres = $this->arbitres->getNomAbritres();
        $pistes = $this->arbitres->loadPistes();
        $correct = array();$fail = array();
        $dataInicial = '0';
        $dataFinal = '0';

        if($this->input->post()) {
            $dataInicial = $this->input->post('dataInicial');
            $dataFinal = $this->input->post('dataFinal');
            $dataInicial = explode("/", $dataInicial);
            $dataInicial = $dataInicial['2']."-".$dataInicial['1']."-".$dataInicial['0'];
            $dataFinal = explode("/", $dataFinal);
            $dataFinal = $dataFinal['2']."-".$dataFinal['1']."-".$dataFinal['0'];
        }

        if($dataInicial == '0' or $dataFinal == '0'){
            $partitsMasculins = $this->arbitres->loadPartitsMasculins();
            $partitsFemenins = $this->arbitres->loadPartitsFemenins();

            $franjaDelegats = $this->arbitres->loadFranjaDelegats();

        } else {

            $partitsMasculins = $this->arbitres->showPartitsMasculinsByDate($dataInicial, $dataFinal);
            $partitsFemenins = $this->arbitres->showPartitsFemeninsByDate($dataInicial, $dataFinal);
            $franjaDelegats = $this->arbitres->showFranjaDelegatByDate($dataInicial, $dataFinal);
        }

        $datos['partits'] = $partitsMasculins;
        $datos['partitsFem'] = $partitsFemenins;
        $datos['franjaDelegats'] = $franjaDelegats;
        $datos['arbitres'] = $arbitres;
        $datos['pistes'] = $pistes;

        $this->load->view('importarPartits', $datos);

    }

    public function showPistes() {
        $this->session->unset_userdata('error');
        $this->session->unset_userdata('success');
        $this->load->model('arbitres');

        $pistes = $this->arbitres->loadPistes();

        $datos['pistes'] = $pistes;

        $this->load->view('llistatPistes', $datos);
    }

    public function newPista () {
        $this->session->unset_userdata('error');
        $this->session->unset_userdata('success');
        $this->load->model('arbitres');

        if($this->input->post())
        {
            $nom = $this->input->post('nom');
            $direccio = $this->input->post('direccio');
            $poblacio = $this->input->post('poblacio');
            $horari = $this->input->post('horari');
            $isForaSabadell = $this->input->post('isForaSabadell');


            $ret = $this->arbitres->createPista($nom,$direccio,$poblacio,$horari,$isForaSabadell);

        }

        if ($ret) {
            $this->session->set_flashdata('success','Franja Creada');
        } else {
            $this->session->set_flashdata('error','Error, Franja no creada');
        }


        $this->showPistes();
    }

    public function editarPista ($idPista) {
        $this->load->model("arbitres");

        $pista = $this->arbitres->getPista($idPista);

        $datos['pista'] = $pista[0];

        $this->load->view('editPista', $datos);
    }

    public function editPistaForm($idPista) {
        $this->session->unset_userdata('error');
        $this->session->unset_userdata('success');
        $this->load->model('arbitres');

        if($this->input->post())
        {
            $nom = $this->input->post('nom');
            $direccio = $this->input->post('direccio');
            $poblacio = $this->input->post('poblacio');
            $horari = $this->input->post('horari');
            $isForaSabadell = $this->input->post('isForaSabadell');

            $ret = $this->arbitres->updatePista($idPista,$nom,$direccio,$poblacio,$horari,$isForaSabadell);

        }

        if ($ret) {
            $this->session->set_flashdata('success','Pista Actualitzada');
        } else {
            $this->session->set_flashdata('error','Error, pista no actualitzada');
        }

        $this->showPistes();
    }

    public function deletePista($idPista) {

        $this->load->model('arbitres');

        $ret = $this->arbitres->deletePista($idPista);

        if ($ret) {
            $this->session->set_flashdata('success','Pista Eliminada');
        } else {
            $this->session->set_flashdata('error','Error, pista no eliminada');
        }

        $pistes = $this->arbitres->loadPistes();

        $datos['pistes'] = $pistes;

        $this->load->view('llistatPistes', $datos);

    }

    public function activatePista($idPista) {

        $this->load->model('arbitres');

        $ret = $this->arbitres->activatePista($idPista);

        if ($ret) {
            $this->session->set_flashdata('success','Pista Reactivada');
        } else {
            $this->session->set_flashdata('error','Error, pista no reactivada');
        }

        $pistes = $this->arbitres->loadPistes();

        $datos['pistes'] = $pistes;

        $this->load->view('llistatPistes', $datos);

    }

    public function enviarDessignacio() {

        $this->load->model('arbitres');

        $arrPartits = array();
        $arrArbitres = array();
        $arrMailArbitres = array();

	    $partits = $_COOKIE['dessignacio'];

        $arbitres = $this->arbitres->getNomAbritres();

        $dessignacio = explode(",", $partits);

        foreach ($dessignacio as $idPartit) {
            $partit = $this->arbitres->getPartit($idPartit);
            array_push($arrPartits, $partit[0]);
            array_push($arrArbitres, $partit[0]->idArbitre);
            array_push($arrArbitres, $partit[0]->idAnotador);
        }
        
        $arrArbitres = array_unique($arrArbitres);
        //var_dump($arrArbitres);
        foreach ($arrArbitres as $arbitre) {
            $mail = $this->arbitres->getMailArbitreById($arbitre);
            if ($mail) {
                array_push($arrMailArbitres, $mail[0]);
            }

        }
        //echo "<pre>";var_dump($arrPartits);

        $datos['partits'] = $arrPartits;
        $datos['mailArbitres'] = $arrMailArbitres;
        $datos['arbitres'] = $arbitres;

	    $this->load->view('createDessignacio', $datos);
	    //$this->load->view('createDessignacio_2', $datos);
    }

    public function desarDessignacio() {
        $this->load->model('arbitres');
        if($this->input->post()) {
            $mailArbitres = '';
            $dessignacio = $this->input->post('dessignacio');
            $data = $this->input->post('data');
            $nom = $this->input->post('nom');
            $mails = $this->input->post('mails');

            $arrayMails = unserialize(base64_decode($mails));

            foreach ($arrayMails as $mail) {
                ($mailArbitres .= $mail->mail.",");
            }


            $this->arbitres->desarDessignacio($dessignacio,$nom,$mailArbitres,$data);

        }

        $this->importarPartits();
    }

    public function updateDessignacio() {
        $this->load->model('arbitres');
        if($this->input->post()) {
            $mailArbitres = '';
            $dessignacio = $this->input->post('dessignacio');
            $idDessignacio = $this->input->post('id');
            $data = $this->input->post('data');
            $nom = $this->input->post('nom');
            $mails = $this->input->post('mails');

            $arrayMails = unserialize(base64_decode($mails));

            foreach ($arrayMails as $mail) {
                ($mailArbitres .= $mail.",");
            }


            $this->arbitres->updateDessignacio($idDessignacio,$dessignacio,$nom,$mailArbitres,$data);

        }

        $this->showDessignacions();
    }

    public function updateDessignacioSend () {
        $this->load->model('arbitres');
        if($this->input->post()) {
            $mailArbitres = '';
            $dessignacio = $this->input->post('dessignacio');
            $idDessignacio = $this->input->post('id');
            $data = $this->input->post('data');
            $nom = $this->input->post('nom');
            $subject = $this->input->post('subject');
            $mails = $this->input->post('mails');

            $arrayMails = unserialize(base64_decode($mails));

            foreach ($arrayMails as $mail) {
                ($mailArbitres .= $mail.",");
            }

            $this->load->library('email'); // load email library
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'smtp.basquetsabadell.com'; //change this
            $config['smtp_port'] = '587';
            //$config['smtp_crypto'] = 'tls';
            $config['smtp_user'] = 'laballes@basquetsabadell.com'; //change this
            $config['smtp_pass'] = 'Whvelico2007'; //change this
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard

            $this->email->initialize($config);
            $this->email->from('laballes@basquetsabadell.com', 'Balles');
            $this->email->to($mailArbitres);
            //$this->email->cc('test2@gmail.com');
            //$message = 'mail de pagaments de la balles';
            $message = '<!DOCTYPE HTML>
                        <html lang="es-ES">
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
                            <title>Pagaments Balles</title>
                        </head>
                        <body>
                          '.$dessignacio.'';


            $this->email->subject($subject);
            $this->email->message($message);
            //$this->email->message("L'equip : ".$_SESSION['USUARIO']['idEquip']." -> ".$nomEquip->nomEquip." ha inscrit un jugador en data : ".date('Y-m-d H:i:s'));
            //$this->email->attach('/path/to/file1.png'); // attach file
            //$this->email->attach('/path/to/file2.pdf');
            if ($this->email->send()){
                //echo "Mail Sent!";
            } else {
                show_error($this->email->print_debugger());
            }

            $this->arbitres->updateDessignacio($idDessignacio,$dessignacio,$nom,$mailArbitres,$data);

        }

        $this->showDessignacions();
    }

    public function showDessignacions () {

	    $this->load->model('arbitres');

        $dessignacions = $this->arbitres->loadDessignacions();

        $datos['dessignacions'] = $dessignacions;

        $this->load->view('llistatDessignacions', $datos);

    }

    public function editarDessignacio ($idDessignacio) {
	    $this->load->model('arbitres');

	    $dessignacio = $this->arbitres->getDessignacio($idDessignacio);

	    $mails = $dessignacio[0]->mails;
        $arrMails = explode(',', $mails);
        array_pop($arrMails);
        $arbitres = $this->arbitres->getNomAbritres();

	    $datos['dessignacio'] = $dessignacio[0];
        $datos['arbitres'] = $arbitres;
        $datos['mailArbitres'] = $arrMails;

	    $this->load->view('editDessignacio', $datos);


    }
}