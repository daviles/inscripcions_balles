<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('../app/fpdf/fpdf.php');
//echo $fitxes."-------".$nomEquip;
define("Alturacarnet",0);
define("HPag",721.89);
define("MargeCarnet_a_Esquerra",100);
//$idEquip = $_SESSION['USUARIO']['idEquip'];
//$idEquip = $_REQUEST['idEquip'];
$contador = 0;
$y=50;
$x=10;
$h = 0;
$i = 0;
$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);

for ($i = 1;$i<=2;$i++){
	$pdf->SetFont('Arial','B',15);
	$pdf->Text($x+29, $y-30, "ASSOCIACIO DE BASQUET DE LLEURE DE SABADELL");
	$pdf->Image('../images/logoBalles.gif',$x+2,$y-43,20,22,'GIF');
	$pdf->Line($x-9, $y-20, $x+199, $y-20);
	$pdf->Text($x+40, $y-10, "REBUT BALLES TEMPORADA 2017/18");
	$pdf->SetFont('Arial','',12);
	$pdf->Text($x+10, $y+5, "NOM EQUIP : ".$nomEquip);
	if ($fitxes != 0){
		$pdf->Text($x+10, $y+15, "TOTAL FITXES : ".$fitxes."  x  55 .- ");
		$pdf->Text($x+150, $y+15, ($fitxes*55)." .- ");
	}
	if ($import != ''){
		$pdf->Text($x+10, $y+25, "IMPORT : ");
		$pdf->Text($x+150, $y+25, $import." .-");
	}
	$pdf->Text($x+42, $y+35, "REBEM DE L'EQUIP ".$nomEquip." L'IMPORT DE  :            ".(($fitxes*55)+$import)." .-");
	$pdf->Text($x+10, $y+55, "Firma i Segell de l'Entitat");
	if($i == 1){
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x+80, $y+75, "(Exemplar per la Balles)");
		$pdf->SetFont('Arial','',12);
		$pdf->Text($x-4, $y+90,"----------------------------------------------------------------------------------------------------------------------------------------------");
	}else{
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x+80, $y+75, "(Exemplar per l'equip)");
	}
	$y += 140;
}

$pdf->Output();
?>