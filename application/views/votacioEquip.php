<?php defined('BASEPATH') OR exit('No direct script access allowed');
 $this->load->view('header'); 
if($_SESSION['USUARIO'] == '' ){header('location:index.php');}
if ($_SESSION['USUARIO']['rol'] == 1){
	//$idEquip = $_REQUEST['idEquip'];
	//$_SESSION['USUARIO']['idEquip'] = $idEquip;	
	//echo "es rol 1";
}else{
	$idEquip = $_SESSION['USUARIO']['idEquip'];
}
/*if ($_SESSION['USUARIO']['rol'] == 1){
	$idEquip = $_REQUEST['idEquip'];
	$_SESSION['USUARIO']['idEquip'] = $idEquip;	
	//echo "es rol 1";
}else{
	$idEquip = $_SESSION['USUARIO']['idEquip'];
}*/
//if($_SESSION['USUARIO'] == '' || $_SESSION['USUARIO']['rol'] != 1 || $_SESSION['USUARIO']['idEquip'] == 16){header('location:index.php');}
//$idDivisio = getIdDivsiobyIdEquip($_SESSION['USUARIO']['idEquip']);

//print_r($_SESSION);

//$numVotacio = getAllVotacioByIdEquip($_SESSION['USUARIO']['idEquip']);
//echo $idDivisio;
//print_r($numVotacio);
?>
<script type="text/javascript">
        var votacioJugadors = <?php echo json_encode($votosJugadores); ?>;
        //console.log(votacioJugadors[0].votos);
        //var jugadors = JSON.parse(votacioJugadors);
		//console.log(jugadors);
		
</script>
<style>
	label
	{
	font-weight:bold;
	padding:10px;
	}
  .select
  {
  border: 1px solid #DBE1EB;
  font-size: 14px;
  font-family: Arial, Verdana;
  padding-left: 7px;
  padding-right: 7px;
  padding-top: 10px;
  padding-bottom: 10px;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  -o-border-radius: 4px;
  background: #FFFFFF;
  background: linear-gradient(left, #FFFFFF, #F7F9FA);
  background: -moz-linear-gradient(left, #FFFFFF, #F7F9FA);
  background: -webkit-linear-gradient(left, #FFFFFF, #F7F9FA);
  background: -o-linear-gradient(left, #FFFFFF, #F7F9FA);
  color: #2E3133;
  }
  
  .select:hover
  {
  border-color: red;
  }
  
  .select option
  {
  border: 1px solid #DBE1EB;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  -o-border-radius: 4px;
  }
  
  .select option:hover
  {
  background: #FC4F06;
  background: linear-gradient(left, #FC4F06, #D85F2B);
  background: -moz-linear-gradient(left, #FC4F06, #D85F2B);
  background: -webkit-linear-gradient(left, #FC4F06, #D85F2B);
  background: -o-linear-gradient(left, #FC4F06, #D85F2B);
  font-style: italic;
  color: #FFFFFF;
  }

.picture {
    background-size: 150px 150px;
    background-repeat: no-repeat;
    margin-left : 41%;
    margin-top: 3%;
    border: 1px solid black;
}
</style>
<script language="javascript">
	$(document).ready(function()
	{
		$('#jugadors').on('change', function () {
		    $('select').removeClass('activeList');
		    $(this).addClass('activeList');
		    var url = $('option:selected', this).attr('myImg');
		    url = '<?=base_url()?>'+url;
		   	$(".picture").css({
			    'background-image': 'url(' + url + ')',
			    'background-repeat': 'no-repeat',
			    'width' : '150px',
			    'height' : '150px'
			});
		});
		$(".divisio").change(function(){
			var id=$(this).val();
			<?php if($numVotacio) { ?>
				var numEquip = <?php echo $numVotacio[0]->numEquip; ?>
			<?php }else{ ?>
				var numEquip = 0;
			<?php } ?>
			//var dataString = 'id='+ id;
			var dataString = {
                "id" : id,
                "numEquip" : numEquip
        };
			
			$.ajax
			({
				type: "POST",
				url: "<?=base_url() ?>equiposBalles/select_dependientes_proceso_equipo",
				data: dataString,
				//dataType:'json',
				cache: false,
				success: function(html)
				{
					$(".equip").html(html);
				} 
			});

		});
		$(".equip").change(function()
		{
			var id=$(this).val();
			var dataString = 'id='+ id;
			//alert(dataString);
			$.ajax
			({
				type: "POST",
				url: "<?=base_url() ?>equiposBalles/select_dependientes_proceso",
				data: dataString,
				//dataType:'json',
				cache: false,
				success: function(html)
				{
					$(".jugadors").html(html);
				} 
			});

		});
	});

	var chart;
	$(document).ready(function() {
	console.log(votacioJugadors[1]);
		$('#grafica').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Votacions All Stars 2017'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Vots'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Vots Totals: <b>{point.y}</b>'
        },
        series: [{
            name: 'Vots Jugadors',
                data: (function() {
                   var data = [];
                    <?php
                        for($i = 0 ;$i<count($votosJugadores);$i++){
                    ?>
                    data.push(['<?php echo $votosJugadores[$i]->jugador;?>',<?php echo $votosJugadores[$i]->votos;?>]);
                    <?php } ?>
                return data;
                })()
            ,
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: 'black',
                align: 'center',
                format: '{point.y}', // one decimal
                y: 25, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
	});		


	function jsFunction()
	{
		//lert('ha cambiado');
	}

</script>
<div class="container">
	<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>
<?php if ($_SESSION['USUARIO']['rol'] == 1){  
	//echo "<a href='adminBalles.php'><button style='margin-left:2%'; type='button' class='btn btn-warning'>Tornar a Administracio</button></a>";
	 }else{ 
	//echo "<a href='formInscripcio.php'><button style='float:right'; type='button' class='btn btn-warning'>Inscrire mes jugadors</button></a>";
	 } ?>
<div style="margin-top:4%"; class="panel panel-success">
  <div style="text-align:center;font-size:22px;" class="panel-heading"><b>VOTACIO</b></span>
 </div>
 	<?php if($numVotacio) {
	 		if($numVotacio[0]->numEquip == 0){ ?>
	 
			 <div class="col-md-12">
			 	<h2>Vota un membre del teu equip</h2>
			 </div>

		<?php }else{ ?>
			<?php if($numVotacio[0]->numRestaEquips == 9){ ?>
				<script> alertify.alert("Ja has realitzat tots els teus vots. Gràcies"); </script>
			<?php }else{ ?>
			
			<div class="col-md-12">
				<script> alertify.alert("Encara et queden "+<?php echo 9-$numVotacio[0]->numRestaEquips;?>+" vots per realitzar"); </script>
			 	<h2>Vota <?php echo (9 - $numVotacio[0]->numRestaEquips); ?> jugadors de la resta d'equips de la teva divisio</h2>
			 </div>
			 <?php } ?>
		<?php } ?>
	<?php } ?>
<?php if( !$numVotacio || $numVotacio[0]->numRestaEquips < 9){ ?>
<div class="row">
	<form role="form" action="<?=base_url() ?>equiposBalles/votacioAllstar" method="POST">
		<input type="hidden" name = "divisio" value = "<?php echo $idDivisio->idDivisio; ?>">
		<div class="col-md-3"> 
	  	<select name="divisio" id="divisio" class="select divisio">
		<option selected="selected">--Select Divisio--</option>
		<?php 
		if($divisio != ''){
			$id=$divisio[0]->idDivisio;
			$data=$divisio[0]->nomDivisio;
			echo '<option value="'.$id.'">'.$data.'</option>';
		 } ?>
		</select>
		</div> <br/>
		<div class="col-md-3">
		  <select name="equip" id="equip" class="select equip">
			<option selected="selected">--Select Equip--</option>
		</select> </div>
		<div class="col-md-3">
		<select name="jugadors" class="select jugadors" id="jugadors">
			<option selected="selected">--Select Player--</option>
		</select>
		</div></br> <button style='margin-left:10%;margin-bottom:5%;' type='sumbit' class='btn btn-success'>Votar</button>
		<div class="col-md-1 picture"></div>
		
		</div>

	</form>
</div>
<?php } ?>
	<div class="col-md-12" id="grafica"></div>
</div>
</div>
	
	<?php $this->load->view('footer'); ?>