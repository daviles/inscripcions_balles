<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $rol = $_SESSION['USUARIO']['rol']; ?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= site_url(); ?>public/images/logoBalles.gif" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $_SESSION['USUARIO']['nomUser']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i>
                    <?php if ($rol == 1) {
                        $rolName = 'Admin';
                    } ?>
                    <?php if ($rol == 2) {
                        $rolName = 'Equip';
                    } ?>
                    <?php if ($rol == 3) {
                        $rolName = 'Arbitre/Anotador';
                    } ?>
                    <?= $rolName ?>
                </a>
            </div>
        </div>
        <!-- search form -->
        <!--<form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
                  <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                  </button>
                </span>
          </div>
        </form>-->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENÚ PRINCIPAL</li>
            <?php if ($rol == 1) { ?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-trophy"></i> <span>Categories</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <!--<li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                        <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>-->
                        <li><a href="<?= base_url() ?>equiposBalles/equipsPerDivisio/1"><i class="fa fa-circle-o"></i>
                                Primera Masculina</a></li>
                        <li><a href="<?= base_url() ?>equiposBalles/equipsPerDivisio/2"><i class="fa fa-circle-o"></i>
                                Segona Masculina</a></li>
                        <li><a href="<?= base_url() ?>equiposBalles/equipsPerDivisio/5"><i class="fa fa-circle-o"></i>
                                Tercera Masculina</a></li>
                        <li><a href="<?= base_url() ?>equiposBalles/equipsPerDivisio/3"><i class="fa fa-circle-o"></i>
                                Primera Femenina</a></li>
                        <li><a href="<?= base_url() ?>equiposBalles/equipsPerDivisio/4"><i class="fa fa-circle-o"></i>
                                Segona Femenina</a></li>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($rol == 1) { ?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-list-alt"></i> <span>Llistats</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= base_url() ?>equiposBalles/esportivitat"><i class="fa fa-circle-o"></i>
                                Esportivitat</a></li>
                        <li><a href="<?= base_url() ?>equiposBalles/lastInscripcions"><i class="fa fa-circle-o"></i>
                                Últimes Inscripcions</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-tasks"></i> <span>Accions</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= base_url() ?>equiposBalles/exportJugadorsExcel"><i
                                        class="fa fa fa-file-excel-o"></i> Exportar Jugadors</a></li>
                        <li><a href="<?= base_url() ?>equiposBalles/printRebutManual"><i class="fa fa fa-money"></i>
                                Rebut Manual</a></li>
                    </ul>
                </li>
                <?php //if ((!array_key_exists('USUARIO', $_SESSION['USUARIO'] )) || ($_SESSION['USUARIO']['rol'] != 1)) {  ?>
                <?php if (isset($pagament['idEquip'])) { ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-th-large"></i> <span>Equip</span>
                            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href='<?= base_url() ?>equiposBalles/printRebut/<?= $pagament['idEquip'] ?>'
                                   target='__blank'><i class="fa fa-file-pdf-o"></i> Imprimir Rebut</a></li>
                            <li><a href='<?= base_url() ?>equiposBalles/printTriptic/<?= $pagament['idEquip'] ?>'
                                   target='__blank'><i class="fa fa-file-excel-o"></i> Imprimir Tríptic</a></li>
                        </ul>
                    </li>

                <?php } ?>
            <?php } ?>
            <?php if (($_SESSION['USUARIO']['rol'] == 1) || ($_SESSION['USUARIO']['rol'] == 3)) { ?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>Arbitres</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href='<?= base_url() ?>arbitresBalles/showTarifes'><i class="fa fa-list"></i> Tarifes</a>
                        </li>
                        <li><a href='<?= base_url() ?>arbitresBalles/llistatArbitres'><i class="fa fa-list"></i>
                                Arbitres</a></li>
                        <?php if ($_SESSION['USUARIO']['rol'] == 3) { ?>
                            <li><a href='<?= base_url() ?>login/loginUser/1'><i class="fa fa-list"></i> Partits</a></li>
                        <?php } ?>
                        <?php if ($_SESSION['USUARIO']['rol'] == 1) { ?>
                            <li><a href='<?= base_url() ?>arbitresBalles/importarPartits'><i class="fa fa-list"></i>
                                    Partits</a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($rol == 1) { ?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-map-marker"></i> <span>Pistes</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href='<?= base_url() ?>arbitresBalles/showPistes'><i class="fa fa-list"></i> Llistat
                                Pistes</a></li>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($rol == 1) { ?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-calendar"></i> <span>Dessignacions</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href='<?= base_url() ?>arbitresBalles/showDessignacions'><i class="fa fa-hashtag"></i>
                                Llistat Dessignacions</a></li>
                    </ul>
                </li>
            <?php } ?>
            <!--<li>
              <a href="pages/widgets.html">
                <i class="fa fa-th"></i> <span>Widgets</span>
                <span class="pull-right-container">
                  <small class="label pull-right bg-green">new</small>
                </span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Charts</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
                <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
                <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>UI Elements</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
                <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
                <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
                <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
                <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
                <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Forms</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
                <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
                <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-table"></i> <span>Tables</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
                <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
              </ul>
            </li>
            <li>
              <a href="pages/calendar.html">
                <i class="fa fa-calendar"></i> <span>Calendar</span>
                <span class="pull-right-container">
                  <small class="label pull-right bg-red">3</small>
                  <small class="label pull-right bg-blue">17</small>
                </span>
              </a>
            </li>
            <li>
              <a href="pages/mailbox/mailbox.html">
                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                <span class="pull-right-container">
                  <small class="label pull-right bg-yellow">12</small>
                  <small class="label pull-right bg-green">16</small>
                  <small class="label pull-right bg-red">5</small>
                </span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-folder"></i> <span>Examples</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
                <li><a href="pages/examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
                <li><a href="pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
                <li><a href="pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
                <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
                <li><a href="pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
                <li><a href="pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
                <li><a href="pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
                <li><a href="pages/examples/pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-share"></i> <span>Multilevel</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level One
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li class="treeview">
                      <a href="#"><i class="fa fa-circle-o"></i> Level Two
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
              </ul>
            </li>
            <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
            <li class="header">LABELS</li>-->
            <li>
                <a href="<?= base_url() ?>login/logout"><i class="fa fa-circle-o text-red"></i>
                    <span>Tancar Sessió</span></a>
            </li>
            <!--<li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>-->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>