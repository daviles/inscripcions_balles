<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php /*include('header.php');*/ $this->load->view('header'); ?>
<?php /*
if($_SESSION['USUARIO'] == '' || $_SESSION['USUARIO']['rol'] == 2){header('location:index.php');}
$idEquip = $_SESSION['USUARIO']['idEquip'];*/
?>
<?php
//$con = "select equip.nomEquip from equip where idEquip <> 1 and actiu='ACT'";
//print_r($query);// = mysql_query($con);
$equiposBuscador = array();
foreach ($query as $q) {
    array_push($equiposBuscador, $q->nomEquip);
}
//print_r($equiposBuscador);
?>
    <script>
        $(function() {
            <?php
            foreach ($equiposBuscador as $row) {
                # code...
                //echo $row;
                $elementos[]= '"'.$row.'"';
                /*while($row= mysql_fetch_array($equiposBuscador)) {//se reciben los valores y se almacenan en un arreglo
                            $elementos[]= '"'.$row.'"';*/
            }
            $arreglo= implode(', ', $elementos);//junta los valores del array en una sola cadena de texto
            ?>
            var availableTags=new Array(<?php echo $arreglo; ?>);//imprime el arreglo dentro de un array de javascript
            $( "#tags" ).autocomplete({
                source: availableTags
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('#partits').DataTable( {
                "order": [[ 1, "asc" ]],
                "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false,
                'scrollX'     : true,

            } );
            $('#delegat').DataTable( {
                "order": [[ 1, "asc" ]],
                "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false,
                'scrollX'     : true,

            } );
        } );
    </script>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Panell <?=$arbitre->nomArbitre?> <?=$arbitre->cognomsArbitre?>
            </h1>


            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>Mail:</h3>

                        <p><?=$arbitre->mail ?></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios-email-outline"></i>
                    </div>
                    <a href="#" class="small-box-footer"><!-- <i class="fa fa-arrow-circle-right"></i>--></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>Telèfon</h3>

                        <p><?=$arbitre->telefon ?></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios-telephone"></i>
                    </div>
                    <a href="#" class="small-box-footer"><!-- <i class="fa fa-arrow-circle-right"></i>--></a>
                </div>
            </div>

            <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->
            <?php if($_SESSION['USUARIO']['nomUser'] == 'david'){ ?><a style="float:right"; href="../app/sessions.txt" target="_blank"><button type="button" class="btn btn-info" id="delete">Sessions</button></a> <?php } ?>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Partits</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="partits" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>idPartit</th>
                                    <th>local</th>
                                    <th>visitant</th>
                                    <th>data</th>
                                    <th>hora</th>
                                    <th>pista</th>
                                    <th>Arbitre</th>
                                    <th>Anotador</th>
                                    <th>Suspes</th>
                                   <!-- <th>Total Partit</th>-->
                                    <th>Acceptat</th>
                                    <th>Pagat</th>
                                    <th>accions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>idPartit</th>
                                    <th>local</th>
                                    <th>visitant</th>
                                    <th>data</th>
                                    <th>hora</th>
                                    <th>pista</th>
                                    <th>Arbitre</th>
                                    <th>Anotador</th>
                                    <th>Suspes</th>
                                    <!--<th>Total Partit</th>-->
                                    <th>Acceptat</th>
                                    <th>Pagat</th>
                                    <th>accions</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php

                                if($partits){
                                    foreach ($partits as $partit) {
                                        if ($partit->idArbitre == $arbitre->idArbitre){
                                            $isPagat = $partit->isPagatArbitre;
                                            $isAcceptat = $partit->isAcceptatArbitre;
                                        }else{
                                            $isPagat = $partit->isPagatAnotador;
                                            $isAcceptat = $partit->isAcceptatAnotador;
                                        }

                                        echo "<tr>";
                                        echo "<td>".$partit->idPartit."</td>";
                                        echo "<td>".$partit->local."</td>";
                                        echo "<td>".$partit->visitant."</td>";
                                        echo "<td>".$partit->data."</td>";
                                        echo "<td>".$partit->hora."</td>";
                                        echo "<td>".$partit->pista."</td>";
                                        echo "<td>".$partit->arbitre." €</td>";
                                        echo "<td>".$partit->anotador." €</td>";
                                        echo "<td>";
                                        if($partit->isSuspes == '1') {
                                            echo "SI";
                                        } else {
                                            echo "NO";
                                        }
                                        echo "</td>";
                                        //echo "<td>".($partit->arbitre+$partit->anotador)." €</td>";

                                        if($isAcceptat == '1') {
                                            echo "<td style='background-color: #BCF5A9';>SI</td>";
                                        } else {
                                            echo "<td style='background-color:#ffcccc';>NO</td>";
                                        }
                                        echo "<td>";
                                        if($isPagat == '1') {
                                            echo "SI";
                                        } else {
                                            echo "NO";
                                        }
                                        echo "</td>";
                                        echo "<td>
                        <a href='".base_url()."arbitresBalles/editarPartitArbitre/$partit->idPartit/$arbitre->idArbitre'><button type='button' class='btn btn-success'> <i class='fa fa-edit'></i></button></a>
                      </td></tr>";
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Delegat</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="delegat" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>idPartitDelegat</th>
                                    <th>data</th>
                                    <th>hora</th>
                                    <th>pista</th>
                                    <th>Delegat</th>
                                    <th>accions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>idPartitDelegat</th>
                                    <th>data</th>
                                    <th>hora</th>
                                    <th>pista</th>
                                    <th>Delegat</th>
                                    <th>accions</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                if($delegats){
                                    foreach ($delegats as $delegat) {
                                        echo "<tr><td>".$delegat->idPartitDelegat."</td>";
                                        echo "<td>".$delegat->data."</td>";
                                        echo "<td>".$delegat->hora."</td>";
                                        echo "<td>".$delegat->pista."</td>";
                                        echo "<td>".$delegat->delegat." €</td>";
                                        echo "<td>
                        <a href='".base_url()."arbitresBalles/editarPartit/$delegat->idPartitDelegat'><button type='button' class='btn btn-success'> <i class='fa fa-edit'></i></button></a>
                      </td></tr>";
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <script>

        $(function () {

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

        })
    </script>

<?php $this->load->view('footer'); ?>