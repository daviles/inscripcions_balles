<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('../app/PHPEXCEL/Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();
if ($registros > 0) {
   //Informacion del excel
  $objPHPExcel->getProperties()
        ->setCreator("David Sierra")
        ->setLastModifiedBy("David Sierra")
        ->setTitle("Exportar excel desde mysql")
        ->setSubject("Equips")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("David Sierra con  phpexcel")
        ->setCategory("Equips"); 
	$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension("H")->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension("I")->setAutoSize(true);
    //$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
    //$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
    //$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);  
 $styleArray = array(
    'font' => array(
        'bold' => true,
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    ),
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
        'rotation' => 90,
        'startcolor' => array(
            'argb' => 'FFA0A0A0',
        ),
        'endcolor' => array(
            'argb' => 'FFFFFFFF',
        ),
    ),
);
 
 if ($idDivisio == 3 || $idDivisio == 4){
$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($styleArray);
  $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'EQUIP')
            ->setCellValue('B1', 'CAPITA')
            ->setCellValue('C1', 'EMAIL')
            ->setCellValue('D1','TELEFON')
            ->setCellValue('E1','PREFERENCIA')
            ->setCellValue('F1','DIRECCIO PISTA')
            ->setCellValue('G1','HORA PARTIT')
            ->setCellValue('H1','POBLACIO')
            ->setCellValue('I1','OBSERVACIONS');
   $i = 2;    
    foreach ($registros as $registro) {
        
      $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $registro->nomEquip)
            ->setCellValue('B'.$i, $registro->nomJugador.' '.$registro->cognomsJugador)
            ->setCellValue('C'.$i, $registro->email)
            ->setCellValue('D'.$i, $registro->telefon)
            ->setCellValue('E'.$i, $registro->preferenciaPartit)
            ->setCellValue('F'.$i, $registro->direccioPista)
            ->setCellValue('G'.$i, $registro->horariPista)
            ->setCellValue('H'.$i, $registro->poblacioPista)
            ->setCellValue('I'.$i, $registro->observacions);
  
      $i++;
       
   }
   }else{
   $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);
  $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'EQUIP')
            ->setCellValue('B1', 'CAPITA')
            ->setCellValue('C1', 'EMAIL')
            ->setCellValue('D1','TELEFON')
             ->setCellValue('E1','PREFERENCIA');
   $i = 2;    
   foreach ($registros as $registro) {
        
      $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $registro->nomEquip)
            ->setCellValue('B'.$i, $registro->nomJugador.' '.$registro->cognomsJugador)
            ->setCellValue('C'.$i, $registro->email)
            ->setCellValue('D'.$i, $registro->telefon)
            ->setCellValue('E'.$i, $registro->preferenciaPartit);
  
      $i++;
       
   }
   }
}
switch ($idDivisio) {
    case 1:
        $nomDivisio = "primera_masculina";
        break;
    case 2:
        $nomDivisio = "segona_masculina";
        break;
    case 5:
        $nomDivisio = "tercera_masculina";
        break;
    case 3:
        $nomDivisio = "primera_femenina";
        break;
    case 4:
        $nomDivisio = "segona_femenina";
        break;
    
    default:
        # code...
        break;
}
header('Content-Type: application/vnd.ms-excel');
header("Content-type: application/x-msexcel"); 
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header('Content-Disposition: attachment;filename=DadesEquips_'.$nomDivisio.'.xls');
header('Cache-Control: max-age=0');
  

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;
//mysql_close ();

?>