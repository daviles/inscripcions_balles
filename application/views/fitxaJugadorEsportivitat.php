<?php defined('BASEPATH') OR exit('No direct script access allowed');
 $this->load->view('header'); 
if (!array_key_exists('USUARIO', $_SESSION )) { header("location:".base_url()."index.php"); }
//if ($idDivisio == ''){ header('location:index.php'); } ?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#formSancio").click(function(){
      $("#novaSancio").slideToggle("slow");
    });
     $('#example').DataTable( {
        //"order": [[ 4, "desc" ]],
        "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true,
        'scrollX'     : true
    } );
		});
</script>

<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      <?php echo "<a href='".base_url()."equiposBalles/esportivitat'><button style='margin-left:2%'; type='button' class='btn btn-warning'><i class='fa fa-arrow-circle-left'></i></button></a>"; ?>  Fitxa Jugador
      </h1>

      <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->
    
    </section>

<?php //echo "<a style='margin-left:2%'; href='../app/exportDadesEquipExcel.php?idDivisio=$idDivisio'><button type='button' class='btn btn-primary' id='delete'>Exportar Dades Equips</button></a>"; ?>

    
<section class="content">
  <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive " src="<?=base_url().$jugador->foto?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?=$jugador->nomJugador?> <?=$jugador->cognomsJugador?></h3>

              <p class="text-muted text-center"><?=$jugador->nomEquip?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Diviso</b> <a class="pull-right"><?=$jugador->nomDivisio?></a>
                </li>
                <li class="list-group-item">
                  <b>Data Neixament</b> <a class="pull-right"><?=$jugador->dataNeixament?></a>
                </li>
                <li class="list-group-item">
                  <b>DNI</b> <a class="pull-right"><?=$jugador->dni?></a>
                </li>
                <li class="list-group-item">
                  <b>Email</b> <a class="pull-right"><?=$jugador->email?></a>
                </li>
                <li class="list-group-item">
                  <b>Telèfon</b> <a class="pull-right"><?=$jugador->telefon?></a>
                </li>
                <li class="list-group-item">
                  <b>Tècniques</b> <a class="pull-right"><?=$esportivitat->tecniques?></a>
                </li>
                <li class="list-group-item">
                  <b>Antiesportives</b> <a class="pull-right"><?=$esportivitat->antiEsportives?></a>
                </li>
                <li class="list-group-item">
                  <b>Desqualificants</b> <a class="pull-right"><?=$esportivitat->desqualificant?></a>
                </li>
                <li class="list-group-item">
                  <b>Observacions</b> <a class="pull-right"><?=$jugador->observacionsEsportivitat?></a>
                </li>
              </ul>


              <button class="btn btn-primary" data-toggle="modal" data-target="#myModalObs" id="formObservacio">
                          <i class="glyphicon glyphicon-edit"></i>
                          </button>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      <div class="row">
        <div class="col-md-8">
          <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Esportivitat</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
            <tr>
              <th>idEsportivitat</th>
              <th>Nom</th>
              <th>Tècniques</th>
              <th>AntiEsportives</th>
              <th>Desqualificants</th>
              <th>Data Partit</th>
              <th>Eliminar</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>idEsportivitat</th>
              <th>Nom</th>
              <th>Tècniques</th>
              <th>AntiEsportives</th>
              <th>Desqualificants</th>
              <th>Data Partit</th>
              <th>Eliminar</th>
            </tr>
        </tfoot>
        <tbody>
          <?php 
        if($lineas){
              foreach ($lineas as $linea) {
                echo "<tr><td>".$linea->idEsportivitat."</td>";
                echo "<td>".$linea->nom." ".$linea->cognoms."</td>";
                echo "<td>".$linea->tecniques."</td>";
                echo "<td>".$linea->antiEsportives."</td>";
                echo "<td>".$linea->desqualificant."</td>";
                echo "<td>".$linea->dataPartit."</td>";
                echo "<td><a href='".base_url()."equiposBalles/eliminarSancio/$linea->idEsportivitat/$linea->idJugador'><button type='button' class='btn btn-danger'><i class='fa fa-trash'></i></button></a></td></tr>";
              }
            } 
          ?>
        </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </div>
</section>
</div>

  <div class="modal fade" id="myModalObs" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Tancar</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Observacions
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
                <form role="form" method="POST" action='<?=base_url()?>equiposBalles/observacionsEsportivitat'>
                  <div class="form-group">
                    <input type="hidden" name="idJugador" id="idJugador" value="<?=$jugador->idJugador?>"/>
                    <label for="observacions">Observacions</label>
                      <textarea  class="form-control" id="observacions" name="observacions"><?=$jugador->observacionsEsportivitat;?>
                      </textarea>
                  </div>
                  <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Tanca
                </button>
                <button type="submit" class="btn btn-primary">
                    Desar
                </button>
            </div>
                  <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
                </form>
            </div>
            
            <!-- Modal Footer -->
            
        </div>
    </div>
</div>


<?php $this->load->view('footer'); ?>