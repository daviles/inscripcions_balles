<?php defined('BASEPATH') OR exit('No direct script access allowed');
 $this->load->view('header'); 
if (!array_key_exists('USUARIO', $_SESSION )) { header("location:".base_url()."index.php"); }
//if ($idDivisio == ''){ header('location:index.php'); } ?>
<script type="text/javascript">
  $(document).ready(function() {
       
      
  } );
</script>
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tarifes
      </h1>
      
      <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->
      <?php if($_SESSION['USUARIO']['nomUser'] == 'david'){ ?><a style="float:right"; href="../app/sessions.txt" target="_blank"><button type="button" class="btn btn-info" id="delete">Sessions</button></a> <?php } ?>
    </section>

<?php //echo "<a style='margin-left:2%'; href='../app/exportDadesEquipExcel.php?idDivisio=$idDivisio'><button type='button' class='btn btn-primary' id='delete'>Exportar Dades Equips</button></a>"; ?>

    
<section class="content">

  <div class="row">
  <div class="col-md-5">
  <div class="box box-danger collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Insertar nova tarifa</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
          <form role="form" method="POST" action='<?=base_url()?>arbitresBalles/newTarifa'>
              <div class="form-group">
                <label for="tipus">Tipus</label>
                  <input type="text" class="form-control" name="tipus" id="tipus" value="">
              </div>
              <div class="form-group">
                <label for="price">Preu</label>
                  <input type="number" min="1" step="any" name="price" id="price" value=""/>
              </div>            
          
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Desar</button>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
  </div>
</div>
</div>
     
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Equip</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
            <tr>
              <th>idTarifa</th>
              <th>Tipus</th>
              <th>Preu</th>
              <th>Accions</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>idTarifa</th>
              <th>Tipus</th>
              <th>Preu</th>
              <th>Accions</th>
            </tr>
        </tfoot>
        <tbody>
           <?php 
            if($tarifes){
              foreach ($tarifes as $tarifa) {
                echo "<tr><td>".$tarifa->idTarifa."</td>";
                echo "<td>".$tarifa->tipus."</td>";
                echo "<td>".$tarifa->preu." €</td>";
                echo "<td>
                        <a href='".base_url()."arbitresBalles/editarTarifa/$tarifa->idTarifa'><button type='button' class='btn btn-success'> <i class='fa fa-edit'></i></button></a>
                        <a href='".base_url()."arbitresBalles/deleteTarifa/$tarifa->idTarifa'><button type='button' class='btn btn-danger'> <i class='fa fa-remove'></i></button>
                      </td></tr>";
              }
            } 
          ?>
        </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

</div>
<script>

  $(function () {

    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker

    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es'
    })

    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es'
    })
     

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
<?php $this->load->view('footer'); ?>