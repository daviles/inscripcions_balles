<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('header');
if($_SESSION['USUARIO'] == '' || $_SESSION['USUARIO']['rol'] != 1){header('location:index.php'); }
//if ($idDivisio == ''){ header('location:index.php'); } ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#formSancio").click(function(){
                $("#novaSancio").slideToggle("slow");
            });
            $('#example').DataTable( {
                //"order": [[ 8 , "desc" ]]
            });
        });
    </script>


    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo "<a href='".base_url()."arbitresBalles/showPistes'><button style='margin-left:2%'; type='button' class='btn btn-primary'><i class='fa fa-users'></i></button></a>"; ?> Dades Partit
                <h3>Pista ID : <?=$pista->idPistaJoc?></h3>
            </h1>

            <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->

        </section>

        <?php //echo "<a style='margin-left:2%'; href='../app/exportDadesEquipExcel.php?idDivisio=$idDivisio'><button type='button' class='btn btn-primary' id='delete'>Exportar Dades Equips</button></a>"; ?>


        <section class="content">
            <div class="row">
                <div class="col-md-8">

                    <!-- Profile Image -->
                    <div class="box box-primary col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">
                        <div class="box-body box-profile">
                            <h3 class="profile-username text-center"></h3>
                            <form role="form" method="POST" action='<?=base_url()?>arbitresBalles/editPistaForm/<?=$pista->idPistaJoc?>'>
                                <p class="text-muted text-center"></p>
                                <?php if($this->session->flashdata('success')){echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('success').'</div>';} ?>
                                <?php if($this->session->flashdata('error')){echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('error').'</div>';} ?>
                                <!--<ul class="list-group list-group-unbordered">-->
                                <div class="form-group col-md-6">
                                    <label for="nom">Nom</label>
                                    <input type="text" class="form-control" name="nom" id="nom" value="<?=$pista->nomPista?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="direccio">Direccio</label>
                                    <input type="text" class="form-control" name="direccio" id="direccio" value="<?=$pista->direccioPista?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="poblacio">Poblacio</label>
                                    <input type="text" class="form-control" name="poblacio" id="poblacio" value="<?=$pista->poblacioPista?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="horari">Horari</label>
                                    <input type="time" class="form-control" name="horari" value="<?=$pista->horariPista?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="isForaSabadell">És Fora de Sabadell?</label>
                                    <select class="form-control select" style="width: 100%;" id="isForaSabadell" name="isForaSabadell">
                                        <option value="0" <?php if($pista->isPistaFora == '0') { ?>selected<?php } ?>>No</option>
                                        <option value="1" <?php if($pista->isPistaFora == '1') { ?>selected<?php } ?>>Sí</option>
                                    </select>

                                </div>
                                <button class="btn btn-primary pull-right" data-toggle="modal" id="formObservacio">
                                    <i class="fa fa-save"></i>
                                </button>
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>

        $(function () {

            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            )

            //Date picker

            $('#datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: 'es'
            })

            $('#datepicker2').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: 'es'
            })


            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            })
        })
    </script>
<?php $this->load->view('footer'); ?>