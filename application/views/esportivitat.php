<?php defined('BASEPATH') OR exit('No direct script access allowed');
 $this->load->view('header'); 
if (!array_key_exists('USUARIO', $_SESSION )) { header("location:".base_url()."index.php"); }
//if ($idDivisio == ''){ header('location:index.php'); } ?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#formSancio").click(function(){
      $("#novaSancio").slideToggle("slow");
    });
     $("#formBanqueta").click(function(){
      $("#novaTecnicaBanqueta").slideToggle("slow");
    });
      //$('#example').DataTable();
      $('#example').DataTable( {
        "order": [[ 4, "desc" ]],
        "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        'scrollX'     : true
    } );

      $('#example2').DataTable( {
        "order": [[ 6, "desc" ]],"lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        'scrollX'     : true
    } );
      
      $("#equip").change(function(){
			var id=$(this).val();
			//var dataString = 'id='+ id;
			var dataString = {
                "id" : id
        };
      console.log(dataString);
			$.ajax
			({
				type: "POST",
				url: "<?=base_url() ?>equiposBalles/selectJugadorsEquip",
				data: dataString,
				//dataType:'json',
				cache: false,
				success: function(html)
				{
					$("#jugador").html(html);
				} 
			});

		});
  } );
</script>
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Esportivitat
      </h1>
      
      <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->
      <?php if($_SESSION['USUARIO']['nomUser'] == 'david'){ ?><a style="float:right"; href="../app/sessions.txt" target="_blank"><button type="button" class="btn btn-info" id="delete">Sessions</button></a> <?php } ?>
    </section>

<?php //echo "<a style='margin-left:2%'; href='../app/exportDadesEquipExcel.php?idDivisio=$idDivisio'><button type='button' class='btn btn-primary' id='delete'>Exportar Dades Equips</button></a>"; ?>

    
<section class="content">
  <div class="row">
  <div class="col-md-5">
  <div class="box box-danger collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Insertar nova sanció</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
          <form role="form" method="POST" action='<?=base_url()?>equiposBalles/sancionar'>
              <div class="form-group">
                <label for="equip">Equip</label>
                <select class="form-control select2" style="width: 100%;" id="equip" name="equip" >
                  <?php foreach ($equips as $equip) {
                    echo "<option value='".$equip->idEquip."' >".$equip->nomEquip."</option>";
                  } ?>
                </select>
              </div>
              <div class="form-group">
                <label for="jugador">Jugador</label>
                <select class="form-control select2" style="width: 100%;" id="jugador" name="jugador" >
                    <option selected='selected'>--Select Jugador--</option>
                </select>
              </div>
              <div class="form-group">
                <label for="dataPartit">Data Partit</label>
                  <!--<input type="text" class="form-control" name="dataPartit" id="dataPartit" placeholder="dd/mm/yyyy" value="">-->
                    <div class="input-group date">
                       <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" class="form-control pull-right" name="dataPartit" id="datepicker">
                </div>
              </div>
              <div class="form-group">
                <label for="quantitat">Quantitat</label>
                  <input type="number" class="form-control" name="quantitat" id="quantitat" value="">
              </div>
              <div class="form-group">
                <label for="sancio">Sanció</label>
                <select class="form-control select2" id="sancio" name="sancio" >
                    <option value="1">Tècnica</option>
                    <option value="2">Antiesportiva</option>
                    <option value="3">Desqualificant</option>
                </select>
              </div>
              
          
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Desar</button>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
  </div>
</div>
  <div class="col-md-5">
  <div class="box box-warning collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Insertar nova Tècnica Banqueta</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
         <form role="form" method="POST" action='<?=base_url()?>equiposBalles/insertTecnicaBanqueta'>
              <div class="form-group">
                <label for="equip">Equip</label>
                <select class="form-control select2" id="equip_tecnica" name="equip_tecnica" >
                  <?php foreach ($equips as $equip) {
                    echo "<option value='".$equip->idEquip."' >".$equip->nomEquip."</option>";
                  } ?>
                </select>
              </div>
              <div class="form-group">
                <label for="dataPartit">Data Partit</label>
                  <!--<input type="text" class="form-control" name="dataPartit" id="dataPartit" placeholder="dd/mm/yyyy" value="">-->
                    <div class="input-group date">
                       <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" class="form-control pull-right" name="dataPartit" id="datepicker2">
                </div>
              </div>
              <div class="form-group">
                <label for="sancio">Sanció</label>
                  <input type="text" readonly class="form-control" name="tecnicaBanqueta" id="tecnicaBanqueta"  value="Tècnica Banqueta">
              </div>
              <div class="form-group">
                <label for="quantitat">Quantitat</label>
                  <input type="number" class="form-control" name="quantitat" id="quantitat" value="">
              </div>
               <div class="box-footer">
                <button type="submit" class="btn btn-primary">Desar</button>
              </div>
                  <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
        </form>
        </div>
        <!-- /.box-body -->
      </div>
  </div>
</div>
     
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Equip</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
            <tr>
              <th>idEsportivitat</th>
              <th>Nom</th>
              <th>idEquip</th>
              <th>Equip</th>
              <th>Tècniques</th>
              <th>AntiEsportives</th>
              <th>Desqualificants</th>
              <th>Fitxa Jugador</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>idEsportivitat</th>
              <th>Nom</th>
              <th>idEquip</th>
              <th>Equip</th>
              <th>Tècniques</th>
              <th>AntiEsportives</th>
              <th>Desqualificants</th>
              <th>Fitxa Jugador</th>
            </tr>
        </tfoot>
        <tbody>
           <?php 
            if($esportivitat){
              foreach ($esportivitat as $jugador) {
                if( ( $jugador->tecniques % 3 ) == 0 ) {
                  $red = 'class="red"';
                }else{
                  $red = '';
                }
                echo "<tr><td>".$jugador->idEsportivitat."</td>";
                echo "<td>".$jugador->nom." ".$jugador->cognoms."</td>";
                echo "<td>".$jugador->idEquip."</td>";
                echo "<td>".$jugador->nomEquip."</td>";
                echo "<td ".$red." data-original-title='999' data-container='body' data-toggle='tooltip' data-placement='bottom' title='prueba'>".$jugador->tecniques."</td>";
                echo "<td>".$jugador->antiEsportives."</td>";
                echo "<td>".$jugador->desqualificant."</td>";
                echo "<td><a href='".base_url()."equiposBalles/veureFitxaJugadorEsportivitat/$jugador->idJugador'><button type='button' class='btn btn-success'> <i class='fa fa-eye'></i></button></a></td></tr>";
              }
            } 
          ?>
        </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Equip</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
            <tr>
              <th>idEquip</th>
              <th>Equip</th>
              <th>Tècniques</th>
              <th>AntiEsportives</th>
              <th>Desqualificants</th>
              <th>Tecniques Banqueta</th>
              <th>Total</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>idEquip</th>
              <th>Equip</th>
              <th>Tècniques</th>
              <th>AntiEsportives</th>
              <th>Desqualificants</th>
              <th>Tecniques Banqueta</th>
              <th>Total</th>
            </tr>
        </tfoot>
        <tbody>
           <?php 
        if($esportivitatEquips){
              foreach ($esportivitatEquips as $equip) {
                echo "<tr><td>".$equip->idEquip."</td>";
                echo "<td>".$equip->nomEquip."</td>";
                echo "<td>".$equip->tecniques."</td>";
                echo "<td>".$equip->antiEsportives."</td>";
                echo "<td>".$equip->desqualificant."</td>";
                echo "<td>".$equip->banqueta."</td>";
                echo "<td>".$equip->total."</td></tr>";
              }
            } 
          ?>
        </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
    </section>
    <!-- /.content -->

</div>
<script>

  $(function () {

    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker

    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es'
    })

    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es'
    })
     

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
<?php $this->load->view('footer'); ?>