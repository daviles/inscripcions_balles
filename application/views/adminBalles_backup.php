<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php /*include('header.php');*/ $this->load->view('header'); ?>
<?php 
if($_SESSION['USUARIO'] == '' || $_SESSION['USUARIO']['rol'] != 1){header('location:index.php');}
$idEquip = $_SESSION['USUARIO']['idEquip'];
?>
	<?php
		//$con = "select equip.nomEquip from equip where idEquip <> 1 and actiu='ACT'";
		//print_r($query);// = mysql_query($con);
    $equiposBuscador = array();
    foreach ($query as $q) {
     array_push($equiposBuscador, $q->nomEquip);
    }
    //print_r($equiposBuscador);
	?>
    <script>
		$(function() {
		<?php
     foreach ($equiposBuscador as $row) {
        # code...
       //echo $row;
       $elementos[]= '"'.$row.'"';
      /*while($row= mysql_fetch_array($equiposBuscador)) {//se reciben los valores y se almacenan en un arreglo
	      		$elementos[]= '"'.$row.'"';*/
		 	}
			$arreglo= implode(', ', $elementos);//junta los valores del array en una sola cadena de texto
		?>	
			var availableTags=new Array(<?php echo $arreglo; ?>);//imprime el arreglo dentro de un array de javascript
			$( "#tags" ).autocomplete({
				source: availableTags
			});
		});
	</script>
  <script type="text/javascript">
  $(document).ready(function() {
     $('#example').DataTable({
        "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]]
     });
  } );
</script>
<div class="container">
  <div class="btn-group">
    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">Categories <span class="caret"></span>
    </button>
   
    <ul class="dropdown-menu" role="menu">
      <li><a href="<?=base_url()?>equiposBalles/equipsPerDivisio/1">Primera Masculina</a></li>
      <li><a href="<?=base_url()?>equiposBalles/equipsPerDivisio/2">Segona Masculina</a></li>
      <li><a href="<?=base_url()?>equiposBalles/equipsPerDivisio/5">Tercera Masculina</a></li>
      <li><a href="<?=base_url()?>equiposBalles/equipsPerDivisio/3">Primera Femenina</a></li>
      <li><a href="<?=base_url()?>equiposBalles/equipsPerDivisio/4">Segona Femenina</a></li>
    </ul>
</div>
<div class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle"
            data-toggle="dropdown">
            Accions <span class="caret"></span>
    </button>
   
    <ul class="dropdown-menu" role="menu">
      <li data-toggle="modal" data-target="#myModalNormRebutManual"><a href="#">Rebut Manual</a></li>
      <li><a href="#">Fitxa Jugador</a></li>
      <li><a target="_blank" href="<?=base_url()?>equiposBalles/exportJugadorsExcel">Exportar Jugadors</a></li>
      <?php if ($_SESSION['USUARIO']['idUser'] == 1 || $_SESSION['USUARIO']['idUser'] == 5) { ?>
      <li><a href="<?=base_url()?>equiposBalles/votacioAdmin" id="delete">Votacio All Star</a></li>
      <?php } ?>
    </ul>
</div>
<div class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Llistats <span class="caret"></span>
    </button>
   
    <ul class="dropdown-menu" role="menu">
      <li><a href="<?=base_url()?>equiposBalles/lastInscripcions">Ultimes Inscripcions</a></li>
      <?php if($_SESSION['USUARIO']['idUser'] == 1 || $_SESSION['USUARIO']['idUser'] == 5){ ?>
        <li><a href="<?=base_url()?>equiposBalles/esportivitat">Esportivitat</a></li> 
      <?php } ?>
      <!--<li><a href="<?=base_url()?>equiposBalles/equipsPerDivisio/2">Segona Masculina</a></li>
      <li><a href="<?=base_url()?>equiposBalles/equipsPerDivisio/5">Tercera Masculina</a></li>
      <li><a href="<?=base_url()?>equiposBalles/equipsPerDivisio/3">Primera Femenina</a></li>-->
    </ul>
</div>
<!-- Button trigger modal -->
<? if ($_SESSION['USUARIO']['idUser'] == 1){ ?>
  <div class="btn-group">
    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">Equips<span class="caret"></span>
    </button>
   
    <ul class="dropdown-menu" role="menu">
      <li data-toggle="modal" data-target="#myModalNorm"><a href="#">Crear Equip Nou</a></li>
      <li data-toggle="modal" data-target="#myModalNormActivarEquip"><a href="#">Activar Equip</a></li>
    </ul>
  </div>
<? } ?>
<!-- Modal -->
<div class="modal fade" id="myModalNormRebutManual" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Tancar</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Rebut Manual
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
                <form class="bs-example bs-example-form" role="form" method="POST" target="_blank" action='<?=base_url()?>equiposBalles/printRebutManual'>
                  <div class="form-group">
                    <label id="" for="nomEquip">Nom Equip</label>
                      <select class="form-control" name="nomEquip">
                        <? foreach ($equiposBuscador as $equipo) {
                            echo "<option value='".$equipo."'>".$equipo."</option>";                        
                          } 
                         
                       ?>
                     </select>
                  </div>
                  <div class="form-group">
                    <label>Número de fitxes: </label>
                      <select class="form-control" id="fitxa" name ="fitxa">
                          <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                          </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Import concret</label>
                      <input type="number" class="form-control" name="import" placeholder="Introdueix un import concret..."></br>
                  </div>
                  <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Tanca
                </button>
                <button type="submit" class="btn btn-primary">
                    Crear Rebut
                </button>
            </div>
                  <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
                </form>
            </div>
            
            <!-- Modal Footer -->
            
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Tancar</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Equip Nou
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
                <form role="form" method="POST" action='<?=base_url()?>login/crearEquipo'>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nom Equip</label>
                      <input type="text" class="form-control"
                      id="nomEquip" name="nomEquip" placeholder="equip"/>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Usuari</label>
                      <input type="text" class="form-control"
                          id="usuari" name="usuari" placeholder="usuari"/>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Contrassenya</label>
                      <input type="text" class="form-control"
                          id="pass" name="pass" placeholder="Password" value="<?php echo $pass; ?>"/>
                  </div>
                  <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Tanca
                </button>
                <button type="submit" class="btn btn-primary">
                    Crea Equip
                </button>
            </div>
                  <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
                </form>
            </div>
            
            <!-- Modal Footer -->
            
        </div>
    </div>
</div>

<!-- Modal jugadors historic -->
<div class="modal fade" id="myModalNormActivarEquip" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Tancar</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Activar equip Temporada anterior
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
            <form role="form" method="POST" action='<?=base_url()?>equiposBalles/activateEquipTemporadaAnterior'>

                  <?php foreach ($equipsInactius as $equip) {
                    echo "<div class='radio' style='margin-bottom:15px !important;'>";
                    echo "<label class='radio control-label' ><input type='radio' class='form-control' style='margin-top:-8px!important;' name='equip' value='".$equip->idEquip."'>".
                    $equip->nomEquip."</label></div>";
                    //echo "<input type='hidden' name=idEquip value='".$equip->idEquip."'>";
                  } ?> 
                               
              <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Tanca
                </button>
                <button type="submit" class="btn btn-primary">
                    Activar
                </button>
            </div>
                  <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
                </form>
            </div>
            
            <!-- Modal Footer -->
            
        </div>
    </div>
</div>
<!--<a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=1&&nomDivisio=Primera Masculina"><button type="button" class="btn btn-success" id="delete">Primera Masculina</button></a>
<a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=2&&nomDivisio=Segona Masculina"><button type="button" class="btn btn-success" id="delete">Segona Masculina</button></a>
<a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=5&&nomDivisio=Tercera Masculina"><button type="button" class="btn btn-success" id="delete">Tercera Masculina</button></a>
<a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=3&&nomDivisio=Primera Femenina"><button type="button" class="btn btn-success" id="delete">Primera Femenina</button></a>-->
<!--<a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=4&&nomDivisio=Segona Femenina"><button type="button" class="btn btn-success" id="delete">Segona Femenina</button></a>-->
<!--<a style="margin-left:1%"; href="../app/exportJugadorsExcel.php"><button type="button" class="btn btn-warning" id="delete">Exportar Jugadors</button></a>-->

<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>
<?php if($_SESSION['USUARIO']['nomUser'] == 'david'){ ?><a style="float:right"; href="../app/sessions.txt" target="_blank"><button type="button" class="btn btn-info" id="delete">Sessions</button></a> <?php } ?>
<!--<div style="padding: 2% 2%;" >
   <form class="bs-example bs-example-form" role="form" action="../app/getIdEquip.php?$_GET['nomDesti'];" method="GET">
      <div class="row">
         <div class="col-md-10">
            <div class="input-group">
               <input type="text" class="form-control" id="tags" name="nomEquip" placeholder="Introdueix un equip...">
               <span class="input-group-btn">
                  <button style="margin-left: 5%;" class="btn btn-warning" type="submit">Buscar Equip</button>-->
                   <!--<a style="margin-left:2%"; href="rebutManual.php?"><button type="button" class="btn btn-primary" id="delete">Rebut Manual</button></a>
                   <a style="margin-left:2%"; href="printFitxa.php?"><button type="button" class="btn btn-primary" id="delete">Fitxa Jugador</button></a>-->
                  <!-- <a style="margin-left:2%"; href="votacioAdmin.php"><button type="button" class="btn btn-danger" id="delete">Votacio All Star</button></a>-->
              <!--</span>
            </div>
         </div>
      </div>
   </form>
</div>-->
<div style="margin-top:4%"; class="panel panel-danger">
 
  <div style="text-align:center"; class="panel-heading">Equips Inscrits<span class="red"> <?php echo $numEquips-1; ?></span> / Jugadors Totals : <span class="red"><?php echo $numJugadorsTotals; ?></span></div>
   <table id="example" class="display" cellspacing="0" width="100%"> 
        <thead>
            <tr>
              <th>ID EQUIP</th>
              <th>Nom Equip</th>
              <th>Jugadors</th>
              <th>Usuari</th>
              <th>Contrasenya</th>
              <th>Divisio</th>
              <th>Observacions</th>
              <th>Pagat</th>
              <th>Pagament</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>ID EQUIP</th>
              <th>Nom Equip</th>
              <th>Jugadors</th>
              <th>Usuari</th>
              <th>Contrasenya</th>
              <th>Divisio</th>
              <th>Observacions</th>
              <th>Pagat</th>
              <th>Pagament</th>
            </tr>
        </tfoot>
        <tbody>
          <?php 
          foreach ($equipsTotal as $equip) {
            echo "<tr>";
            echo "<td>".$equip['idEquip']."</td>";
            echo "<td><a href='".base_url()."equiposBalles/mostraDadesEquip/".$equip['idEquip']."'>".$equip['nomEquip']."</td>";
            echo "<td>".$equip['numJugadorsEquip']."</td>";
            echo "<td>".$equip['nomUser']."</td>";
            echo "<td>".$equip['pass']."</td>";
            echo "<td>".$equip['nomDivisio']."</td>";
            echo "<td>".$equip['observacions']."</td>";
            echo "<td>".$equip['isPagat']."</td>";
            echo "<td>".$equip['pagament']."</td>";
            echo "</tr>";
          
          }
          ?>
        </tbody>
    </table>

 
</div>
</div>

 <?php $this->load->view('footer'); ?>