<?php defined('BASEPATH') OR exit('No direct script access allowed');
 $this->load->view('header'); 
if($_SESSION['USUARIO'] == '' || $_SESSION['USUARIO']['rol'] != 1){header('location:index.php'); }
/*if ($_SESSION['USUARIO']['rol'] == 1){
	$idEquip = $_REQUEST['idEquip'];
	$_SESSION['USUARIO']['idEquip'] = $idEquip;	
	//echo "es rol 1";
}else{
	$idEquip = $_SESSION['USUARIO']['idEquip'];
}*/
//if($_SESSION['USUARIO'] == '' || $_SESSION['USUARIO']['rol'] != 1 || $_SESSION['USUARIO']['idEquip'] == 16){header('location:index.php');}
//$idDivisio = getIdDivsiobyIdEquip($_SESSION['USUARIO']['idEquip']);

//print_r($_SESSION);

//$numVotacio = getAllVotacioByIdEquip($_SESSION['USUARIO']['idEquip']);
//echo $idDivisio;
//print_r($numVotacio);
?>
<script type="text/javascript">
        var votacioJugadorsPrimera = <?php echo json_encode($votosJugadoresPrimera); ?>;
        var votacioJugadorsSegona = <?php echo json_encode($votosJugadoresSegunda); ?>;
        var votacioJugadorsTercera = <?php echo json_encode($votosJugadoresTercera); ?>;
        var votosJugadoresPrimeraFemenina = <?php echo json_encode($votosJugadoresPrimeraFemenina); ?>;
        var votosJugadoresSegonaFemenina = <?php echo json_encode($votosJugadoresSegonaFemenina); ?>;
        console.log(votacioJugadorsPrimera);
        //var jugadors = JSON.parse(votacioJugadorsPrimera);
		//console.log(jugadors);

</script>
<style>
	label
	{
	font-weight:bold;
	padding:10px;
	}
  .select
  {
  border: 1px solid #DBE1EB;
  font-size: 14px;
  font-family: Arial, Verdana;
  padding-left: 7px;
  padding-right: 7px;
  padding-top: 10px;
  padding-bottom: 10px;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  -o-border-radius: 4px;
  background: #FFFFFF;
  background: linear-gradient(left, #FFFFFF, #F7F9FA);
  background: -moz-linear-gradient(left, #FFFFFF, #F7F9FA);
  background: -webkit-linear-gradient(left, #FFFFFF, #F7F9FA);
  background: -o-linear-gradient(left, #FFFFFF, #F7F9FA);
  color: #2E3133;
  }
  
  .select:hover
  {
  border-color: red;
  }
  
  .select option
  {
  border: 1px solid #DBE1EB;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  -o-border-radius: 4px;
  }
  
  .select option:hover
  {
  background: #FC4F06;
  background: linear-gradient(left, #FC4F06, #D85F2B);
  background: -moz-linear-gradient(left, #FC4F06, #D85F2B);
  background: -webkit-linear-gradient(left, #FC4F06, #D85F2B);
  background: -o-linear-gradient(left, #FC4F06, #D85F2B);
  font-style: italic;
  color: #FFFFFF;
  }

.picture {
    background-size: 150px 150px;
    background-repeat: no-repeat;
    margin-left : 41%;
    margin-top: 3%;
    border: 1px solid black;
}
</style>
<script language="javascript">
	$(document).ready(function()
	{
		$('#jugadors').on('change', function () {
		    $('select').removeClass('activeList');
		    $(this).addClass('activeList');
		    var url = $('option:selected', this).attr('myImg');
		    url = '<?=base_url()?>'+url;
		   $(".picture").css({
			    'background-image': 'url(' + url + ')',
			    'background-repeat': 'no-repeat',
			    'width' : '150px',
			    'height' : '150px'
			});
		});
		$(".divisio").change(function(){
			var id=$(this).val();
			var dataString = 'id='+ id;
			$.ajax
			({
				type: "POST",
				url: "<?=base_url() ?>equiposBalles/select_dependientes_proceso_equipo",
				data: dataString,
				//dataType:'json',
				cache: false,
				success: function(html)
				{
					$(".equip").html(html);
				} 
			});

		});
		$(".equip").change(function()
		{
			var id=$(this).val();
			var dataString = 'id='+ id;
			//alert(dataString);
			$.ajax
			({
				type: "POST",
				url: "<?=base_url() ?>equiposBalles/select_dependientes_proceso",
				data: dataString,
				//dataType:'json',
				cache: false,
				success: function(html)
				{
					$(".jugadors").html(html);
				} 
			});

		});
	});

	var chart;
	$(document).ready(function() {
		$('#graficaPrimera').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Votacions All Stars 2017'
	        },
	        subtitle: {
	            text: 'Primera Masculina'
	        },
	        xAxis: {
	            type: 'category',
	            labels: {
	                rotation: -45,
	                style: {
	                    fontSize: '13px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Vots'
	            }
	        },
	        legend: {
	            enabled: false
	        },
	        tooltip: {
	            pointFormat: 'Vots Totals: <b>{point.y}</b>'
	        },
	        series: [{
	            name: 'Vots Jugadors',
	                data: (function() {
	                   var data = [];
	                    <?php
	                        for($i = 0 ;$i<count($votosJugadoresPrimera);$i++){
	                    ?>
	                    data.push(['<?php echo $votosJugadoresPrimera[$i]->jugador.'-'.$votosJugadoresPrimera[$i]->nomEquip;?>',<?php echo $votosJugadoresPrimera[$i]->votos;?>]);
	                    <?php } ?>
	                return data;
	                })()
	            ,
	            dataLabels: {
	                enabled: true,
	                rotation: 0,
	                color: 'black',
	                align: 'center',
	                format: '{point.y}', // one decimal
	                y: 25, // 10 pixels down from the top
	                style: {
	                    fontSize: '13px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        }]
	    });
$(".chart-export").each(function() {
  var jThis = $(this),
      chartSelector = jThis.data("chartSelector"),
      chart = $(chartSelector).highcharts();

  $("*[data-type]", this).each(function() {
    var jThis = $(this),
        type = jThis.data("type");
    if(Highcharts.exporting.supports(type)) {
      jThis.click(function() {
        chart.exportChartLocal({ type: type });
      });
    }
    else {
      jThis.attr("disabled", "disabled");
    }
  });
});
	$('#graficaSegona').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Votacions All Stars 2017'
	        },
	        subtitle: {
	            text: 'Segona Masculina'
	        },
	        xAxis: {
	            type: 'category',
	            labels: {
	                rotation: -45,
	                style: {
	                    fontSize: '13px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Vots'
	            }
	        },
	        legend: {
	            enabled: false
	        },
	        tooltip: {
	            pointFormat: 'Vots Totals: <b>{point.y}</b>'
	        },
	        series: [{
	            name: 'Vots Jugadors',
	                data: (function() {
	                   var data = [];
	                    <?php
	                        for($i = 0 ;$i<count($votosJugadoresSegunda);$i++){
	                    ?>
	                    data.push(['<?php echo $votosJugadoresSegunda[$i]->jugador.'-'.$votosJugadoresSegunda[$i]->nomEquip;?>',<?php echo $votosJugadoresSegunda[$i]->votos;?>]);
	                    <?php } ?>
	                return data;
	                })()
	            ,
	            dataLabels: {
	                enabled: true,
	                rotation: 0,
	                color: 'black',
	                align: 'center',
	                format: '{point.y}', // one decimal
	                y: 25, // 10 pixels down from the top
	                style: {
	                    fontSize: '13px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        }]
	    });
	$('#graficaTercera').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Votacions All Stars 2017'
	        },
	        subtitle: {
	            text: 'Tercera Masculina'
	        },
	        xAxis: {
	            type: 'category',
	            labels: {
	                rotation: -45,
	                style: {
	                    fontSize: '13px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Vots'
	            }
	        },
	        legend: {
	            enabled: false
	        },
	        tooltip: {
	            pointFormat: 'Vots Totals: <b>{point.y}</b>'
	        },
	        series: [{
	            name: 'Vots Jugadors',
	                data: (function() {
	                   var data = [];
	                    <?php
	                        for($i = 0 ;$i<count($votosJugadoresTercera);$i++){
	                    ?>
	                    data.push(['<?php echo $votosJugadoresTercera[$i]->jugador.'-'.$votosJugadoresTercera[$i]->nomEquip;?>',<?php echo $votosJugadoresTercera[$i]->votos;?>]);
	                    <?php } ?>
	                return data;
	                })()
	            ,
	            dataLabels: {
	                enabled: true,
	                rotation: 0,
	                color: 'black',
	                align: 'center',
	                format: '{point.y}', // one decimal
	                y: 25, // 10 pixels down from the top
	                style: {
	                    fontSize: '13px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        }]
	    });
	$('#graficaPrimeraFemenina').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Votacions All Stars 2016'
	        },
	        subtitle: {
	            text: 'Lliga Primera Femenina'
	        },
	        xAxis: {
	            type: 'category',
	            labels: {
	                rotation: -45,
	                style: {
	                    fontSize: '13px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Vots'
	            }
	        },
	        legend: {
	            enabled: false
	        },
	        tooltip: {
	            pointFormat: 'Vots Totals: <b>{point.y}</b>'
	        },
	        series: [{
	            name: 'Vots Jugadors',
	                data: (function() {
	                   var data = [];
	                    <?php
	                        for($i = 0 ;$i<count($votosJugadoresPrimeraFemenina);$i++){
	                    ?>
	                    data.push(['<?php echo $votosJugadoresPrimeraFemenina[$i]->jugador.'-'.$votosJugadoresPrimeraFemenina[$i]->nomEquip;?>',<?php echo $votosJugadoresPrimeraFemenina[$i]->votos;?>]);
	                    <?php } ?>
	                return data;
	                })()
	            ,
	            dataLabels: {
	                enabled: true,
	                rotation: 0,
	                color: 'black',
	                align: 'center',
	                format: '{point.y}', // one decimal
	                y: 25, // 10 pixels down from the top
	                style: {
	                    fontSize: '13px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        }]
	    });
$('#graficaSegonaFemenina').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Votacions All Stars 2017'
	        },
	        subtitle: {
	            text: 'Lliga Segona Femenina'
	        },
	        xAxis: {
	            type: 'category',
	            labels: {
	                rotation: -45,
	                style: {
	                    fontSize: '13px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Vots'
	            }
	        },
	        legend: {
	            enabled: false
	        },
	        tooltip: {
	            pointFormat: 'Vots Totals: <b>{point.y}</b>'
	        },
	        series: [{
	            name: 'Vots Jugadors',
	                data: (function() {
	                   var data = [];
	                    <?php
	                        for($i = 0 ;$i<count($votosJugadoresSegonaFemenina);$i++){
	                    ?>
	                    data.push(['<?php echo $votosJugadoresSegonaFemenina[$i]->jugador.'-'.$votosJugadoresSegonaFemenina[$i]->nomEquip;?>',<?php echo $votosJugadoresSegonaFemenina[$i]->votos;?>]);
	                    <?php } ?>
	                return data;
	                })()
	            ,
	            dataLabels: {
	                enabled: true,
	                rotation: 0,
	                color: 'black',
	                align: 'center',
	                format: '{point.y}', // one decimal
	                y: 25, // 10 pixels down from the top
	                style: {
	                    fontSize: '13px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        }]
	    });
});		


	function jsFunction()
	{
		//lert('ha cambiado');
	}

</script>
<?php
//print_r($_SESSION['USUARIO']);

?>
<body>
<div class="container">
	<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>
<?php if ($_SESSION['USUARIO']['rol'] == 1){  
	//echo "<a href='adminBalles.php'><button style='margin-left:2%'; type='button' class='btn btn-warning'>Tornar a Administracio</button></a>";
	 }else{ 
	//echo "<a href='formInscripcio.php'><button style='float:right'; type='button' class='btn btn-warning'>Inscrire mes jugadors</button></a>";
	 } ?>
<div style="margin-top:4%"; class="panel panel-success">
  <div style="text-align:center;font-size:22px;" class="panel-heading"><b>VOTACIO</b></span>
 </div>
 	<div class="row col-md-12">
	<form role="form" action="<?=base_url() ?>equiposBalles/votacioAllstar" method="POST"><br>
		<div class="col-md-8"> 
		  	<select name="divisio" id="divisio" class="select divisio">
			<option selected="selected">--Select Divisio--</option>
				<?php
					foreach ($divisions as $divisio) { 
						$id=$divisio->idDivisio;
						$data=$divisio->nomDivisio;
						echo '<option value="'.$id.'">'.$data.'</option>';
					}
				 ?>
			</select>
		</div> <br/>
		<div class="col-md-8">
			  <select name="equip" id="equip" class="select equip">
				<option selected="selected">--Select Equip--</option>
			</select> 
		</div>
		<div class="col-md-8"> 
			<select name="jugadors" class="select jugadors" id="jugadors">
				<option selected="selected">--Select Player--</option>
			</select>
		</div></br> 
		<button style='margin-left:10%;margin-bottom:5%;' type='sumbit' class='btn btn-success'>Votar</button>
		<div class="col-md-1 picture"></div>
		
		</div>

	</form>
</div>
<br><br>
	<div class="col-md-12" id="graficaPrimera">
		 <div class=" chart-export" data-chart-selector="#example-1">
	        <button type="button" class="btn btn-default" data-type="image/svg+xml">SVG</button>
	        <button type="button" class="btn btn-default" data-type="image/png">PNG</button>
	        <button type="button" class="btn btn-default" data-type="image/jpeg">JPEG</button>
	        <button type="button" class="btn btn-default" data-type="application/pdf">PDF</button>
	        <button type="button" class="btn btn-default" data-type="text/csv">CSV</button>
	        <button type="button" class="btn btn-default" data-type="application/vnd.ms-excel">XLS</button>
	    </div>
	</div>
	<div class="col-md-12" id="graficaSegona"></div>
	<div class="col-md-12" id="graficaTercera"></div>
	<div class="col-md-12" id="graficaPrimeraFemenina"></div>
	<div class="col-md-12" id="graficaSegonaFemenina"></div>
</div>
</div>
<?php $this->load->view('footer'); ?>	
