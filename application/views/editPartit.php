<?php defined('BASEPATH') OR exit('No direct script access allowed');
 $this->load->view('header');
if ((!array_key_exists('USUARIO', $_SESSION )) || (($_SESSION['USUARIO']['rol'] != 1) & ($_SESSION['USUARIO']['rol'] != 3))) { header('location:index.php'); }
//if ($idDivisio == ''){ header('location:index.php'); } ?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#formSancio").click(function(){
      $("#novaSancio").slideToggle("slow");
    });
      $('#example').DataTable( {
        //"order": [[ 8 , "desc" ]]
      });
		});
</script>

<?php
    $perm = '';
    if($_SESSION['USUARIO']['rol'] == 3) {
        $perm = 'readonly';
        $disabled = 'disabled';
    }

?>
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php if ($_SESSION['USUARIO']['rol'] == 3) {
              echo "<a href='" . base_url() . "login/loginUser/1' ><button style='margin-left:2%'; type='button' class='btn btn-primary'><i class='fa fa-users'></i></button></a>";
           }
          if ($_SESSION['USUARIO']['rol'] == 1) {
              echo "<a href='" . base_url() . "arbitresBalles/importarPartits'><button style='margin-left:2%'; type='button' class='btn btn-primary'><i class='fa fa-users'></i></button></a>";
          }
            ?> Dades Partit
        <h3>Partit ID : <?=$partit->idPartit?></h3>
      </h1> 

      <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->
    
    </section>

<?php //echo "<a style='margin-left:2%'; href='../app/exportDadesEquipExcel.php?idDivisio=$idDivisio'><button type='button' class='btn btn-primary' id='delete'>Exportar Dades Equips</button></a>"; ?>

    
<section class="content">
     <div class="row">
        <?php if ($partit->isAcceptatArbitre == 1) { ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-green">
                    <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Partit Arbitre Acceptat</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                        </div>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        <!-- /.col -->
        <?php } else { ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-red">
                    <span class="info-box-icon"><i class="fa fa-thumbs-o-down"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Partit Arbitre no Acceptat</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 60%"></div>
                        </div>
                        <span class="progress-description">
                        <?php //echo "<a href='" . base_url() . "arbitresBalles/AcceptarPartit/".$partit->idPartit."/".$partit->idArbitre."/1/0' >Confirmar Acceptació</a>"; ?>
                      </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        <?php } ?>
         <?php if ($partit->isAcceptatAnotador == 1) { ?>
             <div class="col-md-3 col-sm-6 col-xs-12">
                 <div class="info-box bg-green">
                     <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

                     <div class="info-box-content">
                         <span class="info-box-text">Partit Anotador Acceptat</span>

                         <div class="progress">
                             <div class="progress-bar" style="width: 100%"></div>
                         </div>
                     </div>
                     <!-- /.info-box-content -->
                 </div>
                 <!-- /.info-box -->
             </div>
             <!-- /.col -->
         <?php } else { ?>
             <div class="col-md-3 col-sm-6 col-xs-12">
                 <div class="info-box bg-red">
                     <span class="info-box-icon"><i class="fa fa-thumbs-o-down"></i></span>

                     <div class="info-box-content">
                         <span class="info-box-text">Partit Anotador no Acceptat</span>

                         <div class="progress">
                             <div class="progress-bar" style="width: 60%"></div>
                         </div>
                         <span class="progress-description">
                        <?php //echo "<a href='" . base_url() . "arbitresBalles/AcceptarPartit/".$partit->idPartit."/".$partit->idAnotador."/0/1' >Confirmar Acceptació</a>"; ?>
                      </span>
                     </div>
                     <!-- /.info-box-content -->
                 </div>
                 <!-- /.info-box -->
             </div>
         <?php } ?>
        <!-- /.col -->
     </div>
  <div class="row">
        <div class="col-md-8">
          
          <!-- Profile Image -->
          <div class="box box-primary col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center"></h3>
              <form role="form" method="POST" action='<?=base_url()?>arbitresBalles/editPartitForm/<?=$partit->idPartit?>'>
              <p class="text-muted text-center"></p>
              <?php if($this->session->flashdata('success')){echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('success').'</div>';} ?>
              <?php if($this->session->flashdata('error')){echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('error').'</div>';} ?>
              <!--<ul class="list-group list-group-unbordered">-->
              <div class="form-group col-md-6">
                <label for="tipus">Local</label>
                <input type="text" class="form-control" <?=$perm?> name="local" id="local" value="<?=$partit->local?>">
              </div>
              <div class="form-group col-md-6">
                <label for="tipus">Visitant</label>
                <input type="text" class="form-control" <?=$perm?> name="visitant" id="visitant" value="<?=$partit->visitant?>">
              </div>
              <div class="form-group col-md-6">
                <label for="tipus">Data</label>
                <input type="text" class="form-control" <?=$perm?> name="dataPartit" id="datepicker" value="<?=$partit->data?>">
              </div>
              <div class="form-group col-md-6">
                <label for="tipus">Hora</label>
                <input type="time" class="form-control" <?=$perm?> name="horaPartit" value="<?=$partit->hora?>">
              </div>
              <div class="form-group col-md-6">
                <label for="tipus">Pista</label>
                <input type="text" class="form-control" <?=$perm?> name="pista" value="<?=$partit->pista?>"/>
              </div>
              <div class="form-group col-md-6">
                <label for="tipus">Suspès</label>
                  <select class="form-control select" style="width: 100%;" id="suspes" name="suspes" <?=$disabled?>>
                      <option value="0" <?php if($partit->isSuspes == '0') { ?>selected<?php } ?>>No</option>
                      <option value="1" <?php if($partit->isSuspes == '1') { ?>selected<?php } ?>>Si</option>
                  </select>
              </div>
               <div class="form-group col-md-6">
                <label for="tipus">Àrbitre</label>
                    <select class="form-control select2" style="width: 100%;" id="arbitre" name="arbitre" >
                      <option value="0">no designado</option>
                      <?php foreach ($arbitres as $arbitre) {
                        $selected = "";
                        if($arbitre->idArbitre == $partit->idArbitre){ $selected = "selected"; }
                        echo "<option value='".$arbitre->idArbitre."' ".$selected.">".$arbitre->nomArbitre."</option>";
                      } ?>
                    </select>
                </div>
                <div class="form-group col-md-6">
                <label for="tipus">Tarfa Àrbitre</label>
                  <select class="form-control select2" style="width: 100%;" id="tarifaArbitre" name="tarifaArbitre" <?=$disabled?>>
                    <option value="0">no designado</option>
                      <?php foreach ($tarifes as $tarifa) {
                        $selected = "";
                        if($tarifa->idTarifa == $partit->idTarifaArbitre){ $selected = "selected"; }
                        echo "<option value='".$tarifa->idTarifa."' ".$selected.">".$tarifa->tipus."(".$tarifa->preu." €)</option>";
                      } ?>
                    </select>
                </div>
               <div class="form-group col-md-6">
                <label for="tipus">Anotador</label>
                  <select class="form-control select2" style="width: 100%;" id="anotador" name="anotador" >
                    <option value="0">no designado</option>
                      <?php foreach ($arbitres as $arbitre) {
                        $selected = "";
                        if($arbitre->idArbitre == $partit->idAnotador){ $selected = "selected"; }
                        echo "<option value='".$arbitre->idArbitre."' ".$selected.">".$arbitre->nomArbitre."</option>";
                      } ?>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="tipus">Tarifa Anotador</label>
                    <select class="form-control select2" style="width: 100%;" id="tarifaAnotador" name="tarifaAnotador" <?=$disabled?> >
                      <option value="0">no designado</option>
                        <?php foreach ($tarifes as $tarifa) {
                          $selected = "";
                          if($tarifa->idTarifa == $partit->idTarifaAnotador){ $selected = "selected"; }
                          echo "<option value='".$tarifa->idTarifa."' ".$selected.">".$tarifa->tipus."(".$tarifa->preu." €)</option>";
                        } ?>
                    </select>
                </div>

                  <div class="form-group col-md-6">
                <label for="tipus">Masculí/Femení</label>
                  <select class="form-control select" style="width: 100%;" id="isFemeni" name="isFemeni" <?=$disabled?>>
                      <option value="0" <?php if($partit->isFemeni == '0') { ?>selected<?php } ?>>Masculí</option>
                      <option value="1" <?php if($partit->isFemeni == '1') { ?>selected<?php } ?>>Femení</option>
                  </select>

                </div>
              <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModalObs" id="formObservacio">
                  <i class="fa fa-save"></i>
              </button>
            </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
</section>
</div>
<script>

  $(function () {

    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker

    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es'
    })

    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es'
    })
     

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
<?php $this->load->view('footer'); ?>