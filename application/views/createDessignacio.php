<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('header');
if ((!array_key_exists('USUARIO', $_SESSION )) || (($_SESSION['USUARIO']['rol'] != 1) & ($_SESSION['USUARIO']['rol'] != 3))) { header('location:index.php'); }
//if ($idDivisio == ''){ header('location:index.php'); } ?>

<?php
$perm = '';
if($_SESSION['USUARIO']['rol'] == 3) {
    $perm = 'readonly';
    $disabled = 'disabled';
}

?>
    <style>
    #draggable { width: 150px; height: 150px; padding: 0.5em; }
    table th, table td {
        width: 500px;
        height:100px;
        padding: 5px;
    }
    table span {
        display:block;
        /*background-color: black;*/
        border: 3px solid black;
        color: fff;
        height: 30px;
        width: 100%;
    }
</style>
    <script>

        $( function() {
           // $(".movil").draggable();
        } );
        $(document).ready(function () {
            $('.event').on("dragstart", function (event) {
                var dt = event.originalEvent.dataTransfer;
                dt.setData('Text', $(this).attr('id'));
            });
            $('table td').on("dragenter dragover drop", function (event) {
                event.preventDefault();
                if (event.type === 'drop') {
                    var data = event.originalEvent.dataTransfer.getData('Text', $(this).attr('id'));

                    de = $('#' + data).detach();
                    if (event.originalEvent.target.tagName === "SPAN") {
                        de.insertBefore($(event.originalEvent.target));
                    }
                    else {
                        de.appendTo($(this));
                    }
                };
            });
        });
        $(document).ready(function(){

            $("#codigo").click(function(){

                fcookie='dessignacio_feta';
                var code = document.getElementById("#our_table");
                //var code = $("#our_table").html();
                var table = code.innerHTML;
                //alert(table);
                //console.log(code);

                var tables = document.getElementById("#our_table");
                var firstTable = tables;
                var tableAttr = firstTable.attributes;
               // console.log(tableAttr);

                var tableString = "<" + firstTable.nodeName.toLowerCase();

                for (var i = 0; i < tableAttr.length; i++) {
                    tableString += " " + tableAttr[i].name + "='" + tableAttr[i].value + "'";

                }

                tableString += ">" + firstTable.innerHTML + "</" + firstTable.nodeName.toLowerCase() + ">";
                var p = tableString;
                //alert(p);
                //console.log(p);
                document.cookie=fcookie+"="+tableString;
                //console.log(document.cookie);

                $(".modal-body #dessignacio").val( tableString );
                $("#myModalDessignacio").modal();
             //   window.location.href = "<?php echo site_url('arbitresBalles/desarDessignacio'); ?>" ;


            });

        });

        function franja1(sel) {
            var x = document.getElementById("franja1");
            var m = document.getElementById("mailDelegat1");
            var arbitre = sel.value;
            var partsOfStr = arbitre.split(',');
            x.innerHTML = partsOfStr[0];
            $('#mailDelegat1').val(partsOfStr[1]);
        }

        function franja2(sel) {
            var x = document.getElementById("franja2");
            var m = document.getElementById("mailDelegat2");
            var arbitre = sel.value;
            var partsOfStr = arbitre.split(',');
            x.innerHTML = partsOfStr[0];
            $('#mailDelegat2').val(partsOfStr[1]);
        }

        function franja3(sel) {
            var x = document.getElementById("franja3");
            var m = document.getElementById("mailDelegat3");
            var arbitre = sel.value;
            var partsOfStr = arbitre.split(',');
            x.innerHTML = partsOfStr[0];
            $('#mailDelegat3').val(partsOfStr[1]);
        }


        </script>

    <div class="content-wrapper">

        <section class="content">
            <div class="row">
                <div class="col-md-5">
                    <div class="box box-danger box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">La dessignacio s'enviarà als següents mails</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" id="mailsEnviament">
                            <?php
                                foreach ($mailArbitres as $mail) {
                                    if ($mail->mail) {
                                        echo $mail->mail . "</br>";
                                    }
                                }
                            ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <div class="btn-group">
                    <button type="button" class="btn-lg btn-primary" id="codigo"">Desar </button>
                    <button type="button" class="btn-lg btn-primary" id="codigo" onclick="getCode_Send()">Desar i Enviar</button>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="tipus">Delegat 1</label>
                    <select class="form-control select2" style="width: 100%;" id="delegat1" name="delegat1" onchange="franja1(this)" >
                        <option value="0">no designado</option>
                        <?php foreach ($arbitres as $arbitre) {
                            echo "<option value='".$arbitre->nomArbitre.",".$arbitre->mail."'>".$arbitre->nomArbitre."</option>";
                        } ?>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="tipus">Delegat 2</label>
                    <select class="form-control select2" style="width: 100%;" id="delegat2" name="delegat2" onchange="franja2(this)" >
                        <option value="0">no designado</option>
                        <?php foreach ($arbitres as $arbitre) {
                            echo "<option value='".$arbitre->nomArbitre.",".$arbitre->mail."'>".$arbitre->nomArbitre."</option>";
                        } ?>
                    </select>
                </div><div class="form-group col-md-3">
                    <label for="tipus">Delegat 3</label>
                    <select class="form-control select2" style="width: 100%;" id="delegat3" name="delegat3" onchange="franja3(this)">
                        <option value="0">no designado</option>
                        <?php foreach ($arbitres as $arbitre) {
                            echo "<option value='".$arbitre->nomArbitre.",".$arbitre->mail."'>".$arbitre->nomArbitre."</option>";
                        } ?>
                    </select>
                </div>
            </div>
            <div class="row">
        <?php
            $i = 0;
            foreach ($partits as $partit) {  $i++ ?>

                    <div class="col-md-3 col-sm-6 col-xs-12 movil" >
                        <div class="info-box bg-green event" id="item<?=$i?>"draggable="true">
                            <span class="info-box-icon" style="font-size:30px;"><?=$partit->hora ?></span>

                            <div class="info-box-content">
                                <span class="info-box-text" style="font-size:20px;"><strong><?=$partit->pista?></strong></span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                                <div><strong><?=strtoupper($partit->local)?> vs <?=strtoupper($partit->visitant)?></strong></div>
                                <div><strong><?=$partit->nomArbitre?> / <?=$partit->nomAnotador?></strong></div>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                <!-- /.col -->

         <?php   }  ?>
            </div>
            <table id="#our_table" border="1">
                <tr>
                    <th style="text-align: center;"><p>PISTA 1</p><span id="franja1"></span></th>
                    <th style="text-align: center;"><p>PISTA 2</p><span id="franja2" ></span></th>
                    <th style="text-align: center;"><p>PISTA 3</p><span id="franja3" ></span>
                </tr>
                <tr>
                    <td>
                        <!--<span class="event" id="item1" draggable="true">Item 1</span>-->
                    </td>
                    <td>
                        <!--<span class="event" id="item2" draggable="true">Item 2</span>
                        <span class="event" id="item3" draggable="true">Item 3</span>-->
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <div class="modal fade" id="myModalDessignacio" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close"
                                    data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Tancar</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">
                                Dessignació
                            </h4>
                        </div>

                        <!-- Modal Body -->
                        <div class="modal-body">

                            <form role="form" method="POST" action='<?=base_url()?>arbitresBalles/desarDessignacio'>
                                <div class="form-group">
                                    <input type="hidden" class="form-control"
                                           id="dessignacio" name="dessignacio" placeholder=""  />
                                </div>
                                <div class="form-group">
                                <input type="hidden" class="form-control"
                                       id="mails" name="mails" placeholder="" value="<?php print base64_encode(serialize($mailArbitres)); ?>"  />
                                </div>
                                <div class="form-group">
                                    <label for="nom">Delegat 1</label>
                                    <input type="text" class="form-control"
                                           id="mailDelegat1" name="mailDelegat1" placeholder="" value=""  />
                                </div>
                                <div class="form-group">
                                    <label for="nom">Delegat 2</label>
                                    <input type="text" class="form-control"
                                           id="mailDelegat2" name="mailDelegat2" placeholder="" value=""  />
                                </div>
                                <div class="form-group">
                                    <label for="nom">Delegat 3</label>
                                    <input type="text" class="form-control"
                                           id="mailDelegat3" name="mailDelegat3" placeholder="" value=""  />
                                </div>
                                <div class="form-group">
                                    <label for="nom">Nom</label>
                                    <input type="text" class="form-control" name="nom" id="nom" value="">
                                </div>
                                <div class="form-group">
                                    <label for="data">Data Dessignacio</label>
                                    <input type="text" class="form-control" name="data" id="datepicker" value="">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">
                                        Tanca
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        Desar
                                    </button>
                                </div>
                                <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
                            </form>
                        </div>

                        <!-- Modal Footer -->

                    </div>
                </div>
            </div>

        </section>

    </div>
    <script>

        $(function () {

            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            )

            //Date picker

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: 'es'
            })

            $('#datepicker2').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: 'es'
            })

            $('#datepicker_from').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: 'es'
            })

            $('#datepicker_to').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: 'es'
            })


            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            })
            CKEDITOR.replace('editor1')
            $('.textarea').wysihtml5()
        })

    </script>

<?php $this->load->view('footer'); ?>