<?php defined('BASEPATH') OR exit('No direct script access allowed');
 $this->load->view('header'); 
if($_SESSION['USUARIO'] == '' || $_SESSION['USUARIO']['rol'] != 1){header('location:index.php'); }
//if ($idDivisio == ''){ header('location:index.php'); } ?>
<script type="text/javascript">
  $(document).ready(function() {
      $('#example').DataTable( {
        "order": [[ 8 , "desc" ]],
        "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
        'paging'      : true,
        'lengthChange': true,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      });
  } );
</script>




<div class="content-wrapper">
  <?php if ($_SESSION['USUARIO']['rol'] == 1){  
  //echo "<a href='".base_url()."login/loginUser/1'><button style='margin-left:2%'; type='button' class='btn btn-warning'>Tornar a Administracio</button></a>";
   }else{ 
  echo "<a href='formInscripcio.php'><button style='float:right'; type='button' class='btn btn-warning'>Inscrire mes jugadors</button></a>";
   } ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Últimes Inscripcions
      </h1>
      
      <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->
<?php if($_SESSION['USUARIO']['nomUser'] == 'david'){ ?><a style="float:right"; href="../app/sessions.txt" target="_blank"><button type="button" class="btn btn-info" id="delete">Sessions</button></a> <?php } ?>
    </section>

    <!-- Main content -->
    <section class="content">


      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Inscripcions</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
            <tr>
              <th>idJugador</th>
              <th>Nom</th>
              <th>Dni</th>
              <th>Email</th>
              <th>Telefon</th>
              <th>Equip</th>
              <th>idEquip</th>
              <th>Data Inscripcio</th>
            </tr>
        </thead>
        <tfoot>
           <tr>
              <th>idJugador</th>
              <th>Nom</th>
              <th>Dni</th>
              <th>Email</th>
              <th>Telefon</th>
              <th>Equip</th>
              <th>idEquip</th>
              <th>Data Inscripcio</th>
            </tr>
        </tfoot>
                <tbody>
          <?php 
        if($jugadors){
              foreach ($jugadors as $jugador) {
                echo "<tr><td>".$jugador->idJugador."</td>";
                echo "<td>".$jugador->nomJugador." ".$jugador->cognomsJugador."</td>";
                echo "<td>".$jugador->dni."</td>";
                echo "<td>".$jugador->email."</td>";
                echo "<td>".$jugador->telefon."</td>";
                echo "<td>".$jugador->nomEquip."</td>";
                echo "<td>".$jugador->idEquip."</td>";
                echo "<td>".$jugador->dataIntroduccio."</td>";
              }
            } 
          ?>
        </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<?php //echo "<a style='margin-left:2%'; href='../app/exportDadesEquipExcel.php?idDivisio=$idDivisio'><button type='button' class='btn btn-primary' id='delete'>Exportar Dades Equips</button></a>"; ?>

<?php $this->load->view('footer'); ?>