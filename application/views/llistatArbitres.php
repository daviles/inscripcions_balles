<?php defined('BASEPATH') OR exit('No direct script access allowed');
 $this->load->view('header'); 
if (!array_key_exists('USUARIO', $_SESSION )) { header("location:".base_url()."index.php"); }
//if ($idDivisio == ''){ header('location:index.php'); } ?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#formSancio").click(function(){
      $("#novaSancio").slideToggle("slow");
    });
     $("#formBanqueta").click(function(){
      $("#novaTecnicaBanqueta").slideToggle("slow");
    });
      //$('#example').DataTable();
      $('#example').DataTable( {
        "order": [[ 1, "asc" ]],
        "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        'scrollX'     : true,

    } );

      
      $("#equip").change(function(){
      var id=$(this).val();
      //var dataString = 'id='+ id;
      var dataString = {
                "id" : id
        };
      console.log(dataString);
      $.ajax
      ({
        type: "POST",
        url: "<?=base_url() ?>equiposBalles/selectJugadorsEquip",
        data: dataString,
        //dataType:'json',
        cache: false,
        success: function(html)
        {
          $("#jugador").html(html);
        } 
      });

    });
    $(function () {
      $("#showNewUser").click(function () {
          if ($(this).is(":checked")) {
              $("#newUser").show();
          } else {
              $("#newUser").hide();
          }
      });
    });
  } );

</script>
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Llistat Àrbitres
      </h1>
      
      <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->
      <?php if($_SESSION['USUARIO']['nomUser'] == 'david'){ ?><a style="float:right"; href="../app/sessions.txt" target="_blank"><button type="button" class="btn btn-info" id="delete">Sessions</button></a> <?php } ?>
    </section>

<?php //echo "<a style='margin-left:2%'; href='../app/exportDadesEquipExcel.php?idDivisio=$idDivisio'><button type='button' class='btn btn-primary' id='delete'>Exportar Dades Equips</button></a>"; ?>

    
<section class="content">
<?php if ($_SESSION['USUARIO']['rol'] == 1) { ?>
  <div class="row">
  <div class="col-md-10">
  <div class="box box-danger collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Insertar nou àrbitre</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
          <form role="form" method="POST" action='<?=base_url()?>arbitresBalles/newArbitre'>
              <div class="form-group col-md-6">
                <label for="tipus">Nom</label>
                  <input type="text" class="form-control" name="nom" id="nom" value="">
              </div>
              <div class="form-group col-md-6">
                <label for="tipus">Cognoms</label>
                  <input type="text" class="form-control" name="cognoms" id="cognoms" value="">
              </div>
              <div class="form-group col-md-6">
                <label for="tipus">Email</label>
                  <input type="text" class="form-control" name="email" id="email" value="">
              </div>
              <div class="form-group col-md-6">
                <label for="tipus">Telefon</label>
                  <input type="text" class="form-control" name="telefon" id="telefon" value="">
              </div>
              <div class="form-group col-md-4">
                <label for="tipus">Arbitre / Anotador</label>
                <div class="form-check form-check-inline">
                      <input class="form-check-input" name="arbitre" type="checkbox" id="inlineCheckbox1" value="1">
                      <label class="form-check-label" for="inlineCheckbox1">Arbitre</label>
                      <input class="form-check-input" name="anotador" type="checkbox" id="inlineCheckbox1" value="1">
                      <label class="form-check-label" for="inlineCheckbox1">Anotador</label>
                </div>
              </div>          
              <div class="form-group col-md-8">
                <label for="tipus">Disponiblitat</label>
                <div class="form-check form-check-inline">
                      <input class="form-check-input" name="dilluns" type="checkbox" id="inlineCheckbox1" value="1">
                      <label class="form-check-label" for="dilluns">Dilluns</label>
                      <input class="form-check-input" name="dimarts" type="checkbox" id="inlineCheckbox1" value="1">
                      <label class="form-check-label" for="dimarts">Dimarts</label>
                      <input class="form-check-input" name="dimecres" type="checkbox" id="inlineCheckbox3" value="1">
                      <label class="form-check-label" for="dimecres">Dimecres</label>
                      <input class="form-check-input" name="dijous" type="checkbox" id="inlineCheckbox2" value="1">
                      <label class="form-check-label" for="dijous">Dijous</label>
                      <input class="form-check-input" name="divendres" type="checkbox" id="inlineCheckbox2" value="1">
                      <label class="form-check-label" for="divendres">Divendres</label>
                      <input class="form-check-input" name="dissabte" type="checkbox" id="inlineCheckbox2" value="1">
                      <label class="form-check-label" for="dissabte">Dissabte</label>
                      <input class="form-check-input" name="diumenge" type="checkbox" id="inlineCheckbox2" value="1">
                      <label class="form-check-label" for="diumenge">Diumenge</label>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="showNewUser">
                    <input type="checkbox" id="showNewUser" name="newUser" value="1"/>
                    Crear usuari?
                </label>
              </div>
              <div id="newUser" style="display: none">
                <div class="form-group col-md-6">
                  <label for="tipus">Nom Usuari</label>
                    <input type="text" class="form-control" name="user" id="user" value="">
                </div>
                <div class="form-group col-md-6">
                  <label for="tipus">Password</label>
                    <input type="text" class="form-control" name="pass" id="pass" value="<?=$pass?>">
                </div>
              </div>
              <div class="form-group col-md-12">
                  <label>Observacions</label>
                  <textarea class="form-control" rows="3" name="observacions" placeholder=""></textarea>
                </div>          
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Desar</button>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
  </div>
</div>
</div>
<?php } ?>
     
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Arbitres</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
            <tr>
              <th>id</th>
              <th>Nom</th>
              <th>Cognoms</th>
              <th>Email</th>
              <th>Telèfon</th>
              <th>Àrbitre</th>
              <th>Anotador</th>
              <th>Disponiblitat</th>
              <th>Observacions</th>
            <?php if ($_SESSION['USUARIO']['rol'] == 1) { ?>
                <th>Accions</th>
            <?php } ?>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>id</th>
              <th>Nom</th>
              <th>Cognoms</th>
              <th>Email</th>
              <th>Telèfon</th>
              <th>Àrbitre</th>
              <th>Anotador</th>
              <th>Disponiblitat</th>
              <th>Observacions</th>
            <?php if ($_SESSION['USUARIO']['rol'] == 1) { ?>
              <th>Accions</th>
            <?php } ?>
            </tr>
        </tfoot>
        <tbody>
           <?php 
            $disponibilitat = '';
            if($arbitres){
              foreach ($arbitres as $arbitre) {
                $disponibilitat = '';$isArbitre = '';$isAnotador = '';
                if ($arbitre->dilluns == '1') {
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-success">DL</button>'; 
                }else{
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-danger">DL</button>'; 
                }
                if ($arbitre->dimarts == '1') {
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-success">DM</button>';
                }else{
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-danger">DM</button>'; 
                }
                 if ($arbitre->dimecres == '1') {
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-success">DC</button>';
                }else{
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-danger">DM</button>'; 
                }
                 if ($arbitre->dijous == '1') {
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-success">DJ</button>';
                }else{
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-danger">DM</button>'; 
                }
                 if ($arbitre->divendres == '1') {
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-success">DV</button>';
                }else{
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-danger">DM</button>'; 
                }
                 if ($arbitre->dissabte == '1') {
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-success">DS</button>';
                }else{
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-danger">DM</button>'; 
                }
                 if ($arbitre->diumenge == '1') {
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-success">DG</button>';
                }else{
                  $disponibilitat = $disponibilitat.'<button type="button" class="btn-xs btn-danger">DM</button>'; 
                }

                if ($arbitre->isArbitre == '1') {
                    $isArbitre = 'SI';
                } else {
                    $isArbitre = 'NO';
                }

                if ($arbitre->isAnotador == '1') {
                    $isAnotador = 'SI';
                } else {
                    $isAnotador = 'NO';
                }

                echo "<tr><td>".$arbitre->idArbitre."</td>";
                echo "<td>".$arbitre->nomArbitre."</td>";
                echo "<td>".$arbitre->cognomsArbitre."</td>";
                echo "<td>".$arbitre->mail."</td>";
                echo "<td>".$arbitre->telefon."</td>";
                echo "<td>".$isArbitre."</td>";
                echo "<td>".$isAnotador."</td>";
                echo "<td>".$disponibilitat."</td>";
                echo "<td>".$arbitre->observacions."</td>";
                if ($_SESSION['USUARIO']['rol'] == 1) {
                echo "<td>
                        <a href='".base_url()."arbitresBalles/showPartitsArbitre/$arbitre->idArbitre'><button type='button' class='btn btn-primary'> <i class='fa fa-euro'></i></button></a>
                        <a href='".base_url()."arbitresBalles/editarArbitre/$arbitre->idArbitre'><button type='button' class='btn btn-success'> <i class='fa fa-edit'></i></button></a>
                        <a href='".base_url()."arbitresBalles/deleteArbitre/$arbitre->idArbitre'><button type='button' class='btn btn-danger'> <i class='fa fa-remove'></i></button></a>
                      </td>";
                }
                echo "</tr>";
              }
            } 
          ?>
        </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

</div>
<script>

  $(function () {

    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker

    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es'
    })

    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es'
    })
     

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
<?php $this->load->view('footer'); ?>