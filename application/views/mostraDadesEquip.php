<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php /*include('header.php');*/ $this->load->view('header'); 
if($_SESSION['USUARIO'] == '' ){header('location:index.php');}
if ($_SESSION['USUARIO']['rol'] == 1){
	//$idEquip = $_REQUEST['idEquip'];
	//$_SESSION['USUARIO']['idEquip'] = $idEquip;	
	//echo "es rol 1";
}else{
	$idEquip = $_SESSION['USUARIO']['idEquip'];
}

/*$equip = getNomEquip();
$nomEquip = $equip['nomEquip'];
$numJugadorsEquip = numJugadorsEquip($idEquip);
if ($numJugadorsEquip == ''){$numJugadorsEquip = '0';}
$arrDadesEquip = getDadesEquip($idEquip);
$arrDadesJugadors = getDadesTriptic($idEquip);
$observacio = getObservacioEquip($idEquip);*/
?>
<script type="text/javascript">
  $(document).ready(function() {
     $('#example').DataTable({
        "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
        'paging'      : true,
        'lengthChange': true,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
     });
  } );
</script>


<div class="content-wrapper">
  <?php if ($_SESSION['USUARIO']['rol'] == 1){  
  //echo "<a href='".base_url()."login/loginUser/1'><button style='margin-left:2%'; type='button' class='btn btn-warning'>Tornar a Administracio</button></a>";
   }else{ 
  echo "<a href='formInscripcio.php'><button style='float:right'; type='button' class='btn btn-warning'>Inscrire mes jugadors</button></a>";
   } ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dades Equip
      </h1>
      <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <?php if ($_SESSION['USUARIO']['rol'] == 1 && $pagament['idEquip'] == 53) {  ?>
                <h3>13</h3>
                <?php } else { ?> 
              <h3><?php echo $numJugadorsEquip; ?></h3>
                 <?php } ?>
              <p>Jugadors Inscrits</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-basketball"></i>
            </div>
            <a href="#" class="small-box-footer"><!-- <i class="fa fa-arrow-circle-right"></i>--></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
               <?php if ($_SESSION['USUARIO']['rol'] == 1 && $pagament['idEquip'] == 53) {  ?>
                <h3>1262.50 €</h3>
                <?php } else { ?> 
                   <h3><?=$pagament['totalPagar']?> €</h3>
                <?php } ?>
              <p>Total a pagar</p>
            </div>
            <div class="icon">
              <i class="ion ion-social-euro"></i>
            </div>
            <a href="#" class="small-box-footer"><!-- <i class="fa fa-arrow-circle-right"></i>--></a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Observacions</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?=$equip->observacions?>
               <button type="button" class="btn-xs btn-success pull-right" data-toggle="modal" data-target="#myModalObservacio"><i class="fa fa-edit">
                </i>
              </button>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->
<?php if($_SESSION['USUARIO']['nomUser'] == 'david'){ ?><a style="float:right"; href="../app/sessions.txt" target="_blank"><button type="button" class="btn btn-info" id="delete">Sessions</button></a> <?php } ?>
    </section>

    <!-- Main content -->
    <section class="content">

  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pagament Equip</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                   <th>EQUIP</th>
                   <th>JUGADORS</th>
                   <th>TOTAL FITXES</th>
                   <th>Pista Propia</th>
                   <th>Esportivitat</th>
                   <th>Desc Equip nou</th>
                   <th>Prefència</th>
                   <th>INSCRIPCIO</th>
                   <th>TOTAL A PAGAR</th>
                   <th>Pagat</th>
                   <?php if ($_SESSION['USUARIO']['rol'] == 1){ ?><th>Pagament</th> <?php } ?> 
                  </tr>
                <?php if ($_SESSION['USUARIO']['rol'] == 1) {  
                    
                    echo "<tr><td>".$pagament['nomEquip']."</td>";
        if ($_SESSION['USUARIO']['rol'] == 1 && $pagament['idEquip'] == 53) {
            echo "<td>13</td>";
        } else { 
            echo "<td>".$numJugadorsEquip."</td>";
        }
        //echo "<td>".$pagament['jugadors']."</td>";
        if ($_SESSION['USUARIO']['rol'] == 1 && $pagament['idEquip'] == 53) {
            echo "<td>715</td>";
        } else { 
            echo "<td>".$pagament['totalFitxes']."</td>";
        }
        //echo "<td>".$pagament['totalFitxes']."</td>";
        echo "<td>".$pagament['pistaPropia']."</td>";
        //echo "<td>".$pagament['esportivitat']."</td>";
        if ($_SESSION['USUARIO']['rol'] == 1 && $pagament['idEquip'] == 53) {
            echo "<td>SI(25%)</td>";
        } else { 
            echo "<td>".$pagament['esportivitat']."</td>";
        }
         if ($_SESSION['USUARIO']['rol'] == 1 && $pagament['idEquip'] == 53) {
            echo "<td>SI(-90€)</td>";
        } else { 
            echo "<td>".$pagament['descompteEquipNou']."</td>";
        }
        //echo "<td>".$pagament['descompteEquipNou']."</td>";
        if ($_SESSION['USUARIO']['rol'] == 1 && $pagament['idEquip'] == 53) {
            echo "<td>NO</td>";
        } else { 
            echo "<td>".$pagament['preferencia']."</td>";
        }
        //echo "<td>".$pagament['preferencia']."</td>";
        if ($_SESSION['USUARIO']['rol'] == 1 && $pagament['idEquip'] == 53) {
            echo "<td>637.50</td>";
        } else { 
            echo "<td>".$pagament['inscripcio']."</td>";
        }
        //echo "<td>".$pagament['inscripcio']."</td>";
        if ($_SESSION['USUARIO']['rol'] == 1 && $pagament['idEquip'] == 53) {
            echo "<td>1262.50</td>";
        } else { 
            echo "<td>".$pagament['totalPagar']."</td>";
        }
        //echo "<td>".$pagament['totalPagar']."</td>";
        echo "<td>".$pagament['isPagat']."</td>";
        echo "<td>".$pagament['pagament']."</td></tr>";

                } else {

                    echo "<tr><td>".$pagament['nomEquip']."</td>";
                    echo "<td>".$pagament['jugadors']."</td>";
                    echo "<td>".$pagament['totalFitxes']."</td>";
                    echo "<td>".$pagament['pistaPropia']."</td>";
                    echo "<td>".$pagament['esportivitat']."</td>";
                    echo "<td>".$pagament['descompteEquipNou']."</td>";
                    echo "<td>".$pagament['preferencia']."</td>";
                    echo "<td>".$pagament['inscripcio']."</td>";
                    echo "<td>".$pagament['totalPagar']."</td>";
                    echo "<td>".$pagament['isPagat']."</td>";
                    echo "<td>".$pagament['pagament']."</td></tr>";

                    }

                ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>



      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Equip</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
            <tr>
              <th>JUGADOR</th>
              <!--<th>Foto</th>-->
              <th>DNI</th>
              <th>Nom</th>
              <th>Edat</th>
              <th>Email</th>
              <th>Telefon</th>
              <th>Tipus fitxa</th>
              <th>Accions</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>JUGADOR</th>
              <!--<th>Foto</th>-->
              <th>DNI</th>
              <th>Nom</th>
              <th>Edat</th>
              <th>Email</th>
              <th>Telefon</th>
              <th>Tipus fitxa</th>
              <th>Accions</th>
        </tfoot>
                <tbody>
          <?php $i = 1; 
          if ($jugadors) {
            foreach ($jugadors as $jugador) {
              echo "<tr>";
              echo "<td>".$i."</td>";
              echo "<td>".$jugador->dni."</td>";
              echo "<td>".strtoupper($jugador->nomJugador." ".$jugador->cognomsJugador)."</td>";
              echo "<td>".$jugador->dataNeixament."</td>";
              echo "<td>".$jugador->email."</td>";
              echo "<td>".$jugador->telefon."</td>";
              echo "<td>".$jugador->isEntrenador."</td>";
              echo "<td><a href='".base_url()."equiposBalles/editJugador/".$jugador->idJugador."'><button type='button' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></button></a>
                        <a href='".base_url()."equiposBalles/deleteJugador/".$jugador->idJugador."/".$pagament['idEquip']."'><button type='button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove-sign' aria-hidden='true'></span></button></a></td>";
              echo "</tr>";
              $i++;
            }
          }
          ?>
        </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->









	<?php if ($_SESSION['USUARIO']['rol'] == 1){ ?>  
<!--    <div class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle"
            data-toggle="dropdown">
            Accions <span class="caret"></span>
    </button>
   
    <ul class="dropdown-menu" role="menu">
      <li><a href='<?=base_url()?>equiposBalles/printTriptic/<?=$pagament['idEquip']?>' target='_blank'>Imprimir Triptic</a></li>
      <li data-toggle="modal" data-target="#myModalObservacio"><a href="#">Afegir Observacio</a></li>
      <li><a href='<?=base_url()?>equiposBalles/printRebut/<?=$pagament['idEquip']?>' target='_blank'>Imprimir Rebut</a></li>
      <li data-toggle="modal" data-target="#myModalDivisio"><a href="#">Canviar Divisió</a></li>
    </ul>
    <a href='<?=base_url()?>login/loginUser/1'><button style='float:right'; type='button' class='btn btn-warning'>Tornar a Administracio</button></a>
</div>-->
		
 <?php }else{ 
		echo "<a href='formInscripcio.php'><button style='float:right'; type='button' class='btn btn-warning'>Inscrire mes jugadors</button></a>";
	 } ?>
<!-- Modal --> 
<div class="modal fade" id="myModalObservacio">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Tancar</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Observació
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
                <form role="form" method="POST" action='<?=base_url()?>equiposBalles/afegirObservacio/<?=$pagament['idEquip']?>'>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Observació</label>
                      <input type="text" class="form-control"
                      id="observacio" name="observacio" placeholder="observacio" value="<?=$equip->observacions?>"/>
                  </div>
                  <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Tanca
                </button>
                <button type="submit" class="btn btn-primary">
                    Afegir
                </button>
            </div>
                  <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
                </form>
            </div>
            
            <!-- Modal Footer -->
            
        </div>
    </div>
</div>
<!-- Modal --> 
<div class="modal fade" id="myModalDivisio" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Tancar</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Divisió
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
                <form role="form" method="POST" action='<?=base_url()?>equiposBalles/canviarDivisio/<?=$pagament['idEquip']?>'>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Divisió</label>
                      <select class="form-control" id="divisio" name ="divisio">
                          <option value="0">Escull nova divisió</option>
                            <option value="1" <? if($equip->idDivisio == 1 ) { ?> selected <? } ?> >Primera Masculina</option>
                            <option value="2" <? if($equip->idDivisio == 2 ) { ?> selected <? } ?> >Segona Masculina</option>
                            <option value="5" <? if($equip->idDivisio == 5 ) { ?> selected <? } ?> >Tercera Masculina</option>
                            <option value="3" <? if($equip->idDivisio == 3 ) { ?> selected <? } ?> >Primera Femenina</option>
                            <option value="4" <? if($equip->idDivisio == 4 ) { ?> selected <? } ?> >Segona Femenina</option>
                        </select>
                        <input type="hidden" name="idEquip" value="<?=$pagament['idEquip']?>">
                  </div>
                  <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Tanca
                </button>
                <button type="submit" class="btn btn-primary">
                    Canviar
                </button>
            </div>
                  <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
                </form>
            </div>
            
            <!-- Modal Footer -->
            


<?php $this->load->view('footer'); ?>