<?php defined('BASEPATH') OR exit('No direct script access allowed');
 $this->load->view('header'); 
if (!array_key_exists('USUARIO', $_SESSION )) { header("location:".base_url()."index.php"); }
//if ($idDivisio == ''){ header('location:index.php'); } ?>
<script type="text/javascript">
  $(document).ready(function() {

      $('#partits').DataTable( {
        "order": [[ 1, "asc" ]],
        "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        'scrollX'     : true,

    } );
      $('#delegat').DataTable( {
          "order": [[ 1, "asc" ]],
          "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false,
          'scrollX'     : true,

      } );
    } );
</script>

<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Partits
      </h1>

    </section>

<?php //echo "<a style='margin-left:2%'; href='../app/exportDadesEquipExcel.php?idDivisio=$idDivisio'><button type='button' class='btn btn-primary' id='delete'>Exportar Dades Equips</button></a>"; ?>

    <section class="content-header">
        <div class="col-md-6">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-yellow">
                    <!--<div class="widget-user-image">
                        <img class="img-circle" src="../dist/img/user7-128x128.jpg" alt="User Avatar">
                    </div>-->
                    <!-- /.widget-user-image -->
                    <h3 class="widget-user-username"><?=$arbitre->nomArbitre?> <?=$arbitre->cognomsArbitre?></h3>
                    <h5 class="widget-user-desc"><?=$arbitre->mail?></h5>
                    <?php
                    echo "<a href='".base_url()."arbitresBalles/printRebutArbitre/$arbitre->idArbitre'><button type='button' class='btn btn-danger'> <i class='fa fa-print'></i></button></a>";
                    echo "<a href='".base_url()."arbitresBalles/enviarMailArbitre/$arbitre->idArbitre/$dataInicial/$dataFinal'><button type='button' class='btn btn-danger'> <i class='fa fa-envelope'></i></button></a>";
                    ?>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">.
                        <li><a href="#">Arbitre <b>( Masculins : <?=$countArbitre?> / Femenins : <?=$countArbitreFem?>)</b>
                                <span class="pull-right badge bg-blue"><?=$totalArbitre?> €</span></a></li>
                        <li><a href="#">Anotador <b>(Masculins : <?=$countAnotador?> / Femenins : <?=$countAnotadorFem?>)</b>
                                <span class="pull-right badge bg-aqua"><?=$totalAnotador?> €</span></a></li>
                        <li><a href="#">Delegat <b>(<?=$countDelegat?> franjes)</b>
                                <span class="pull-right badge bg-green"><?=$totalDelegat?> €</span></a></li>
                        <li><a href="#">Total <span class="pull-right badge bg-red"><?=$total?> €</span></a></li>
                    </ul>
                </div>
            </div>
            <!-- /.widget-user -->
        </div>

        <div class="col-md-5">
          <div class="box box-danger collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Buscar per Dates</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
          <form role="form" method="POST" action='<?=base_url()?>arbitresBalles/showPartitsArbitreByDate'>
              <div class="form-group col-md-6">
                <input type="hidden" name="idArbitre" value="<?=$arbitre->idArbitre?>">
                <label for="tipus">Data Inicial</label>
                <input type="text" class="form-control" name="dataInicial" id="datepicker" required value="">
              </div>
              <div class="form-group col-md-6">
                <label for="tipus">Data Final</label>
                <input type="text" class="form-control" name="dataFinal" id="datepicker2" required value="">
              </div>           
          
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Buscar</button>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
  </div>
</div>

    </section>
<section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Partits</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="partits" class="table table-bordered table-hover">
              <thead>
                  <tr>
                    <th>idPartit</th>
                    <th>local</th>
                    <th>visitant</th>
                    <th>data</th>
                    <th>hora</th>
                    <th>pista</th>  
                    <th>Arbitre</th>
                    <th>Anotador</th>
                    <th>Suspes</th>
                    <th>Total Partit</th>
                    <th>accions</th>
                  </tr>
              </thead>
            <tfoot>
              <tr>
                <th>idPartit</th>
                <th>local</th>
                <th>visitant</th>
                <th>data</th>
                <th>hora</th>
                <th>pista</th>
                <th>Arbitre</th>
                <th>Anotador</th>
                <th>Suspes</th>
                <th>Total Partit</th>
                <th>accions</th>
              </tr>
          </tfoot>
        <tbody>
           <?php 
            if($partits){
              foreach ($partits as $partit) {
                echo "<tr><td>".$partit->idPartit."</td>";
                echo "<td>".$partit->local."</td>";
                echo "<td>".$partit->visitant."</td>";
                echo "<td>".$partit->data."</td>";
                echo "<td>".$partit->hora."</td>";
                echo "<td>".$partit->pista."</td>";
                echo "<td>".$partit->arbitre." €</td>";
                echo "<td>".$partit->anotador." €</td>";
                echo "<td>".$partit->isSuspes."</td>";
                echo "<td>".($partit->arbitre+$partit->anotador)." €</td>";
                echo "<td>
                        <a href='".base_url()."arbitresBalles/editarPartit/$partit->idPartit'><button type='button' class='btn btn-success'> <i class='fa fa-edit'></i></button></a>
                        <a href='".base_url()."arbitresBalles/deletePartit/$partit->idPartit'><button type='button' class='btn btn-danger'> <i class='fa fa-remove'></i></button></a>
                      </td></tr>";
              }
            } 
          ?>
        </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Delegat</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="delegat" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>idPartitDelegat</th>
                            <th>data</th>
                            <th>hora</th>
                            <th>pista</th>
                            <th>Delegat</th>
                            <th>accions</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>idPartitDelegat</th>
                            <th>data</th>
                            <th>hora</th>
                            <th>pista</th>
                            <th>Delegat</th>
                            <th>accions</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        if($delegats){
                            foreach ($delegats as $delegat) {
                                echo "<tr><td>".$delegat->idPartitDelegat."</td>";
                                echo "<td>".$delegat->data."</td>";
                                echo "<td>".$delegat->hora."</td>";
                                echo "<td>".$delegat->pista."</td>";
                                echo "<td>".$delegat->delegat." €</td>";
                                echo "<td>
                        <a href='".base_url()."arbitresBalles/editarPartit/$delegat->idPartitDelegat'><button type='button' class='btn btn-success'> <i class='fa fa-edit'></i></button></a>
                        <a href='".base_url()."arbitresBalles/deletePartit/$delegat->idPartitDelegat'><button type='button' class='btn btn-danger'> <i class='fa fa-remove'></i></button></a>
                      </td></tr>";
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </section>
    <!-- /.content -->

</div>
<script>

  $(function () {

    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es'
    })

    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es'
    })
     

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
<?php $this->load->view('footer'); ?>