<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php /*include('header.php');*/
$this->load->view('header'); ?>
<?php /*
if($_SESSION['USUARIO'] == '' || $_SESSION['USUARIO']['rol'] != 1){header('location:index.php');}
$idEquip = $_SESSION['USUARIO']['idEquip'];*/
?>
<?php /*include('header.php');*/ //$this->load->view('header');
if ($_SESSION['USUARIO'] == '') {
    header('location:index.php');
}
if ($_SESSION['USUARIO']['rol'] == 1) {
    //$idEquip = $_REQUEST['idEquip'];
    //$_SESSION['USUARIO']['idEquip'] = $idEquip;
    //echo "es rol 1";
} else {
    $idEquip = $_SESSION['USUARIO']['idEquip'];
}

/*$equip = getNomEquip();
$nomEquip = $equip['nomEquip'];
$numJugadorsEquip = numJugadorsEquip($idEquip);
if ($numJugadorsEquip == ''){$numJugadorsEquip = '0';}
$arrDadesEquip = getDadesEquip($idEquip);
$arrDadesJugadors = getDadesTriptic($idEquip);
$observacio = getObservacioEquip($idEquip);*/
?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                "lengthMenu": [[50], ["All"]],
                "language": {
                    "lengthMenu": "Mostra _MENU_ registres per pagina",
                    "zeroRecords": "Res per mostrar",
                    "info": "Pagina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hi ha registres disponibles",
                    "sSearch": "Buscar:",
                    "oPaginate": {
                        "sFirst": "Primer",
                        "sLast": "Últim",
                        "sNext": "Següent",
                        "sPrevious": "Anterior"
                    },
                    "infoFiltered": "(filtrat de _MAX_ registres totals)"
                }
            });
            //alertify.alert("Ja no es poden inscriure mes jugadors");

            $("#formJugador").click(function () {
                $("#nouJugador").slideToggle("slow");
            });

            $("#dadesPista").click(function () {
                $("#pista").slideToggle("slow");
            });
            $("#preferenciaHoraris").click(function () {
                $("#preferencia").slideToggle("slow");
            });
            $("#afegir").click(function () {
                $("#buttons").slideToggle("slow");
            });
            $("#imgInp").change(function () {
                readURL(this);
            });
            $("#fileUpload").on('change', function () {
                //Get count of selected files
                var countFiles = $(this)[0].files.length;
                var imgPath = $(this)[0].value;
                var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                var image_holder = $("#image-holder");
                image_holder.empty();
                if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                    if (typeof(FileReader) != "undefined") {
                        //loop for each file selected for uploaded.
                        for (var i = 0; i < countFiles; i++) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $("<img />", {
                                    "src": e.target.result,
                                    "class": "thumb-image"
                                }).appendTo(image_holder);
                            }
                            image_holder.show();
                            reader.readAsDataURL($(this)[0].files[i]);
                        }
                    } else {
                        alert("This browser does not support FileReader.");
                    }
                } else {
                    alert("Pls select only images");
                }
            });
            $("#fileUploadDNI").on('change', function () {
                //Get count of selected files
                var countFiles = $(this)[0].files.length;
                var imgPath = $(this)[0].value;
                var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                var image_holder = $("#image-holderDNI");
                image_holder.empty();
                if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                    if (typeof(FileReader) != "undefined") {
                        //loop for each file selected for uploaded.
                        for (var i = 0; i < countFiles; i++) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $("<img />", {
                                    "src": e.target.result,
                                    "class": "thumb-image"
                                }).appendTo(image_holder);
                            }
                            image_holder.show();
                            reader.readAsDataURL($(this)[0].files[i]);
                        }
                    } else {
                        alert("This browser does not support FileReader.");
                    }
                } else {
                    alert("Pls select only images");
                }
            });

            /*$('#myModalNorm').on('shown.bs.modal', function () {
                $(this).find('.modal-dialog').css({width:'90%',
                                           height:'auto',
                                          'max-height':'100%'});
            });*/

        });

        function mostrarOcultarPista(obj) {
            pista.style.visibility = (obj.checked) ? 'visible' : 'hidden';
        }

        function mostrarOcultarPreferencia(obj) {
            preferencia.style.visibility = (obj.checked) ? 'visible' : 'hidden';
        }

        function resetForm(form) {
            // clearing inputs
            var inputs = form.getElementsByTagName('input');
            for (var i = 0; i < inputs.length; i++) {
                switch (inputs[i].type) {
                    // case 'hidden':
                    case 'text':
                        inputs[i].value = '';
                        break;
                    case 'radio':
                    case 'checkbox':
                        inputs[i].checked = false;
                }
            }

            // clearing selects
            var selects = form.getElementsByTagName('select');
            for (var i = 0; i < selects.length; i++)
                selects[i].selectedIndex = 0;

            // clearing textarea
            var text = form.getElementsByTagName('textarea');
            for (var i = 0; i < text.length; i++)
                text[i].innerHTML = '';

            return false;
        }

        $(document).ready(function () {

            /*$("#formJugador").on("submit", function(e)
            {
                $.ajax({
                    type: "POST",
                    url : $(this).attr("action"),
                    data: $(this).serialize(),
                    success: function(data){
                        console.log(data);
                        var json = JSON.parse(data);

                        $(".errornom,.errorcognoms,.errordni,.erroremail, .errortelefon, .errordataneixament, .errorpoblacio, .errorfileupload, .errorfileuploaddni, .correcto").html("").css({"display":"none"});

                        if(json.res == "error"){
                            if(json.dni){
                                $(".errordni").append(json.dni).css({"display":"block"});
                            }
                            if(json.nom){
                                $(".errornom").append(json.nom).css({"display":"block"});
                            }
                            if(json.cognoms){
                                $(".errorcognoms").append(json.cognoms).css({"display":"block"});
                            }
                            if(json.email){
                                $(".erroremail").append(json.email).css({"display":"block"});
                            }
                            if(json.telefon){
                                $(".errortelefon").append(json.telefon).css({"display":"block"});
                            }
                            if(json.dataNeixament){
                                $(".errordataneixament").append(json.dataNeixament).css({"display":"block"});
                            }
                            if(json.poblacio){
                                $(".errorpoblacio").append(json.poblacio).css({"display":"block"});
                            }*/
            /*if(json.email){
                $(".errorfileupload").append(json.fileUpload).css({"display":"block"});
            }
            if(json.email){
                $(".errorfileuploaddni").append(json.fileUploadDNI).css({"display":"block"});
            }*/

            /*} else {
                location.reload();
                $(".correcto").append("Jugador Inscrit correctament").css({"display":"block"});
            }
        },
        error: function(xhr, exception) {
            console.log(data);
        }
    });
    e.preventDefault();
});*/
        });
        /*alertify
          .alert("Ja no es poden realitzar més fitxatges");*/
    </script>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Equip
            </h1>
            </br>
            <div class="col-lg-12 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">

                    </div>
                    <?php if ($jugadorsHistoric) { ?>
                        <a data-toggle="modal" data-target="#myModalHistoric"
                            title="Afegir jugadors temporada anterior" class="btn btn-app">
                            <i class="fa fa-users"></i>Anteriors
                        </a>
                    <?php } ?>
                    <a id="formJugador" class="btn btn-app" title="Inscriure Nou Jugador">
                        <i class="fa fa-user-plus"></i>Nou
                    </a>
                    <a data-toggle="modal" data-target="#myModalCapitans" title="Capitans" class="btn btn-app">
                        <i class="fa fa-graduation-cap"></i>Capitans
                    </a>
                    <a data-toggle="modal" data-target="#myModalEquip" title="Dades Equip" class="btn btn-app">
                        <i class="fa fa-info-circle"></i>Dades
                    </a>
                    <a data-toggle="modal" data-target="#myModalPref" id="formPreferencia" title="Preferència Horària" class="btn btn-app">
                        <i class="fa fa-clock-o"></i>Preferència
                    </a>
                    <?php if ($arrDadesEquip->idDivisio == 3 || $arrDadesEquip->idDivisio == 4) { ?>
                        <a data-toggle="modal" data-target="#myModalEquipPista" id="formPista" title="Pista Pròpia" class="btn btn-app">
                            <i class="fa fa-map-o"></i>Pista
                        </a>
                        <a href='<?= base_url() ?>equiposBalles/printTriptic/<?= $arrDadesEquip->idEquip ?>' target='_blank' id="formPista"
                           class="btn btn-app" title="Imprimir Triptic">
                            <i class="fa fa-print"></i>Tríptic
                        </a>
                    <?php } ?>
                    <div class="icon">
                        <i class="ion ion-plus-circled"></i>
                    </div>
                    <br>
                    <a href="#" class="small-box-footer"><!-- <i class="fa fa-arrow-circle-right"></i>--></a>
                </div>
            </div>


            <!--  <a style="float:left;margin-top:5px;"; href="<?= base_url() ?>equiposBalles/votacio"><button type="button" class="btn btn-success" id="delete">Votació All Star</button></a>-->

            </br></br>

            <div class="modal fade" id="myModalEquip" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close"
                                    data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Tancar</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">
                                Dades Equip
                            </h4>
                        </div>

                        <!-- Modal Body -->
                        <div class="modal-body">

                            <form role="form" method="POST" action='<?= base_url() ?>equiposBalles/datosEquipo'>
                                <div class="form-group">
                                    <label for="divisio">Divisió</label>
                                    <select class="form-control" id="divisio" name="divisio">
                                        <option name="1" value=1>Sense determinar</option>
                                        <option name="2" <?php if ($arrDadesEquip->nomDivisio == 'Primera Masculina') {
                                            echo "selected";
                                        } ?> value=2>Primera Divisio Masculina
                                        </option>
                                        <option name="3" <?php if ($arrDadesEquip->nomDivisio == 'Segona Masculina') {
                                            echo "selected";
                                        } ?> value=3>Segona Divisio Masculina
                                        </option>
                                        <option name="4" <?php if ($arrDadesEquip->nomDivisio == 'Primera Femenina') {
                                            echo "selected";
                                        } ?> value=4>Primera Divisio Femenina
                                        </option>
                                        <option name="5" <?php if ($arrDadesEquip->nomDivisio == 'Segona Femenina') {
                                            echo "selected";
                                        } ?> value=5>Segona Divisio Femenina
                                        </option>
                                        <option name="6" <?php if ($arrDadesEquip->nomDivisio == 'Tercera Masculina') {
                                            echo "selected";
                                        } ?> value=6>Tercera Divisio Masculina
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Color Samarreta</label>
                                    <input type="text" class="form-control"
                                           id="samarreta" name="samarreta" placeholder=""
                                           value="<?php echo $arrDadesEquip->colorSamarreta; ?>"/>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Color Pantaló</label>
                                    <input type="text" class="form-control"
                                           id="pantalo" name="pantalo" placeholder=""
                                           value="<?php echo $arrDadesEquip->colorPantalo; ?>"/>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">
                                        Tanca
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        Desar
                                    </button>
                                </div>
                                <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
                            </form>
                        </div>

                        <!-- Modal Footer -->

                    </div>
                </div>
            </div>
            <div class="modal fade" id="myModalEquipPista" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close"
                                    data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Tancar</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">
                                Dades Pista de Joc
                            </h4>
                        </div>

                        <!-- Modal Body -->
                        <div class="modal-body">
                            <form role="form" method="POST" action='<?= base_url() ?>equiposBalles/datosEquipoPista'>
                                <div class="form-group">
                                    <label for="nomPista">Nom Pista</label>
                                    <input type="text" class="form-control"
                                           id="nomPista" name="nomPista" placeholder=""
                                           value="<?php echo $arrDadesEquip->nomPista; ?>"/>
                                </div>
                                <div class="form-group">
                                    <label for="horaPista">Horari Partit</label>
                                    <input type="text" class="form-control"
                                           id="horaPista" name="horaPista" placeholder=""
                                           value="<?php echo $arrDadesEquip->horariPista; ?>"/>
                                </div>
                                <div class="form-group">
                                    <label for="poblacioPista">Poblacio</label>
                                    <input type="text" class="form-control"
                                           id="poblacioPista" name="poblacioPista" placeholder=""
                                           value="<?php echo $arrDadesEquip->poblacioPista; ?>"/>
                                </div>
                                <div class="form-group">
                                    <label for="direccioPista">Direccio Pista</label>
                                    <input type="text" class="form-control"
                                           id="direccioPista" name="direccioPista" placeholder=""
                                           value="<?php echo $arrDadesEquip->direccioPista; ?>"/>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">
                                        Tanca
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        Desar
                                    </button>
                                </div>
                                <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
                            </form>
                        </div>

                        <!-- Modal Footer -->

                    </div>
                </div>
            </div>
            <div class="modal fade" id="myModalCapitans" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close"
                                    data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Tancar</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">
                                Establir capitans
                            </h4>
                        </div>

                        <!-- Modal Body -->
                        <div class="modal-body">

                            <form role="form" method="POST" action='<?= base_url() ?>equiposBalles/establecerCapitanes'>
                                <div class="form-group">
                                    <label for="primer">Primer Capità</label>
                                    <select class="form-control" id="primer" name="primer">
                                        <?php foreach ($jugadors as $jugador) {
                                            if ($jugador->isPrimerCapita == 1) {
                                                $select = 'selected';
                                            } else {
                                                $select = '';
                                            }
                                            echo "<option value='" . $jugador->idJugador . "' " . $select . ">" . $jugador->nomJugador . " " . $jugador->cognomsJugador . "</option>";
                                        } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="segon">Segon Capità</label>
                                    <select class="form-control" id="segon" name="segon">
                                        <?php foreach ($jugadors as $jugador) {
                                            if ($jugador->isSegonCapita == 1) {
                                                $select = 'selected';
                                            } else {
                                                $select = '';
                                            }
                                            echo "<option value='" . $jugador->idJugador . "' " . $select . ">" . $jugador->nomJugador . " " . $jugador->cognomsJugador . "</option>";
                                        } ?>
                                    </select>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">
                                        Tanca
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        Desar
                                    </button>
                                </div>
                                <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
                            </form>
                        </div>

                        <!-- Modal Footer -->

                    </div>
                </div>
            </div>

            <!-- Modal jugadors historic -->
            <div class="modal fade" id="myModalHistoric" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close"
                                    data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Tancar</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">
                                Afegir jugadors temporada passada
                            </h4>
                        </div>

                        <!-- Modal Body -->
                        <div class="modal-body">

                            <form role="form" method="POST"
                                  action='<?= base_url() ?>equiposBalles/addJugadorsTemporadaAnterior'>
                                <div class="form-group">
                                    <?php foreach ($jugadorsHistoric as $jugadorH) {

                                        echo "<div class='checkbox'>";
                                        echo "<label>";
                                        echo "<input type='checkbox' name='jugadorsH[]' value='" . $jugadorH->idJugador . "'>" .
                                            $jugadorH->nomJugador . " " . $jugadorH->cognomsJugador . "</label></div>";

                                    } ?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">
                                        Tanca
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        Afegir
                                    </button>
                                </div>
                                <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
                            </form>
                        </div>

                        <!-- Modal Footer -->

                    </div>
                </div>
            </div>

            <div class="modal fade" id="myModalPref" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close"
                                    data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Tancar</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">
                                Preferència Horària
                            </h4>
                        </div>

                        <!-- Modal Body -->
                        <div class="modal-body">

                            <form role="form" method="POST" action='<?= base_url() ?>equiposBalles/preferenciaEquipo'>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Preferència</label>
                                    <input type="text" class="form-control"
                                           id="preferencia" name="preferencia" placeholder=""
                                           value="<?php echo $arrDadesEquip->preferenciaPartit; ?>"/>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">
                                        Tanca
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        Desar
                                    </button>
                                </div>
                                <!--<button type="submit" class="btn btn-primary">Crea Equip</button>-->
                            </form>
                        </div>

                        <!-- Modal Footer -->

                    </div>
                </div>
            </div>

            <?php
            echo validation_errors('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
            if ($this->session->flashdata('error')) {
                echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . $this->session->flashdata('error') . '</div>';
            }
            ?>
            <?php if ($this->session->flashdata('errorCapita')) {
                echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . $this->session->flashdata('errorCapita') . '</div>';
            } ?>
            <?php if ($this->session->flashdata('success')) {
                echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . $this->session->flashdata('success') . '</div>';
            } ?>
            <div class="" id="nouJugador"
                 aria-hidden="true" <?php if ($res == 'error' || $this->session->flashdata('error')) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
                <div class="">
                    <div class="">
                        <!-- Modal Header -->
                        <div class="">
                            <h4 class="" id="">
                                Nou Jugador (els camps amb (*) són obligatoris)
                            </h4>
                        </div>

                        <!-- Modal Body -->
                        <div class="">

                            <?php $attributes = array('class' => 'form-horizontal', 'id' => 'formJugador', 'role' => 'form'); ?>
                            <?php echo form_open_multipart('EquiposBalles/crearJugador', $attributes); ?>
                            <!--<form class="form-horizontal" id="formJugador" role="form" method="POST" enctype="multipart/form-data" action='<?= base_url() ?>EquiposBalles/crearJugador'>	-->
                            <div class="correcto alert alert-success" style="display:none;"></div>
                            <div class="row">
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="dni" class="col-md-4 control-label">DNI *</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="dni" id="dni"
                                                   placeholder="DNI"
                                                   value="<?php echo ($reset) ? "" : set_value('dni'); ?>">
                                            <div class="errordni alert alert-danger"
                                                 style="display:none;height:22%;padding:2px;margin-top:2px;"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="nom" class="col-md-4 control-label">Nom *</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="nom" id="nom"
                                                   placeholder="Nom"
                                                   value="<?php echo ($reset) ? "" : set_value('nom'); ?>">
                                            <div class="errornom alert alert-danger"
                                                 style="display:none;height:22%;padding:0px;margin-top:2px;"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="cognoms" class="col-md-4 control-label">Cognoms *</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="cognoms" id="cognoms"
                                                   placeholder="Cognoms"
                                                   value="<?php echo ($reset) ? "" : set_value('cognoms'); ?>">
                                            <div class="errorcognoms alert alert-danger"
                                                 style="display:none;height:22%;padding:2px;margin-top:2px;"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="email" class="col-md-4 control-label">Email *</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="email" id="email"
                                                   placeholder="email"
                                                   value="<?php echo ($reset) ? "" : set_value('email'); ?>">
                                            <div class="erroremail alert alert-danger"
                                                 style="display:none;height:22%;padding:2px;margin-top:2px;"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="telefon" class="col-md-4 control-label">Telèfon *</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="telefon" id="telefon"
                                                   placeholder="Telèfon"
                                                   value="<?php echo ($reset) ? "" : set_value('telefon'); ?>">
                                            <div class="errortelefon alert alert-danger"
                                                 style="display:none;height:22%;padding:2px;margin-top:2px;"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="dorsal" class="col-md-4 control-label">Dorsal</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="dorsal" id="dorsal"
                                                   placeholder="Dorsal"
                                                   value="<?php echo ($reset) ? "" : set_value('dorsal'); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="entrenador" class="col-md-4 control-label">Entrenador?</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" class="form-control" name="entrenador"
                                                   id="entrenador" placeholder="Entrenador" value='1'
                                                   value="<?php echo ($reset) ? "" : set_value('entrenador'); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="dataNeixament" class="col-md-4 control-label">Data Neixament
                                            *</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="dataNeixament"
                                                   id="dataNeixament" placeholder="dd/mm/yyyy"
                                                   value="<?php echo ($reset) ? "" : set_value('dataNeixament'); ?>">
                                            <div class="errordataneixament alert alert-danger"
                                                 style="display:none;height:22%;padding:2px;margin-top:2px;"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="poblacio" class="col-md-4 control-label">Població *</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="poblacio" id="poblacio"
                                                   placeholder="poblacio"
                                                   value="<?php echo ($reset) ? "" : set_value('poblacio'); ?>">
                                            <div class="errorpoblacio alert alert-danger"
                                                 style="display:none;height:22%;padding:2px;margin-top:2px;"></div>
                                        </div>
                                    </div>

                                </div>
                            </div><!-- /.row this actually does not appear to be needed with the form-horizontal -->
                            <div class="row">
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="fileUpload" class="col-md-4 control-label">Foto *</label>
                                        <div class="col-md-8">
                                            <input name="fileUpload" id="fileUpload" type="file"/>
                                            <div id="image-holder"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="fileUploadDNI" class="col-md-4 control-label">DNI *</label>
                                        <div class="col-md-8">
                                            <input name="fileUploadDNI" id="fileUploadDNI" type="file"/>
                                            <div id="image-holderDNI"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="footer">
                                <button type="submit" class="btn btn-primary">
                                    Inscriure
                                </button>
                                <input type='reset' value='Reset' name='reset' class="btn btn-warning"
                                       onclick="return resetForm(this.form);">
                            </div>
                            <?php echo form_close(); ?>
                            <!--</form>-->


                            <!-- Modal Footer -->

                        </div>
                    </div>
                </div>
                </br></br>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Arbitres</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                                <table class="table">
                                    <tr>
                                        <th>EQUIP</th>
                                        <th>JUGADORS</th>
                                        <th>TOTAL FITXES</th>
                                        <th>Pista Propia</th>
                                        <th>Esportivitat</th>
                                        <th>Desc. eq nou</th>
                                        <th>Prefència</th>
                                        <th>INSCRIPCIO</th>
                                        <th>TOTAL A PAGAR</th>
                                        <th>Pagat</th>
                                        <?php if ($_SESSION['USUARIO']['rol'] == 1) { ?>
                                            <th>Pagament</th> <?php } ?>
                                    </tr>
                                    <?php
                                    echo "<tr><td>" . $pagament['nomEquip'] . "</td>";
                                    echo "<td>" . $pagament['jugadors'] . "</td>";
                                    echo "<td>" . $pagament['totalFitxes'] . "</td>";
                                    echo "<td>" . $pagament['pistaPropia'] . "</td>";
                                    echo "<td>" . $pagament['esportivitat'] . "</td>";
                                    echo "<td>" . $pagament['descompteEquipNou'] . "</td>";
                                    echo "<td>" . $pagament['preferencia'] . "</td>";
                                    echo "<td>" . $pagament['inscripcio'] . "</td>";
                                    echo "<td>" . $pagament['totalPagar'] . "</td>";
                                    echo "<td>" . $pagament['isPagat'] . "</td>";
                                    echo "<td>" . $pagament['pagament'] . "</td></tr>";

                                    ?>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Arbitres</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example" class="table table-bordered table-hover display">
                                        <thead>
                                        <tr>
                                            <th>JUGADOR</th>
                                            <!--<th>Foto</th>-->
                                            <th>DNI</th>
                                            <th>Nom</th>
                                            <th>Edat</th>
                                            <th>Email</th>
                                            <th>Telefon</th>
                                            <th>Tipus fitxa</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>JUGADOR</th>
                                            <!--<th>Foto</th>-->
                                            <th>DNI</th>
                                            <th>Nom</th>
                                            <th>Edat</th>
                                            <th>Email</th>
                                            <th>Telefon</th>
                                            <th>Tipus fitxa</th>
                                        </tfoot>
                                        <tbody>
                                        <?php $i = 1;
                                        if ($jugadors) {
                                            foreach ($jugadors as $jugador) {
                                                echo "<tr>";
                                                echo "<td>" . $i . "</td>";
                                                echo "<td>" . $jugador->dni . "</td>";
                                                echo "<td>" . strtoupper($jugador->nomJugador . " " . $jugador->cognomsJugador) . "</td>";
                                                echo "<td>" . $jugador->dataNeixament . "</td>";
                                                echo "<td>" . $jugador->email . "</td>";
                                                echo "<td>" . $jugador->telefon . "</td>";
                                                echo "<td>" . $jugador->isEntrenador . "</td>";
                                                echo "</tr>";
                                                $i++;
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
        </section>
    </div>

<?php $this->load->view('footer'); ?>