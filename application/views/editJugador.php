<?php defined('BASEPATH') OR exit('No direct script access allowed');
 $this->load->view('header'); 
if($_SESSION['USUARIO'] == '' || $_SESSION['USUARIO']['rol'] != 1){header('location:index.php'); }
//if ($idDivisio == ''){ header('location:index.php'); } ?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#formSancio").click(function(){
      $("#novaSancio").slideToggle("slow");
    });
      $('#example').DataTable( {
        //"order": [[ 8 , "desc" ]]
      });
		});
</script>


<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      <?php echo "<a href='".base_url()."equiposBalles/mostraDadesEquip/$jugador->idEquip'><button style='margin-left:2%'; type='button' class='btn btn-primary'><i class='fa fa-users'></i></button></a>"; ?> Fitxa Jugador
      </h1>

      <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->
    
    </section>

<?php //echo "<a style='margin-left:2%'; href='../app/exportDadesEquipExcel.php?idDivisio=$idDivisio'><button type='button' class='btn btn-primary' id='delete'>Exportar Dades Equips</button></a>"; ?>

    
<section class="content">
  <div class="row">
        <div class="col-md-8">
          
          <!-- Profile Image -->
          <div class="box box-primary col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive " src="<?=base_url().$jugador->foto?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?=$jugador->nomJugador?> <?=$jugador->cognomsJugador?></h3>
              <form role="form" method="POST" action='<?=base_url()?>equiposBalles/editJugadorForm/<?=$jugador->idJugador?>'>
              <p class="text-muted text-center"><?=$jugador->nomEquip?></p>
              <?php if($this->session->flashdata('success')){echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('success').'</div>';} ?>
              <?php if($this->session->flashdata('error')){echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('error').'</div>';} ?>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Diviso</b> <a class="pull-right"><?=$jugador->nomDivisio?></a>
                </li>
                <li class="list-group-item">
                  <b>DNI</b> <a class="pull-right"><?=$jugador->dni?></a>
                </li>
                <li class="list-group-item">
                  <b>Data Neixament</b><a class="pull-right"><input type="text" name="dataNeixament" value="<?=$jugador->dataNeixament?>"/></a>
                </li>
                <li class="list-group-item">
                  <b>Email</b> <a class="pull-right"><input type="email" name="email" value="<?=$jugador->email?>"/></a>
                </li>
                <li class="list-group-item">
                  <b>Telèfon</b> <a class="pull-right"><input type="text" name="telefon" value="<?=$jugador->telefon?>"/></a>
                </li>
                <li class="list-group-item">
                  <b>Dorsal</b> <a class="pull-right"><input type="text" name="dorsal" value="<?=$jugador->dorsal?>"/></a>
                </li>
              </ul>


              <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModalObs" id="formObservacio">
                  <i class="fa fa-save"></i>
              </button>
            </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
</section>
</div>
<?php $this->load->view('footer'); ?>