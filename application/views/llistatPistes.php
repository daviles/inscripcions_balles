<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('header');
if (!array_key_exists('USUARIO', $_SESSION )) { header("location:".base_url()."index.php"); }
//if ($idDivisio == ''){ header('location:index.php'); } ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#formSancio").click(function(){
                $("#novaSancio").slideToggle("slow");
            });
            $("#formBanqueta").click(function(){
                $("#novaTecnicaBanqueta").slideToggle("slow");
            });
            //$('#example').DataTable();
            $('#example').DataTable( {
                "order": [[ 4, "desc" ]],
                "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false,
                'scrollX'     : true,

            } );


            $("#equip").change(function(){
                var id=$(this).val();
                //var dataString = 'id='+ id;
                var dataString = {
                    "id" : id
                };
                console.log(dataString);
                $.ajax
                ({
                    type: "POST",
                    url: "<?=base_url() ?>equiposBalles/selectJugadorsEquip",
                    data: dataString,
                    //dataType:'json',
                    cache: false,
                    success: function(html)
                    {
                        $("#jugador").html(html);
                    }
                });

            });
        } );
    </script>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Pistes
            </h1>

            <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->
            <?php if($_SESSION['USUARIO']['nomUser'] == 'david'){ ?><a style="float:right"; href="../app/sessions.txt" target="_blank"><button type="button" class="btn btn-info" id="delete">Sessions</button></a> <?php } ?>
        </section>

        <?php //echo "<a style='margin-left:2%'; href='../app/exportDadesEquipExcel.php?idDivisio=$idDivisio'><button type='button' class='btn btn-primary' id='delete'>Exportar Dades Equips</button></a>"; ?>


        <section class="content">

            <div class="row">
                <div class="col-md-5">
                    <div class="box box-danger collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Insertar nova Pista</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: none;">
                            <form role="form" method="POST" action='<?=base_url()?>arbitresBalles/newPista'>
                                <div class="form-group">
                                    <label for="nom">Nom</label>
                                    <input type="text" class="form-control" name="nom" id="nom" value="">
                                </div>
                                <div class="form-group">
                                    <label for="direccio">Direcció</label>
                                    <input type="text" class="form-control" name="direccio" id="direccio" value="">
                                </div>
                                <div class="form-group">
                                    <label for="poblacio">Població</label>
                                    <input type="text" class="form-control" name="poblacio" id="poblacio" value="">
                                </div>
                                <div class="form-group">
                                    <label for="horari">Horari i dia</label>
                                    <input type="text" class="form-control" name="horari" id="horari" value=""/>
                                </div>
                                <div class="form-group">
                                    <label for="isForaSabadell">Fora de Sabadell</label>
                                    <select class="form-control select" style="width: 100%;" id="isForaSabadell" name="isForaSabadell">
                                        <option value="0">No</option>
                                        <option value="1">Si</option>
                                    </select>
                                </div>

                                <!-- /.box-body -->

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Crear</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <?php if($this->session->flashdata('success')){echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('success').'</div>';} ?>
            <?php if($this->session->flashdata('error')){echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('error').'</div>';} ?>

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Pistes</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>idPista</th>
                                    <th>Nom</th>
                                    <th>Horari</th>
                                    <th>Poblacio</th>
                                    <th>Direcció</th>
                                    <th>Fora Sabadell</th>
                                    <th>Accions</th>

                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>idPista</th>
                                    <th>Nom</th>
                                    <th>Horari</th>
                                    <th>Poblacio</th>
                                    <th>Direcció</th>
                                    <th>Fora Sabadell</th>
                                    <th>Accions</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                if($pistes){
                                    foreach ($pistes as $pista) {
                                        if ($pista->actiu == "ACT") {
                                            echo "<tr>";
                                        } else {
                                            echo "<tr style='background-color:#ffcccc';>";
                                        }
                                        echo "<td>".$pista->idPistaJoc."</td>";
                                        echo "<td>".$pista->nomPista."</td>";
                                        echo "<td>".$pista->horariPista."</td>";
                                        echo "<td>".$pista->poblacioPista."</td>";
                                        echo "<td>".$pista->direccioPista."</td>";
                                        echo "<td>";
                                        if($pista->isPistaFora == '1') {
                                            echo "SI";
                                        } else {
                                            echo "NO";
                                        }
                                        echo "</td>";
                                        echo "<td>
                                        <a href='".base_url()."arbitresBalles/editarPista/$pista->idPistaJoc'><button type='button' class='btn btn-default'> <i class='fa fa-edit'></i></button></a>";
                                        if ($pista->actiu == 'ACT') {
                                            echo "<a href='".base_url()."arbitresBalles/deletePista/$pista->idPistaJoc'><button type='button' class='btn btn-danger'> <i class='fa fa-remove'></i></button>";
                                        }else{
                                            echo "<a href='".base_url()."arbitresBalles/activatePista/$pista->idPistaJoc'><button type='button' class='btn btn-success'> <i class='fa fa-check'></i></button>";
                                        }
                                        echo "</td></tr>";
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

    </div>
    <script>

        $(function () {

            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            )

            //Date picker

            $('#datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: 'es'
            })

            $('#datepicker2').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: 'es'
            })


            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            })
        })
    </script>
<?php $this->load->view('footer'); ?>