<?php defined('BASEPATH') OR exit('No direct script access allowed');
 $this->load->view('header'); 
if($_SESSION['USUARIO'] == '' || $_SESSION['USUARIO']['rol'] != 1){header('location:index.php'); }
if ($idDivisio == ''){ header('location:index.php'); } ?>
<script type="text/javascript">
  $(document).ready(function() {
     $('#example').DataTable({
        "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
     });
  } );
</script>

<div class="content-wrapper">
  <?php if($_SESSION['USUARIO']['rol'] == 1){
  //echo "<a href='".base_url()."login/loginUser/1'><button style='margin-left:2%'; type='button' class='btn btn-warning'>Tornar a Administracio</button></a>";
   }else{ 
  echo "<a href='formInscripcio.php'><button style='float:right'; type='button' class='btn btn-warning'>Inscrire mes jugadors</button></a>";
   } ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Equips <b><?php echo (strtoupper($nomDivisio)); ?></b>
      </h1>
      <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->
<?php if($_SESSION['USUARIO']['nomUser'] == 'david'){ ?><a style="float:right"; href="../app/sessions.txt" target="_blank"><button type="button" class="btn btn-info" id="delete">Sessions</button></a> <?php } ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Equips</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
            <tr>
        <?php if ($idDivisio == 1 || $idDivisio == 2 || $idDivisio == 5) { ?>
              <th>idEquip</th>
              <th>Equip</th>
              <th>capita</th>
              <th>Email</th>
              <th>Telefon</th>
              <th>Preferencia Partits</th>
              <th>Observacions</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>idEquip</th>
              <th>Equip</th>
              <th>capita</th>
              <th>Email</th>
              <th>Telefon</th>
              <th>Preferencia Partits</th>
              <th>Observacions</th>
          <?php }else{ ?>
                <th>idEquip</th>
                <th>Equip</th>
                <th>capita</th>
                <th>Email</th>
                <th>Telefon</th>
                <th>Preferencia Partits</th>
                <th>Direccio Pista</th>
                <th>Horari Partit</th>
                <th>Poblacio</th>
                <th>Observacions</th>
              </tr>
          </thead>
          <tfoot>
              <tr>
                <th>idEquip</th>
                <th>Equip</th>
                <th>capita</th>
                <th>Email</th>
                <th>Telefon</th>
                <th>Preferencia Partits</th>
                <th>Direccio Pista</th>
                <th>Horari Partit</th>
                <th>Poblacio</th>
                <th>Observacions</th>
          <?php } ?>
              </tr>
        </tfoot>
        <tbody>
     <?php
         if($equipsPerDivisio || $equipsPerDivsioPista){
            if ($idDivisio == 1 || $idDivisio == 2 || $idDivisio == 5) { 
              foreach ($equipsPerDivisio as $equip) {
                echo "<tr><td>".$equip->idEquip."</td>";
                echo "<td><a href='".base_url()."equiposBalles/mostraDadesEquip/$equip->idEquip'>".$equip->nomEquip."</a></td>";
                echo "<td>".$equip->nomJugador." ".$equip->cognomsJugador."</td>";
                echo "<td>".$equip->email."</td>";
                echo "<td>".$equip->telefon."</td>";
                echo "<td>".$equip->preferenciaPartit."</td>";
                echo "<td>".$equip->observacions."</td></tr>";
              }
            }else{
              foreach ($equipsPerDivsioPista as $equip) {
                echo "<tr><td>".$equip->idEquip."</td>";
                echo "<td><a href='".base_url()."equiposBalles/mostraDadesEquip/$equip->idEquip'>".$equip->nomEquip."</a></td>";
                echo "<td>".$equip->nomJugador." ".$equip->cognomsJugador."</td>";
                echo "<td>".$equip->email."</td>";
                echo "<td>".$equip->telefon."</td>";
                echo "<td>".$equip->preferenciaPartit."</td>";
                echo "<td>".$equip->direccioPista."</td>";
                echo "<td>".$equip->horariPista."</td>";
                echo "<td>".$equip->poblacioPista."</td>";
                echo "<td>".$equip->observacions."</td></a></tr>";
              }
            } 
          } ?>
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>