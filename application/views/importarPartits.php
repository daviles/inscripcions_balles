<?php defined('BASEPATH') OR exit('No direct script access allowed');
 $this->load->view('header'); 
if (!array_key_exists('USUARIO', $_SESSION )) { header("location:".base_url()."index.php"); }
//if ($idDivisio == ''){ header('location:index.php'); } ?>
    <style>
        .selected {
            background-color: lightgreen;
        }
    </style>
<script type="text/javascript">
  $(document).ready(function() {

      var table =  $('#partits').DataTable( {
        "order": [[ 0, "asc" ]],
        "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        'scrollX'     : true,
        'stateSave'   : true
    } );
      $('#partitsFem').DataTable( {
          "order": [[ 0, "asc" ]],
          "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false,
          'scrollX'     : true,
          'stateSave'   : true
      } );
      $('#franja').DataTable( {
          "order": [[ 0, "asc" ]],
          "lengthMenu": [[10,25,50,-1],[10,25,50,"All"]],
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false,
          'scrollX'     : true,
          'stateSave'   : true
      } );
      //var table = $('#partits').DataTable();

      $('#partits').find('tbody').on( 'click', 'tr', function () {
          $(this).toggleClass('selected');
      } );

      $('#button').click( function () {
          fcookie='dessignacio';
          alert( table.rows('.selected').data().length +' partits sel.leccionats' );
          var dataArr = [];
          $.each($("#partits tr.selected"),function(){ //get each tr which has selected class
              dataArr.push($(this).find('td').eq(0).text()); //find its first td and push the value

              //dataArr.push($(this).find('td:first').text()); You can use this too
          });
          console.log(dataArr);
          alert(dataArr);

          document.cookie=fcookie+"="+dataArr;
          //alert(document.cookie);

          //alert(dataArr);
          //var p = jQuery.getJSON( dataArr );
          window.location.href = "<?php echo site_url('arbitresBalles/enviarDessignacio'); ?>" ;
      } );
    } );
  $(document).on('change', '.sel', function(e){
      e.preventDefault();

      window.location = $(this).find('option:selected').val();
  });
  $(document).on('change', '.sel2', function(e){
      e.preventDefault();

      window.location = $(this).find('option:selected').val();
  });
  $(document).on('change', '.sel3', function(e){
      e.preventDefault();

      window.location = $(this).find('option:selected').val();
  });
  $(document).on('change', '.sel4', function(e){
      e.preventDefault();

      window.location = $(this).find('option:selected').val();
  });
  $(document).on('change', '.sel5', function(e){
      e.preventDefault();

      window.location = $(this).find('option:selected').val();
  });
  $(document).on('change', '.sel_pista', function(e){
      e.preventDefault();

      window.location = $(this).find('option:selected').val();
  });
</script>
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Partits
      </h1>
      
      <!--<a style="float:right"; href="<?=base_url() ?>login/logout"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>-->
      <?php if($_SESSION['USUARIO']['nomUser'] == 'david'){ ?><a style="float:right"; href="../app/sessions.txt" target="_blank"><button type="button" class="btn btn-info" id="delete">Sessions</button></a> <?php } ?>
    </section>

<?php //echo "<a style='margin-left:2%'; href='../app/exportDadesEquipExcel.php?idDivisio=$idDivisio'><button type='button' class='btn btn-primary' id='delete'>Exportar Dades Equips</button></a>"; ?>

    
<section class="content">

  <div class="row">
  <div class="col-md-2">
  <div class="box box-danger collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Importar Partits</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
          <?php echo form_open_multipart('arbitresBalles/to_mysql');?>
 
                <input type="file" name="excel" size="200" />
 
      <br /><br />
 
     
 
      <input type="submit" value="Importar" />
 
    <?php echo form_close() ?>
        </div>
        <!-- /.box-body -->
  </div>
</div>
      <div class="col-md-5">
          <div class="box box-success collapsed-box">
              <div class="box-header with-border">
                  <h3 class="box-title">Enviar Mail Pagaments</h3>

                  <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                      <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
                  </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body" style="display: none;">
                  <?php echo form_open('arbitresBalles/enviarMailPagaments');?>

                  <div class="form-group col-md-6">
                      <label for="tipus">Data Inicial</label>
                      <input type="text" class="form-control" name="dataInicial" id="datepicker" value="">
                  </div>
                  <div class="form-group col-md-6">
                      <label for="tipus">Data Final</label>
                      <input type="text" class="form-control" name="dataFinal" id="datepicker2" value="">
                  </div>
                  <div class="form-group col-md-12">
                      <label for="tipus">Assumpte</label>
                      <input type="text" class="form-control" name="subject" value="">
                  </div>
                  <div class="form-group col-md-12">
                      <label>Textarea</label>
                      <textarea class="form-control" id="editor1" name="mensajeMail" rows="3">
                          <p>Bona tarda,</p>
                          <p>Us comuniquem que el dia de cobrament dels <b>partits de Març</b>
                          de la Balles (masculí i femení), serà el proper <b>divendres dia  06 d'Abril en horari
                          de 18.15 a 19.45 hores</b> a les nostres instalacions habituals.</p>

                        <p>En el cas de què hi hagués alguna incidència en els imports o d'altres,
                            preguem ens ho comuniqueu per via mail posteriorment, ja que el dia 06
                            d'Abril
                            només es realitzaran pagaments, i no es podrà contrastar
                            tota la informació necessària per solventar-les, donada la quantitat de
                            persones  que s'han d'atendre.</p>

                            <p>També us recordem de què hi hauria la possiblitat de cobrar les dietes
                            dels partits per transferència. Si algú estigués interessat, si us plau,
                                poseu-vos en contacte amb nosaltres.</p>

                          <p>Per tal de poder-nos organitzar, agraïria confirmessiu assitència.</p>

                          <p>Moltes gràcies a tothom.</p>

                          <p>Salutacions,</p>

                            <p>Marta.</p>

                      </textarea>
                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Enviar</button>
                  </div>

                  <?php echo form_close() ?>
              </div>
              <!-- /.box-body -->
          </div>
      </div>
      <div class="col-md-3">
          <div class="box box-success collapsed-box">
              <div class="box-header with-border">
                  <h3 class="box-title">Filtrar per Dates</h3>

                  <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                      <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
                  </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body" style="display: none;">
                  <?php echo form_open('arbitresBalles/filtrarPartitsDates');?>

                  <div class="form-group col-md-6">
                      <label for="tipus">Data Inicial</label>
                      <input type="text" class="form-control" name="dataInicial" id="datepicker_from" required value="">
                  </div>
                  <div class="form-group col-md-6">
                      <label for="tipus">Data Final</label>
                      <input type="text" class="form-control" name="dataFinal" id="datepicker_to" required value="">
                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Buscar</button>
                  </div>

                  <?php echo form_close() ?>
              </div>
              <!-- /.box-body -->
          </div>
      </div>
</div>
      <div class="row">
          <?php
          $correct = $this->session->flashdata('success');
          $error = $this->session->flashdata('error');
          if ($correct)
          {
                  ?>
                  <script type="text/javascript">
                      alertify
                          .alert("Enviats Correctament : ","<?php
                                foreach ($correcto as $correct) {
                                    echo $correct."</br>";
                                }
                      ?>", function(){
                              alertify.message('OK');
                          });
                  </script>

                  <?php
        }

          if ($error)
          {
          ?>
          <script type="text/javascript">
              alertify
                  .alert("Error Enviament : ","<?php
                      foreach ($error as $err) {
                          echo $err."</br>";
                      }
                      ?>", function(){
                      alertify.message('OK');
                  });
          </script>

              <?php
            }
            ?>
        <div class="col-xs-12">
            <?php if($this->session->flashdata('success')){echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('success').'</div>';} ?>
            <?php if($this->session->flashdata('error')){echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$this->session->flashdata('error').'</div>';} ?>
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Partits</h3>
                <a href='<?=base_url()?>arbitresBalles/newPartit'><button type='button' class='btn btn-info' title="Afegir partit Manual"> <i class='fa fa-plus-square'></i></button></a>
                <a href='<?=base_url()?>arbitresBalles/assignarDelegat'><button type='button' class='btn btn-warning' title="Afegir partit delegat"> <i class='fa fa-user-secret'></i></button></a>
                <a href='<?=base_url()?>arbitresBalles/enviarMailPagamentsForm'><button type='button' class='btn btn-danger' title="Enviar Mail Pagaments"> <i class='fa fa-eur'></i></button></a>
                <button class="btn btn-default" id="button">Crear Dessignació</button></div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="partits" class="table table-bordered table-hover">
              <thead>
                  <tr>
                    <th>idPartit</th>
                    <th>local</th>
                    <th>visitant</th>
                    <th>data</th>
                    <th>hora</th>
                    <th>pista</th>
                    <th>Arbitre</th>
                    <!--<<th>tarifa Arbitre</th>-->
                    <th>Anotador</th>
                    <!--<th>tarifa Anotador</th>-->
                    <!--<th>tarifa Delegat</th>-->
                    <th>Suspes</th>
                    <th>accions</th>
                  </tr>
              </thead>
          <tfoot>
            <tr>
              <th>idPartit</th>
              <th>local</th>
              <th>visitant</th>
              <th>data</th>
              <th>hora</th>
              <th>pista</th>
              <th>Arbitre</th>
             <!--<<th>tarifa Arbitre</th>-->
              <th>Anotador</th>
              <!--<th>tarifa Anotador</th>-->
              <!--<th>tarifa Delegat</th>-->
              <th>Suspes</th>
              <th>accions</th>
            </tr>
        </tfoot>
        <tbody>
           <?php 
            if($partits){
              foreach ($partits as $partit) {
                echo "<tr>";
                echo "<td>".$partit->idPartit."</td>";
                echo "<td>".$partit->local."</td>";
                echo "<td>".$partit->visitant."</td>";
                echo "<td>".$partit->data."</td>";
                echo "<td>".$partit->hora."</td>";
                //echo "<td>".$partit->pista."</td>";
                  echo "<td>";
                  echo "<select class='form-control select sel_pista' style='width: 70%;' id='pista' name='pista' >
                      <option value='".base_url()."arbitresBalles/updatePistaPartit/$partit->idPartit/0'>no designado</option>";
                  foreach ($pistes as $pista) {
                      $selected = '';
                      if($pista->nomPista == $partit->pista){ $selected = "selected"; }
                      echo "<option value='".base_url()."arbitresBalles/updatePistaPartit/$partit->idPartit/$pista->idPistaJoc' ".$selected.">".$pista->nomPista."</option>";
                  }
                  echo "</select>";
                  echo "</td>";
                  if($partit->isAcceptatArbitre) {
                      echo "<td style='border:3px solid palegreen;'>";
                  } else {
                      echo "<td style='border:1px solid indianred;'>";
                  }

                echo "<select class='form-control select sel' style='width: 70%;' id='arbitre' name='arbitre' >
                      <option value='".base_url()."arbitresBalles/updateArbitre/$partit->idPartit/0'>no designado</option>";
                      foreach ($arbitres as $arbitre) {
                        $selected = '';
                        if($arbitre->idArbitre == $partit->idArbitre){ $selected = "selected"; }
                            echo "<option value='".base_url()."arbitresBalles/updateArbitre/$partit->idPartit/$arbitre->idArbitre' ".$selected.">".$arbitre->nomArbitre."</option>";
                      }
                echo "</select>";
                echo "<a href='".base_url()."arbitresBalles/showPartitsArbitre/$partit->idArbitre'><button type='button' class='btn btn-default'> <i class='fa fa-eye'></i></button></a>";
                echo "</td>";
                //echo "<td>
                //    <a href='".base_url()."arbitresBalles/showPartitsArbitre/$partit->idArbitre'>".$partit->nomArbitre."</a></td>";
               // echo "<td>".$partit->tarifaArbitre." €</td>";
                  echo "</td>";
                  if($partit->isAcceptatAnotador) {
                      echo "<td style='border:3px solid palegreen;'>";
                  } else {
                      echo "<td style='border:1px solid indianred;'>";
                  }
                  echo "<select class='form-control select sel2' style='width: 70%;' id='arbitre' name='arbitre' >
                      <option value='".base_url()."arbitresBalles/updateAnotador/$partit->idPartit/0'>no designado</option>";
                  foreach ($arbitres as $arbitre) {
                      $selected = '';
                      if($arbitre->idArbitre == $partit->idAnotador){ $selected = "selected"; }
                      echo "<option value='".base_url()."arbitresBalles/updateAnotador/$partit->idPartit/$arbitre->idArbitre' ".$selected.">".$arbitre->nomArbitre."</option>";
                  }
                  echo "</select>";
                  echo "<a href='".base_url()."arbitresBalles/showPartitsArbitre/$partit->idAnotador'><button type='button' class='btn btn-default'> <i class='fa fa-eye'></i></button></a>";
                  echo "</td>";



                //  echo "<td>
                 //   <a href='".base_url()."arbitresBalles/showPartitsArbitre/$partit->idAnotador'>".$partit->nomAnotador."</a></td>";
               // echo "<td>".$partit->tarifaAnotador." €</td>";
               // echo "<td>".$partit->tarifaDelegat." €</td>";
                if($partit->isSuspes == 1) {
                  echo "<td>SI</td>";
                }else{
                  echo "<td>NO</td>";
                }
                echo "<td>
                        <a href='".base_url()."arbitresBalles/editarPartit/$partit->idPartit'><button type='button' class='btn btn-success'> <i class='fa fa-edit'></i></button></a>
                        <a href='".base_url()."arbitresBalles/deletePartit/$partit->idPartit'><button type='button' class='btn btn-danger'> <i class='fa fa-remove'></i></button></a>
                      </td></tr>";
              }
            } 
          ?>
        </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    <div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Partits Femenins</h3>
               </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="partitsFem" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>idPartit</th>
                        <th>local</th>
                        <th>visitant</th>
                        <th>data</th>
                        <th>hora</th>
                        <th>pista</th>
                        <th>Arbitre</th>
                        <!--<<th>tarifa Arbitre</th>-->
                        <th>Anotador</th>
                        <!--<th>tarifa Anotador</th>-->
                        <!--<th>tarifa Delegat</th>-->
                        <th>Suspes</th>
                        <th>accions</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>idPartit</th>
                        <th>local</th>
                        <th>visitant</th>
                        <th>data</th>
                        <th>hora</th>
                        <th>pista</th>
                        <th>Arbitre</th>
                        <!--<<th>tarifa Arbitre</th>-->
                        <th>Anotador</th>
                        <!--<th>tarifa Anotador</th>-->
                        <!--<th>tarifa Delegat</th>-->
                        <th>Suspes</th>
                        <th>accions</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php
                    if($partitsFem){
                        foreach ($partitsFem as $partit) {
                            echo "<tr><td>".$partit->idPartit."</td>";
                            echo "<td>".$partit->local."</td>";
                            echo "<td>".$partit->visitant."</td>";
                            echo "<td>".$partit->data."</td>";
                            echo "<td>".$partit->hora."</td>";
                            //echo "<td>".$partit->pista."</td>";
                            echo "<td>";
                            echo "<select class='form-control select sel_pista' style='width: 70%;' id='pista' name='pista' >
                                    <option value='".base_url()."arbitresBalles/updatePistaPartit/$partit->idPartit/0'>no designado</option>";
                            foreach ($pistes as $pista) {
                                $selected = '';
                                if($pista->nomPista == $partit->pista){ $selected = "selected"; }
                                echo "<option value='".base_url()."arbitresBalles/updatePistaPartit/$partit->idPartit/$pista->idPistaJoc' ".$selected.">".$pista->nomPista."</option>";
                            }
                            echo "</select>";
                            echo "</td>";
                            echo "<td>";
                            echo "<select class='form-control select sel3' style='width: 70%;' id='arbitre' name='arbitre' >
                                <option value='".base_url()."arbitresBalles/updateArbitre/$partit->idPartit/0'>no designado</option>";
                                foreach ($arbitres as $arbitre) {
                                    $selected = '';
                                    if($arbitre->idArbitre == $partit->idArbitre){ $selected = "selected"; }
                                    echo "<option value='".base_url()."arbitresBalles/updateArbitre/$partit->idPartit/$arbitre->idArbitre' ".$selected.">".$arbitre->nomArbitre."</option>";
                                }
                                echo "</select>";
                                echo "<a href='".base_url()."arbitresBalles/showPartitsArbitre/$partit->idArbitre'><button type='button' class='btn btn-default'> <i class='fa fa-eye'></i></button></a>";
                            echo "</td>";
                            //echo "<td><a href='".base_url()."arbitresBalles/showPartitsArbitre/$partit->idArbitre'>".$partit->nomArbitre."</a></td>";
                            // echo "<td>".$partit->tarifaArbitre." €</td>";
                            echo "<td>";
                            echo "<select class='form-control select sel4' style='width: 70%;' id='arbitre' name='arbitre' >
                                    <option value='".base_url()."arbitresBalles/updateAnotador/$partit->idPartit/0'>no designado</option>";
                                    foreach ($arbitres as $arbitre) {
                                        $selected = '';
                                        if($arbitre->idArbitre == $partit->idAnotador){ $selected = "selected"; }
                                        echo "<option value='".base_url()."arbitresBalles/updateAnotador/$partit->idPartit/$arbitre->idArbitre' ".$selected.">".$arbitre->nomArbitre."</option>";
                                    }
                                    echo "</select>";
                                    echo "<a href='".base_url()."arbitresBalles/showPartitsArbitre/$partit->idAnotador'><button type='button' class='btn btn-default'> <i class='fa fa-eye'></i></button></a>";
                                    echo "</td>";

                            //echo "<td><a href='".base_url()."arbitresBalles/showPartitsArbitre/$partit->idAnotador'>".$partit->nomAnotador."</a></td>";
                            // echo "<td>".$partit->tarifaAnotador." €</td>";
                            // echo "<td>".$partit->tarifaDelegat." €</td>";
                            if($partit->isSuspes == 1) {
                                echo "<td>SI</td>";
                            }else{
                                echo "<td>NO</td>";
                            }
                            echo "<td>
                        <a href='".base_url()."arbitresBalles/editarPartit/$partit->idPartit'><button type='button' class='btn btn-success'> <i class='fa fa-edit'></i></button></a>
                        <a href='".base_url()."arbitresBalles/deletePartit/$partit->idPartit'><button type='button' class='btn btn-danger'> <i class='fa fa-remove'></i></button></a>
                      </td></tr>";
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Franges Delegats</h3>
                    <a href='<?=base_url()?>arbitresBalles/newPartit'><button type='button' class='btn btn-info' title="Afegir partit Manual"> <i class='fa fa-plus-square'></i></button></a>
                    <a href='<?=base_url()?>arbitresBalles/assignarDelegat'><button type='button' class='btn btn-warning' title="Afegir partit delegat"> <i class='fa fa-user-secret'></i></button></a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="franja" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>idFranja</th>
                            <th>Data</th>
                            <th>Hora/Franja</th>
                            <th>Pista</th>
                            <th>Delegat</th>
                            <th>accions</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>idFranja</th>
                            <th>Data</th>
                            <th>Hora/Franja</th>
                            <th>Pista</th>
                            <th>Delegat</th>
                            <th>accions</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        if($franjaDelegats){
                            foreach ($franjaDelegats as $franja) {
                                echo "<tr><td>".$franja->idPartitDelegat."</td>";
                                echo "<td>".$franja->data."</td>";
                                echo "<td>".$franja->hora."</td>";
                                echo "<td>".$franja->pista."</td>";
                                echo "<td>";
                                echo "<select class='form-control select sel5' style='width: 70%;' id='arbitre' name='arbitre' >
                                    <option value='".base_url()."arbitresBalles/updateDelegat/$partit->idPartit/0'>no designado</option>";
                                foreach ($arbitres as $arbitre) {
                                    $selected = '';
                                    if($arbitre->idArbitre == $partit->idAnotador){ $selected = "selected"; }
                                    echo "<option value='".base_url()."arbitresBalles/updateAnotador/$partit->idPartit/$arbitre->idArbitre' ".$selected.">".$arbitre->nomArbitre."</option>";
                                }
                                echo "</select>";
                                echo "<a href='".base_url()."arbitresBalles/showPartitsArbitre/$partit->idAnotador'><button type='button' class='btn btn-default'> <i class='fa fa-eye'></i></button></a>";
                                echo "</td>";
                                //echo "<td><a href='".base_url()."arbitresBalles/showPartitsArbitre/$franja->idDelegat'>".$franja->nomDelegat."</td>";
                                echo "<td>
                        <a href='".base_url()."arbitresBalles/editarPartit/$franja->idPartitDelegat'><button type='button' class='btn btn-success'> <i class='fa fa-edit'></i></button></a>
                        <a href='".base_url()."arbitresBalles/deletePartit/$franja->idPartitDelegat'><button type='button' class='btn btn-danger'> <i class='fa fa-remove'></i></button></a>
                      </td></tr>";
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </section>
    <!-- /.content -->

</div>
<script>

  $(function () {

    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker

      //Date picker
      $('#datepicker').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
          language: 'es'
      })

      $('#datepicker2').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
          language: 'es'
      })

      $('#datepicker_from').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
          language: 'es'
      })

      $('#datepicker_to').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
          language: 'es'
      })
     

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
      CKEDITOR.replace('editor1')
      $('.textarea').wysihtml5()
  })

</script>
<?php $this->load->view('footer'); ?>