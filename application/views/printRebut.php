	<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('../app/fpdf/fpdf.php');
define("Alturacarnet",0);
define("HPag",721.89);
define("MargeCarnet_a_Esquerra",100);
//$idEquip = $_SESSION['USUARIO']['idEquip'];
//$idEquip = $_REQUEST['idEquip'];
//$arrDadesRebut = array();
//$arrDadesRebut = getDadesRebut($idEquip);
//$idDivisio = getIdDivsiobyIdEquip($idEquip);
$contador = 0;
$y=50;
$x=10;
$h = 0;
$i = 0;
$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
if ($idDivisio->idDivisio == 1 || $idDivisio->idDivisio == 2 || $idDivisio->idDivisio == 5){
	$inscripcio = 998;
}elseif ($idDivisio->idDivisio == 3 || $idDivisio->idDivisio == 4){
	if (is_object($pistaJoc)){
		$inscripcio = 720;
		$pistaPropia = 'SI';
	}else{
		$inscripcio = 850;
	}
}else{
	$inscripcio = 0;
}
if ($idEquip == 63 || $idEquip == 34 || $idEquip == 33){
	$esportivitat = 'SI (25%)';
	$inscripcio -= ($inscripcio * 0.25);
}elseif ($idEquip == 58 || $idEquip == 52){
	$esportivitat = 'SI (12.5%)';
	$inscripcio -= ($inscripcio * 0.125);
}
if ($preferencia->preferenciaPartit != NULL || $preferencia->preferenciaPartit != ''){
	$pref = 'SI (70€)';
	$prefHoraria = 70;
}
$descompteEquipNou = '';
if($idEquip == 36 || $idEquip == 55) {
	$descompteEquipNou = '(descompte per equip nou -90 euros)';
	$inscripcio -= 90;
}
if($idEquip == 39) {
	$descompteEquipNou = '(descompte per equip nou -180 euros)';
	$inscripcio -= 180;
}
/*if ($dadesJugador['foto'] != ''){
		$pdf->Image($dadesJugador['foto'],$x+2,$y+8.5,20,22,'JPG');
	}else{
		$pdf->Image('../app/images/user.jpg',$x+2,$y+8.5,20,25,'JPG');
	}*/
for ($i = 1;$i<=2;$i++){
	$pdf->SetFont('Arial','B',15);
	$pdf->Text($x+29, $y-30, "ASSOCIACIO DE BASQUET DE LLEURE DE SABADELL");
	$pdf->Image('../images/logoBalles.gif',$x+2,$y-43,20,22,'GIF');
	$pdf->Line($x-9, $y-20, $x+199, $y-20);
	$pdf->Text($x+40, $y-10, "REBUT INSCRIPCIO TEMPORADA 2017/18");
	$pdf->SetFont('Arial','',12);
	$pdf->Text($x+10, $y+5, "NOM EQUIP : ".$arrDadesRebut[0]->nomEquip);
	$pdf->Text($x+10, $y+15, "TOTAL FITXES : ".$arrDadesRebut[0]->Jugadors."  x  55 .- ");
	$pdf->Text($x+150, $y+15, $arrDadesRebut[0]->totalFitxes." .- ");
	$pdf->Text($x+10, $y+25, "INSCRIPCIO  : ".$descompteEquipNou);
	$pdf->Text($x+150, $y+25, $inscripcio." .-");
	if ( $preferencia->preferenciaPartit != NULL || $preferencia->preferenciaPartit != ''){
		$pdf->Text($x+10, $y+35, "PREFERENCIA HORARIA  : ");
		$pdf->Text($x+150, $y+35, $prefHoraria." .-");
	}
	$pdf->Text($x+100, $y+45, "TOTAL A PAGAR :            ".$arrDadesRebut[0]->totalPagar);
	$pdf->Text($x+10, $y+65, "Firma i Segell de l'Entitat");
	if($i == 1){
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x+80, $y+85, "(Exemplar per la Balles)");
		$pdf->SetFont('Arial','',12);
		$pdf->Text($x-4, $y+95,"----------------------------------------------------------------------------------------------------------------------------------------------");
	}else{
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x+80, $y+85, "(Exemplar per l'equip)");
	}
	$y += 140;
}

$pdf->Output();
?>