<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once('../app/fpdf/fpdf.php');
define("Alturacarnet",0);
define("HPag",721.89);
define("MargeCarnet_a_Esquerra",100);

//$arrDadesEquip = getDadesEquip($idEquip);

function pintar_carnet($dadesJugador,$dadesEquip,&$pdf,$contador,$x,$y){
	$pdf->Rect($x, $y, 60, 35 );
	//$pdf->Rect($x+2,$y+2,20,25);
	if ($dadesJugador->foto != ''){
		$pdf->Image($dadesJugador->foto,$x+2,$y+8.5,20,22,'JPG');
	}else{
		$pdf->Image('../app/images/user.jpg',$x+2,$y+8.5,20,25,'JPG');
	}
	$y += 5;
	$pdf->SetFont('Arial','B',9);
	$pdf->Text($x+2,$y-1,"Associacio Basquet Lleure Sabadell");
	$pdf->SetFont('Arial','B',10);
	$nomEquip = $dadesEquip->nomEquip;
	if(strlen($nomEquip)> 25) {
		$pdf->SetFont('Arial','',6);
	}
	$pdf->Text($x+2,$y+2,"Equip : ".$dadesEquip->nomEquip);
	$pdf->SetFont('Arial','B',9);
	$y += 5;
	$pdf->Text($x+25,$y,$dadesEquip->nomDivisio);
	$pdf->SetFont('Arial','',9);
	$y += 4;
	$pdf->Text($x+25,$y,strtoupper($dadesJugador->nomJugador));
	$y += 3.5;
	$cognoms = strtoupper($dadesJugador->cognomsJugador);
	if(strlen($cognoms )> 16) {
		$pdf->SetFont('Arial','',7);
	}
	$pdf->Text($x+25,$y,$cognoms);
	$pdf->SetFont('Arial','',9);
	$y += 4;
	$pdf->Text($x+25,$y,$dadesJugador->dni);
	$y += 4;
	$pdf->Text($x+25,$y,$dadesJugador->dataNeixament);
	$y += 4;
	$poblacio = strtoupper($dadesJugador->poblacio);
	if(strlen($poblacio )> 17) {
		$pdf->SetFont('Arial','',6);
	}
	$pdf->Text($x+25,$y,$poblacio);
	$pdf->SetFont('Arial','',9);
	$y += 4;
	if ($dadesJugador->dorsal != '0'){
		$pdf->Text($x+25,$y,"DORSAL : ".$dadesJugador->dorsal);
	}else{
		$pdf->Text($x+25,$y,"DORSAL : ");
	}
	if ($dadesJugador->isPrimerCapita == 1){
		$pdf->setTextColor(255,0,0);
		if($dadesJugador->isEntrenador == 1){
			$pdf->Text($x+2,$y,"Entrenador");
		}else{
			$pdf->Text($x+2,$y,"Primer Capita");
		}
		
		$pdf->setTextColor(0,0,0);
		$pdf->Text(13, 53, "DADES CONTACTE 1er CAPITA : EMAIL : ".$dadesJugador->email."   , TEL. : ".$dadesJugador->telefon);
	}
	if ($dadesJugador->isSegonCapita == 1){
		$pdf->setTextColor(255,0,0);
		if($dadesJugador->isEntrenador == 1){
			$pdf->Text($x+2,$y,"Entrenador");
		}else{
			$pdf->Text($x+2,$y,"Segon Capita");
		}
		
		$pdf->setTextColor(0,0,0);
		$pdf->Text(13, 57, "DADES CONTACTE 2er CAPITA : EMAIL : ".$dadesJugador->email."   , TEL. : ".$dadesJugador->telefon);
	}
	if ($dadesJugador->isEntrenador == 1){
		$pdf->setTextColor(255,0,0);
		$pdf->Text($x+2,$y,"Entrenador");
		$pdf->setTextColor(0,0,0);
	}
}

if($_SESSION['USUARIO']['rol'] != 1){
	if($arrDadesEquip->idDivisio != $_SESSION['USUARIO']['idDivisio']){
		echo "<script type='text/javascript'>alert('No tens permís per veure aquest equip');</script>";
		die('no tens permis per veure aquest equip');

	}
}
$contador = 0;
$y=60;
$x=10;
$h = 0;
$i = 0;
$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
//$pdf->Rect(10, 10, 60, 40 );
$num=count($arrDadesJugador,0);
$pdf->SetFont('Arial','B',15);
$pdf->Text($x+29, $y-50, "ASSOCIACIO DE BASQUET DE LLEURE DE SABADELL");
$pdf->Line($x-9, $y-45, $x+199, $y-45);
$pdf->Rect($x, $y-42, 184, 40);
$pdf->Text($x+3, $y-36, "NOM EQUIP : ".$arrDadesEquip->nomEquip);
$pdf->Text($x+3, $y-29, "DIVISIO : ".$arrDadesEquip->nomDivisio);
$pdf->SetFont('Arial','',11);
$pdf->Text($x+3, $y-23, "COLOR SAMARRETA : ".$arrDadesEquip->colorSamarreta."     COLOR PANTALO : ".$arrDadesEquip->colorPantalo);
$pdf->Text($x+3, $y-19, "NUMERO DE JUGADORS : ".$num);
if ($equipAmbPistaJoc){
	$pdf->Text($x+3, $y-15,"PISTA JOC : ".$arrDadesEquip->nomPista."     HORARI : ".$arrDadesEquip->horariPista." h");
	$pdf->Text($x+3, $y-11,"DIRECCIO : ".$arrDadesEquip->direccioPista." ".$arrDadesEquip->poblacioPista);
	
}




if($arrDadesJugador==null) echo 'es null';

//if($num > 12) $num = 12; //Limitem a MAX_PDF

while($num>0){
	$h += 20;
	if($contador % 20==0 && $contador != 0) {$pdf->AddPage();$y=HPag;}
	pintar_carnet(array_shift($arrDadesJugador),$arrDadesEquip,$pdf,$contador,$x,$y);
	$y+=Alturacarnet*($contador % 2);//baixem l'altura d'un carnet en cas de ser imparell
	if ($contador == 0){
		$x = 10;//$y += 38;
		$contador++;
	}
	if ($contador == 3 || $contador == 6 || $contador == 9 || $contador == 12 || $contador == 15){
		$x = 10;
		$y += 38;
	}
	if ($contador == 1 || $contador == 4 || $contador == 7 || $contador == 10 || $contador == 13 || $contador == 16){
		$x = 72;
	}
	if ($contador == 2 || $contador == 5 || $contador == 8 || $contador == 11 || $contador == 14 || $contador == 17){
		$x = 134;
	}
	$contador++;
	$num--;
}
$pdf->Output();
?>
                            