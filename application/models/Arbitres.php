<?php

class Arbitres extends CI_Model {

	/**
	 * Constructor por defecto
	 */
	function __construct() {
		parent :: __construct();
	}

	public function loadTarifes(){
		$sql = 'select * from tarifes where tarifes.actiu="ACT"' ;
		
		return $this->db->query($sql)->result();

	}

	public function insertTarifa($tipus,$price){
		
		$sql = "INSERT INTO tarifes(tipus,preu,actiu) VALUES ('" . $tipus . "', '" . $price . "' ,'ACT')";
		$this->db->query($sql);
			
	}

	public function deleteTarifa($idTarifa) {

		$sql = "UPDATE tarifes SET actiu='DES' WHERE tarifes.idTarifa = '".$idTarifa."' ";

		$this->db->query($sql);

	}

	public function loadArbitres(){
		
		$sql = 'select * from arbitres where arbitres.actiu="ACT"' ;
		
		return $this->db->query($sql)->result();

	}

	public function loadArbitre($idArbitre) {
		$sql = 'select * from arbitres where idArbitre="'.$idArbitre.'" and arbitres.actiu="ACT"' ;
		
		return $this->db->query($sql)->result();
	}

    public function getArbitreByIdUser($idUser) {
        $sql = 'select * from arbitres where idUser="'.$idUser.'" ' ;

        return $this->db->query($sql)->result();
    }

    public function getNomAbritres(){
		
		$sql = 'select idArbitre, concat(nomArbitre," ",cognomsArbitre) as nomArbitre, mail from arbitres where arbitres.actiu="ACT"' ;
		
		return $this->db->query($sql)->result();

	}

	public function getMailArbitreById($id) {
        $sql = 'select mail from arbitres where arbitres.idArbitre="'.$id.'" ' ;

        return $this->db->query($sql)->result();
    }
    
	public function createArbitre($nom, $cognoms, $email, $telefon, $isArbitre, $isAnotador, $dilluns,$dimarts, $dimecres, $dijous, $divendres, $dissabte, $diumenge, $idUser, $observacions) {
		
		$sqlArbitre = "INSERT INTO arbitres(nomArbitre,cognomsArbitre,mail,telefon,isArbitre,isAnotador,dilluns,dimarts,dimecres,dijous,divendres,dissabte,diumenge,idUser,observacions, actiu) 
		VALUES ('" .$nom. "','" .$cognoms. "','" .$email. "','" .$telefon. "','" .$isArbitre. "','" .$isAnotador. "','" .$dilluns. "','" .$dimarts. "','" .$dimecres. "',
		'" .$dijous. "','" .$divendres. "','" .$dissabte. "','" .$diumenge. "','" .$idUser. "','" .$observacions. "', 'ACT') ";
		
		$this->db->query($sqlArbitre);
		
	}

	public function excel($table_name,$sql) { 
    //si existe la tabla
        $ret = false;
		
      //si es un array y no está vacio
      if(!empty($sql) && is_array($sql))
      {
      	foreach ($sql as $partit) {
      	    if ($partit['local'] != '' && $partit['visitant'] != '') {
                $query = "INSERT INTO partits(local,visitant,data,hora,pista,isFemeni) 
                        VALUES ('" . $partit['local'] . "','" . $partit['visitant'] . "','" . $partit['data'] . "','" . $partit['hora'] . "','" . $partit['pista'] . "','" . $partit['isFemeni'] . "')";
                $this->db->query($query);

                $ret = TRUE;
            }
      	}

      	return $ret;
        //si se lleva a cabo la inserción
        /*if($this->db->insert_batch($table_name, $sql)) {
          return TRUE;
        }else{
          return FALSE;
        }*/
      }
  	}

  	public function loadPartits() {
		$sql = 'select partits.idPartit, partits.local, partits.visitant, STR_TO_DATE(partits.data,"%d/%m/%Y") as data, partits.hora, 
		partits.pista, partits.isSuspes, partits.idArbitre, partits.idTarifaArbitre, partits.idAnotador,
		partits.idTarifaAnotador, partits.isAcceptatArbitre, partits.isPagatArbitre, partits.isPagatAnotador,
		partits.isAcceptatAnotador, partits.actiu, 
		(SELECT concat(nomArbitre," ",cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partits.idArbitre limit 1) as nomArbitre,
		(SELECT concat(nomArbitre," ",cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partits.idAnotador limit 1) as nomAnotador,
		(SELECT preu from tarifes where tarifes.idTarifa=partits.idTarifaArbitre limit 1) as tarifaArbitre,
		(SELECT preu from tarifes where tarifes.idTarifa=partits.idTarifaAnotador limit 1) as tarifaAnotador
		from partits where partits.actiu="ACT"' ;
		
		return $this->db->query($sql)->result();

	}

    public function loadPartitsMasculins() {
        $sql = 'select partits.idPartit, partits.local, partits.visitant, STR_TO_DATE(partits.data,"%d/%m/%Y") as data, partits.hora, 
		partits.pista, partits.isSuspes, partits.idArbitre, partits.idTarifaArbitre, partits.idAnotador,
		partits.idTarifaAnotador,partits.isAcceptatArbitre, partits.isPagatArbitre, partits.isPagatAnotador,
		partits.isAcceptatAnotador,partits.actiu, 
		(SELECT concat(nomArbitre," ",cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partits.idArbitre limit 1) as nomArbitre,
		(SELECT concat(nomArbitre," ",cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partits.idAnotador limit 1) as nomAnotador,
		(SELECT preu from tarifes where tarifes.idTarifa=partits.idTarifaArbitre limit 1) as tarifaArbitre,
		(SELECT preu from tarifes where tarifes.idTarifa=partits.idTarifaAnotador limit 1) as tarifaAnotador
		from partits where partits.actiu="ACT" and partits.isFemeni = 0 ' ;

        return $this->db->query($sql)->result();

    }

    public function loadPartitsFemenins() {
        $sql = 'select partits.idPartit, partits.local, partits.visitant, STR_TO_DATE(partits.data,"%d/%m/%Y") as data, partits.hora, 
		partits.pista, partits.isSuspes, partits.idArbitre, partits.idTarifaArbitre, partits.idAnotador,
		partits.idTarifaAnotador,partits.isAcceptatArbitre, partits.isPagatArbitre, partits.isPagatAnotador,
		partits.isAcceptatAnotador, partits.actiu, 
		(SELECT concat(nomArbitre," ",cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partits.idArbitre limit 1) as nomArbitre,
		(SELECT concat(nomArbitre," ",cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partits.idAnotador limit 1) as nomAnotador,
		(SELECT preu from tarifes where tarifes.idTarifa=partits.idTarifaArbitre limit 1) as tarifaArbitre,
		(SELECT preu from tarifes where tarifes.idTarifa=partits.idTarifaAnotador limit 1) as tarifaAnotador
		from partits where partits.actiu="ACT" and partits.isFemeni = 1 ' ;

        return $this->db->query($sql)->result();

    }

	public function loadFranjaDelegats() {
	    $sql = 'SELECT partitDelegat.idPartitDelegat as idPartitDelegat, STR_TO_DATE(partitDelegat.data,"%d/%m/%Y") as data, partitDelegat.hora as hora ,
	            partitDelegat.pista as pista, partitDelegat.idDelegat as idDelegat,
	            (SELECT concat(nomArbitre," ",cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partitDelegat.idDelegat limit 1) as nomDelegat
	            FROM partitDelegat where partitDelegat.actiu = "ACT" ';

        return $this->db->query($sql)->result();
    }

	public function getPartit($idPartit) {

		$sql= "select partits.idPartit, partits.local, partits.visitant, partits.data as data, partits.hora, 
				partits.pista, partits.isSuspes, partits.idArbitre, partits.idTarifaArbitre, partits.idAnotador,
				partits.idTarifaAnotador,partits.isFemeni, partits.isAcceptatArbitre, partits.isPagatArbitre, partits.isPagatAnotador,
		        partits.isAcceptatAnotador,partits.actiu, 
				(SELECT concat(nomArbitre,' ',cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partits.idArbitre limit 1) as nomArbitre,
				(SELECT concat(nomArbitre,' ',cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partits.idAnotador limit 1) as nomAnotador,
				(SELECT preu from tarifes where tarifes.idTarifa=partits.idTarifaArbitre limit 1) as tarifaArbitre,
				(SELECT preu from tarifes where tarifes.idTarifa=partits.idTarifaAnotador limit 1) as tarifaAnotador
				from partits where partits.actiu='ACT' and partits.idPartit = '" .$idPartit."' ";
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function updatePartit($idPartit,$local,$visitant,$data,$hora,$pista,$isSuspes,$idArbitre,$idTarifaArbitre,$idAnotador,$idTarifaAnotador, $isFemeni) {
		$ret = false;
		$sql = "UPDATE partits set local='".$local."', visitant='".$visitant."', data='".$data."', hora='".$hora."', pista='".$pista."',isSuspes='".$isSuspes."',
				idArbitre='".$idArbitre."', idTarifaArbitre='".$idTarifaArbitre."', idAnotador='".$idAnotador."', idTarifaAnotador='".$idTarifaAnotador."' ,isFemeni='".$isFemeni."'
				WHERE idPartit='".$idPartit."' "; 


		 if($this->db->query($sql)) {
		 	$ret = true;
		 }
		
		 return $ret;

	}

	public function acceptarPartitArbitre($idPartit, $idArbitre ) {
        $ret = false;
        $sql = "UPDATE partits set isAcceptatArbitre='1'WHERE idPartit='".$idPartit."' AND idArbitre ='".$idArbitre."' ";

        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;
    }

    public function acceptarPartitAnotador($idPartit, $idArbitre ) {
        $ret = false;
        $sql = "UPDATE partits set isAcceptatAnotador='1' WHERE idPartit='".$idPartit."' AND idAnotador ='".$idArbitre."' ";

        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;
    }

    public function updateArbitre($idPartit, $idArbitre ) {
        $ret = false;
        $sql = "UPDATE partits set idArbitre='".$idArbitre."' WHERE idPartit='".$idPartit."' ";

        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;
    }

    public function updateAnotador($idPartit, $idArbitre ) {
        $ret = false;
        $sql = "UPDATE partits set idAnotador='".$idArbitre."' WHERE idPartit='".$idPartit."' ";

        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;
    }

    public function updatePistaPartit($idPartit, $pista ) {
        $ret = false;
        $sql = "UPDATE partits set pista='".$pista."' WHERE idPartit='".$idPartit."' ";

        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;
    }

    public function createPartit($local,$visitant,$data,$hora,$pista,$isSuspes,$idArbitre,$idTarifaArbitre,$idAnotador,$idTarifaAnotador,$isFemeni) {
        $ret = false;
        $sql = "INSERT INTO partits (local,visitant,data,hora,pista,isSuspes,idArbitre,idTarifaArbitre,idAnotador,idTarifaAnotador,isFemeni,actiu) VALUES ('".$local."','".$visitant."','".$data."','".$hora."','".$pista."','".$isSuspes."',
                '".$idArbitre."','".$idTarifaArbitre."', '".$idAnotador."','".$idTarifaAnotador."','".$isFemeni."', 'ACT' )" ;

        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;

    }

    public function createFranjaDelegat($data,$hora,$pista,$idDelegat,$idTarifaDelegat) {
        $ret = false;
        $sql = "INSERT INTO partitDelegat (data,hora,pista,idDelegat,idTarifaDelegat,actiu) VALUES ('".$data."','".$hora."','".$pista."',
                '".$idDelegat."','".$idTarifaDelegat."', 'ACT' )" ;

        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;
    }

	public function showPartitsArbitre($idArbitre){
		$ret = false;
		$sql = "SELECT distinct partits.idPartit as idPartit, partits.local as local, partits.visitant as visitant, STR_TO_DATE(partits.data,'%d/%m/%Y') as data, partits.hora as hora, partits.pista as pista, 
				partits.isSuspes as isSuspes, partits.isFemeni as isFemeni, partits.idArbitre, partits.idAnotador, partits.isAcceptatArbitre, partits.isPagatArbitre,
				partits.isAcceptatAnotador, partits.isPagatAnotador,
				IF(idArbitre = '".$idArbitre."' ,(select tarifes.preu from tarifes where tarifes.idTarifa = idTarifaArbitre), 0) as arbitre, 
				IF(idAnotador = '".$idArbitre."' ,(select tarifes.preu from tarifes where tarifes.idTarifa = idTarifaAnotador), 0) as anotador
				from partits where (idArbitre = '".$idArbitre."' or idAnotador = '".$idArbitre."' )";

		$consulta = $this->db->query($sql);
		
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function showPartitsDelegat($idArbitre) {
        $sql = 'SELECT partitDelegat.idPartitDelegat as idPartitDelegat, partitDelegat.data as data, partitDelegat.hora as hora ,
	            partitDelegat.pista as pista, 
	            (select tarifes.preu from tarifes where tarifes.idTarifa = idTarifaDelegat) as delegat
	            FROM partitDelegat where partitDelegat.idDelegat = "'.$idArbitre.'" and partitDelegat.actiu = "ACT" ';
        $consulta = $this->db->query($sql);

        if($consulta->num_rows() > 0){
            return $consulta->result();
        } else {
            return null;
        }
    }

	public function showPartitsArbitreByDate($idArbitre, $dataInicial, $dataFinal){
		$ret = false;
		$sql = "SELECT distinct partits.idPartit as idPartit, partits.local as local, partits.visitant as visitant, STR_TO_DATE(partits.data,'%d/%m/%Y') as data, partits.hora as hora, partits.pista as pista, 
				partits.isSuspes as isSuspes, partits.isFemeni as isFemeni,partits.idArbitre, partits.idAnotador, partits.isAcceptatArbitre, partits.isPagatArbitre,
				partits.isAcceptatAnotador, partits.isPagatAnotador,
				IF(idArbitre = '".$idArbitre."' ,(select tarifes.preu from tarifes where tarifes.idTarifa = idTarifaArbitre), 0) as arbitre, 
				IF(idAnotador = '".$idArbitre."' ,(select tarifes.preu from tarifes where tarifes.idTarifa = idTarifaAnotador), 0) as anotador
				from partits where (idArbitre = '".$idArbitre."' or idAnotador = '".$idArbitre."' ) AND
                (STR_TO_DATE(partits.data,'%d/%m/%Y') BETWEEN '".$dataInicial."' AND '".$dataFinal."' )";
		/*and (partits.data like '%/".$dataInicial."' or partits.data like '%/".$dataFinal."')";
		 * and ((partits.data >= STR_TO_DATE('".$dataInicial."', '%Y/%m/%d')) and (partits.data <= STR_TO_DATE('".$dataFinal."', '%Y/%m/%d'))) ";*/

		$consulta = $this->db->query($sql);
		
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function showPartitsDelegatByDate($idArbitre, $dataInicial, $dataFinal) {
	    $ret = false;
        $sql = "SELECT partitDelegat.idPartitDelegat as idPartitDelegat, partitDelegat.data as data, partitDelegat.hora as hora ,
	            partitDelegat.pista as pista, 
	            (select tarifes.preu from tarifes where tarifes.idTarifa = idTarifaDelegat) as delegat
	            FROM partitDelegat where partitDelegat.idDelegat = '".$idArbitre."' and partitDelegat.actiu = 'ACT' 
	            and (partitDelegat.data like '%/".$dataInicial."' or partitDelegat.data like '%/".$dataFinal."')";
        $consulta = $this->db->query($sql);

        if($consulta->num_rows() > 0){
            return $consulta->result();
        } else {
            return null;
        }
    }

    public function showPartitsMasculinsByDate($dataInicial, $dataFinal) {
        $sql = "select partits.idPartit, partits.local, partits.visitant, STR_TO_DATE(partits.data,'%d/%m/%Y') as data, partits.hora, 
		partits.pista, partits.isSuspes, partits.idArbitre, partits.idTarifaArbitre, partits.idAnotador,
		partits.idTarifaAnotador,partits.isAcceptatArbitre, partits.isPagatArbitre, partits.isPagatAnotador,
		partits.isAcceptatAnotador,partits.actiu, 
		(SELECT concat(nomArbitre,' ',cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partits.idArbitre limit 1) as nomArbitre,
		(SELECT concat(nomArbitre,' ',cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partits.idAnotador limit 1) as nomAnotador,
		(SELECT preu from tarifes where tarifes.idTarifa=partits.idTarifaArbitre limit 1) as tarifaArbitre,
		(SELECT preu from tarifes where tarifes.idTarifa=partits.idTarifaAnotador limit 1) as tarifaAnotador
		from partits where partits.actiu='ACT' and partits.isFemeni = 0 AND
        (STR_TO_DATE(partits.data,'%d/%m/%Y') BETWEEN '".$dataInicial."' AND '".$dataFinal."' )" ;

        return $this->db->query($sql)->result();

    }

    public function showPartitsFemeninsByDate($dataInicial, $dataFinal) {
        $sql = "select partits.idPartit, partits.local, partits.visitant, STR_TO_DATE(partits.data,'%d/%m/%Y') as data, partits.hora, 
		partits.pista, partits.isSuspes, partits.idArbitre, partits.idTarifaArbitre, partits.idAnotador,
		partits.idTarifaAnotador,partits.isAcceptatArbitre, partits.isPagatArbitre, partits.isPagatAnotador,
		partits.isAcceptatAnotador, partits.actiu, 
		(SELECT concat(nomArbitre,' ',cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partits.idArbitre limit 1) as nomArbitre,
		(SELECT concat(nomArbitre,' ',cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partits.idAnotador limit 1) as nomAnotador,
		(SELECT preu from tarifes where tarifes.idTarifa=partits.idTarifaArbitre limit 1) as tarifaArbitre,
		(SELECT preu from tarifes where tarifes.idTarifa=partits.idTarifaAnotador limit 1) as tarifaAnotador
		from partits where partits.actiu='ACT' and partits.isFemeni = 1 AND
        (STR_TO_DATE(partits.data,'%d/%m/%Y') BETWEEN '".$dataInicial."' AND '".$dataFinal."' )" ;

        return $this->db->query($sql)->result();

    }

    public function showFranjaDelegatByDate($dataInicial, $dataFinal) {
        $sql = "SELECT partitDelegat.idPartitDelegat as idPartitDelegat, STR_TO_DATE(partitDelegat.data,'%d/%m/%Y') as data, partitDelegat.hora as hora ,
	            partitDelegat.pista as pista, partitDelegat.idDelegat as idDelegat,
	            (SELECT concat(nomArbitre,' ',cognomsArbitre) From arbitres WHERE arbitres.idArbitre=partitDelegat.idDelegat limit 1) as nomDelegat
	            FROM partitDelegat where partitDelegat.actiu = 'ACT' AND
                (STR_TO_DATE(partitDelegat.data,'%d/%m/%Y') BETWEEN '".$dataInicial."' AND '".$dataFinal."' )";

        return $this->db->query($sql)->result();
    }

    public function loadPistes() {
        $sql = 'select * from pistaJoc';

        return $this->db->query($sql)->result();
    }

    public function createPista($nom,$direccio,$poblacio,$horari,$isForaSabadell) {
        $ret = false;
        $sql = "INSERT INTO pistaJoc (nomPista,horariPista, poblacioPista, direccioPista, isPistaFora) VALUES (
              '".$nom."','".$direccio."','".$poblacio."','".$horari."','".$isForaSabadell."' )" ;

        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;
    }

    public function getPista($idPista) {
        $sql="select * from pistaJoc where idPistaJoc = '".$idPista."' ";

        $consulta = $this->db->query($sql);
        if($consulta->num_rows() > 0){
            return $consulta->result();
        } else {
            return null;
        }
    }

    public function updatePista($idPista,$nom,$direccio,$poblacio,$horari,$isForaSabadell) {
        $ret = false;
        $sql = "UPDATE pistaJoc set nomPista='".$nom."', horariPista='".$horari."', poblacioPista='".$poblacio."', direccioPista='".$direccio."', isPistaFora='".$isForaSabadell."'
				WHERE idPistaJoc='".$idPista."' ";


        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;
    }

    public function deletePista($idPista) {
        $sql = "UPDATE pistaJoc SET actiu='DES' WHERE pistaJoc.idPistaJoc = '".$idPista."' ";

        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;
    }

    public function activatePista($idPista) {
        $sql = "UPDATE pistaJoc SET actiu='ACT' WHERE pistaJoc.idPistaJoc = '".$idPista."' ";

        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;
    }

    public function desarDessignacio($dessignacio, $nom, $mails, $data) {
        $ret = false;
        $sql = "INSERT INTO dessignacio (dessignacio,nom,mails, data,isEnviat, Actiu) VALUES (
                '".addslashes($dessignacio)."','".$nom."','".$mails."','".$data."', '0', 'ACT' )" ;

        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;

    }

    public function updateDessignacio($idDessignacio, $dessignacio, $nom, $mails, $data) {
        $ret = false;
        $sql = "UPDATE dessignacio SET dessignacio = '".addslashes($dessignacio)."', nom = '".$nom."', mails = '".$mails."', data = '".$data."'
                WHERE idDessignacio = '".$idDessignacio."' ";

        if($this->db->query($sql)) {
            $ret = true;
        }

        return $ret;

    }

    public function loadDessignacions(){
        $sql = 'select * from dessignacio where actiu="ACT"' ;

        return $this->db->query($sql)->result();

    }

    public function getDessignacio($idDessignacio) {
        $sql="select * from dessignacio where idDessignacio = '".$idDessignacio."' ";

        $consulta = $this->db->query($sql);
        if($consulta->num_rows() > 0){
            return $consulta->result();
        } else {
            return null;
        }
    }
	
}