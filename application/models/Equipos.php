<?php

class Equipos extends CI_Model {

	/**
	 * Constructor por defecto
	 */
	function __construct() {
		parent :: __construct();
	}

	
	public function loginUsuario($user, $pass) {
	 	$ret = false;
	 	$sql = 'SELECT *
				FROM usuari
				WHERE nomUser = "' . $user . '" AND pass = "' . $pass . '" AND actiu = "ACT" ';
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->row();
		}
		else
		{
			return null;
		} 
	}

	public function getNomEquip() {
		$user = $_SESSION ['USUARIO'] ['nomUser'];
		$idEquip = $_SESSION['USUARIO']['idEquip'];
		$idUser = $_SESSION['USUARIO']['idUser'];
		$sql = 'SELECT nomEquip from equip
					inner join usuari on usuari.idEquip = equip.idEquip
					WHERE usuari.idUser = "' . $idUser . '" and equip.actiu = "ACT" LIMIT 1';
		
		return $this->db->query($sql)->row();
	}

	public function getNomDivisio($idDivisio){
		$sql = 'select divisio.nomDivisio where divisio.idDivisio="' . $idDivisio . '" ' ;
		
		return $this->db->query($sql)->row();
	}

	public function getDadesDivisio($idDivisio){
		$sql = 'select * from divisio where divisio.idDivisio="' . $idDivisio . '" ' ;
		
		return $this->db->query($sql)->row();
	}

	public function getDivisions(){
		$sql = 'select * from divisio'  ;
		
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getDivisioByIdDivisio($idDivisio) { 
		$sql = 'select * from divisio where idDivisio = '. $idDivisio;
		
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getNumJugadorsEquip($idEquip) {
		$sql = "SELECT count(jugador.idJugador) as num
				FROM jugador WHERE jugador.idEquip = '".$idEquip."' and jugador.actiu = 'ACT' ";

		return $this->db->query($sql)->row();
	}

	public function getDadesEquip($idEquip) {
		$dades = array();
		$sql = 'SELECT equip.idEquip, equip.nomEquip, equip.preferenciaPartit, equip.colorPantalo, equip.colorSamarreta, divisio.nomDivisio,pistaJoc.nomPista,pistaJoc.direccioPista,
				pistaJoc.poblacioPista,pistaJoc.horariPista, equip.idDivisio,equip.observacions
			  	FROM equip
			  	LEFT JOIN divisio on equip.idDivisio = divisio.idDivisio
				LEFT JOIN pistaJoc on equip.idEquip = pistaJoc.idEquip
				WHERE equip.idEquip = "' . $idEquip . '" and equip.actiu = "ACT"';
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->row();
		} else {
			return null;
		}
	}

	public function getDadesTriptic($idEquip) {
		$sql = 'SELECT * from jugador
				WHERE jugador.idEquip = "' . $idEquip . '" and jugador.actiu = "ACT" 
				ORDER BY isPrimerCapita DESC , isSegonCapita DESC ';

		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getDadesTripticHistoric($idEquip) {
		$sql = 'SELECT * from jugador
				WHERE jugador.idEquip = "' . $idEquip . '" and jugador.actiu = "DES" 
				ORDER BY isPrimerCapita DESC , isSegonCapita DESC ';

		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getEquipsNoActius() {
		$sql = 'SELECT * from equip
				WHERE equip.actiu = "DES" 
				ORDER BY nomEquip ASC ';

		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getNumEquipsInscrits(){
		$sql = "SELECT count(equip.idEquip) as num FROM equip where actiu = 'ACT' and idEquip <> 52 and idEquip <> 57 ";
		//$sql = "SELECT count(equip.idEquip) as num FROM equip where actiu = 'ACT'";

		return $this->db->query($sql)->row();
	}

	public function getNumJugadorsTotals(){
		$sql = "SELECT count(jugador.idJugador) as num FROM jugador where actiu = 'ACT' ";

		return $this->db->query($sql)->row();
	}

	public function getEquiposBuscador() {
		$sql = "select equip.nomEquip from equip where idEquip <> 1 and actiu='ACT'";
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function mostraEquips(){
		$dades = array();
		$sql = 'SELECT equip.idEquip,equip.nomEquip,usuari.nomUser, usuari.pass, equip.observacions, divisio.nomDivisio, equip.isPagat from equip 
						inner join usuari on usuari.idEquip = equip.IdEquip 
						left join divisio on divisio.idDivisio = equip.idDivisio 
						where equip.actiu = "ACT" and equip.idEquip <> 1 and equip.idEquip <> 52 and equip.idEquip <> 57
						order by equip.nomEquip ASC';
		$queryEquip = $this->db->query($sql);
		foreach ($queryEquip->result_array() as $desti){
			$idEquip = $desti['idEquip'];
			$pagament = $desti['isPagat'];
			if ($pagament == 0){
				$isPagat = 'NO';
				$pagament = "<a href='".base_url()."equiposBalles/afegirPagament/$idEquip/1'><button type='button' class='btn btn-success'>PAGAR</button></a>";
			}else{
				$isPagat = 'SI';
				$pagament = "<a href='".base_url()."equiposBalles/afegirPagament/$idEquip/0'><button type='button' class='btn btn-danger'>TREURE PAGAMENT</button></a>";
			}
			$desti['isPagat'] = $isPagat;
			$desti['pagament'] = $pagament;
			if ($desti['idEquip'] != 1){
	                $numJugadorsEquip = $this->getNumJugadorsEquip($desti['idEquip']);
	                $desti['numJugadorsEquip'] = $numJugadorsEquip->num;
	                //if ($desti['idEquip'] == 52){$numJugadorsEquip -= 2;}
	                //if ($desti['idEquip'] == 50){$numJugadorsEquip -= 1;}

				if ($desti['nomDivisio'] == ''){$nomDivisio = 'Sense Determinar';}else{$nomDivisio = $desti['nomDivisio'];}
				$desti['nomDivisio'] = $nomDivisio;
		    	$tabla = "<tr>".
		    				"<td>".$desti['idEquip']."</td>".
		    				"<td><a href='".base_url()."equiposBalles/mostraDadesEquip/".$desti['idEquip']."'>".$desti['nomEquip']."</a></td>".
		    				"<td><b>".$desti['numJugadorsEquip']."</b></td><td>".$desti['nomUser']."</td>".
		    				"<td>".$desti['pass']."</td><td>".$desti['nomDivisio'] ."</td>".
		    				"<td>".$desti['observacions']."</td>".
		    				"<td>".$desti['isPagat']."</td><td>".$desti['pagament']."</td>".
		    			"</tr>";
		    }

		    array_push($dades, $desti);
		}

		return $dades;
	}

	public function getEquipsByidDivisio($idDivisio){
		$sql = 'SELECT equip.idEquip, equip.nomEquip, jugador.nomJugador, jugador.cognomsJugador, jugador.email, jugador.telefon, equip.preferenciaPartit, equip.observacions from equip 
				inner join divisio on divisio.idDivisio = equip.idDivisio 
				left join jugador on jugador.idEquip = equip.idEquip 
				WHERE divisio.idDivisio = "' . $idDivisio. '"  and equip.actiu = "ACT" and jugador.isPrimerCapita = 1 and jugador.actiu = "ACT" group by equip.nomEquip order by equip.nomEquip ASC';

		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getAllEquips(){
		$sql = 'SELECT equip.idEquip, equip.nomEquip from equip 
				WHERE  equip.actiu = "ACT"  order by equip.nomEquip ASC';

		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getJugadorsByIdEquip($idEquip){
		$sql = 'SELECT jugador.idJugador, jugador.nomJugador, jugador.cognomsJugador from jugador 
				WHERE  jugador.actiu = "ACT"  and jugador.idEquip = "'.$idEquip.'" order by jugador.idJugador ASC';

		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getEquipsByidDivisioPista($idDivisio){
		$sql = 'SELECT equip.idEquip, equip.nomEquip, jugador.nomJugador, jugador.cognomsJugador, jugador.email, jugador.telefon, equip.preferenciaPartit, pistaJoc.direccioPista,pistaJoc.horariPista,pistaJoc.poblacioPista, equip.observacions from equip 
			inner join divisio on divisio.idDivisio = equip.idDivisio 
			left join jugador on jugador.idEquip = equip.idEquip 
			left join pistaJoc on pistaJoc.idEquip = equip.idEquip
			WHERE divisio.idDivisio = "' . $idDivisio. '" and equip.actiu = "ACT" 
			and jugador.actiu = "ACT" and jugador.isPrimerCapita = 1
			group by equip.nomEquip order by equip.nomEquip ASC';
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getIdDivsiobyIdEquip($idEquip){
		$sql = 'SELECT divisio.idDivisio 
				from divisio 
				left join equip on equip.idDivisio = divisio.idDivisio 
				where equip.idEquip="' . ($idEquip) . '" and equip.actiu = "ACT" limit 1 ';
		return $this->db->query($sql)->row();
		
	}

	public function getIdPistaJocbyIdEquip($idEquip){
		$sql = 'SELECT pistaJoc.idPistaJoc 
				from pistaJoc 
				left join equip on equip.idEquip = pistaJoc.idEquip 
				where equip.idEquip = "' . $idEquip . '" and equip.actiu = "ACT" limit 1 ';
		return $this->db->query($sql)->row();
	}

	public function getPreferenciaByIdEquip($idEquip){
		$sql = 'SELECT equip.preferenciaPartit from equip where equip.idEquip = "' . ($idEquip) . '" and equip.actiu = "ACT" limit 1 ';
		return $this->db->query($sql)->row();
	}

	public function afegirPagament($idEquip,$isPagat){
		$sql = "UPDATE equip SET isPagat='".$isPagat."' WHERE equip.idEquip = '".$idEquip."' ";
		$this->db->query($sql);
	}

	public function deleteJugador($idJugador){
		$sql = "UPDATE jugador SET actiu='DES' WHERE jugador.idJugador = '".$idJugador."' ";
		$this->db->query($sql);
	}

	public function setPagamentFalse($idEquip) {
		$sql = "UPDATE equip set isPagat = 0 where idEquip = '".$idEquip."' ";
		$this->db->query($sql);
	}

	public function getPagament($idEquip){
		
		$inscripcio = '';$esportivitat = 'NO';$pistaPropia = 'NO';$preferencia = 'NO';
		$descompteEquipNou = 'NO';
		$idDivisio = $this->getIdDivsiobyIdEquip($idEquip);
		//echo($idEquip);die();
		$pistaJoc = $this->getIdPistaJocbyIdEquip($idEquip);
		$pref = $this->getPreferenciaByIdEquip($idEquip);
		if ($idDivisio->idDivisio == 1 || $idDivisio->idDivisio == 2 || $idDivisio->idDivisio == 5){
			$inscripcio = 998;
		}elseif ($idDivisio->idDivisio == 3 || $idDivisio->idDivisio == 4){
			if (is_object($pistaJoc)){
			//if ($pistaJoc->idPistaJoc != '' || $pistaJoc->idPistaJoc != 0){
				$inscripcio = 720;
				$pistaPropia = 'SI';
			}else{
				$inscripcio = 850;
			}
		}else{
			$inscripcio = 0;
		}
		if ($idEquip == 34 || $idEquip == 98 || $idEquip == 32 || $idEquip == 51){
			$esportivitat = 'SI (25%)';
			$inscripcio -= ($inscripcio * 0.25);
		}elseif ($idEquip == 105 || $idEquip == 30){
			$esportivitat = 'SI (12.5%)';
			$inscripcio -= ($inscripcio * 0.125);
		}
		if ($pref->preferenciaPartit != NULL || $pref->preferenciaPartit != ''){
		     $preferencia = 'SI (70€)';
		}
		if($idEquip == 55 ) {
			$descompteEquipNou = 'SI(-90€)';
			$inscripcio -= 90;
		}
		/*
		if($idEquip == 39) {
			$descompteEquipNou = 'SI(-180€)';
			$inscripcio -= 180;
		}*/

		$dades = array();
		$sql = 'SELECT equip.idEquip, equip.nomEquip, count(jugador.idJugador) as jugadors, (count(jugador.idJugador)*55) as totalFitxes, ((count(jugador.idJugador)*55)+'.$inscripcio.') as totalPagar, equip.isPagat 
				FROM equip 
				inner join divisio on divisio.idDivisio = equip.idDivisio 
				inner join jugador on jugador.idEquip = equip.idEquip 
				WHERE divisio.idDivisio = "' . $idDivisio->idDivisio. '" and equip.idEquip = "' . $idEquip. '" and equip.actiu = "ACT" and jugador.actiu = "ACT" ';

		$queryPagament = $this->db->query($sql);
		foreach ($queryPagament->result_array() as $desti){
       		$idEquip = $desti['idEquip'];
			$pagament = $desti['isPagat'];
	        if ($pagament == 0){
				$isPagat = 'NO';
				$pagament = "<a href='".base_url()."equiposBalles/afegirPagament/$idEquip/1'><button type='button' class='btn btn-success'>PAGAR</button></a>";
			}else{
				$isPagat = 'SI';
				$pagament = "<a href='".base_url()."equiposBalles/afegirPagament/$idEquip/0'><button type='button' class='btn btn-danger'>TREURE PAGAMENT</button></a>";
			}
			if($_SESSION['USUARIO']['rol'] != 1){$pagament = '';}
			if($preferencia != 'NO'){$desti['totalPagar'] += 70;}
			$desti['pagament'] = $pagament;
			$desti['isPagat'] = $isPagat;
			$desti['pistaPropia'] = $pistaPropia;
			$desti['esportivitat'] = $esportivitat;
			$desti['descompteEquipNou'] = $descompteEquipNou;
			$desti['preferencia'] = $preferencia;
			$desti['inscripcio'] = $inscripcio;

			return $desti;
			
			/*if($idEquip == 52 && $_SESSION['USUARIO']['rol'] == 1){
				$desti['Jugadors'] -= 2;
				$desti['totalFitxes'] = $desti['Jugadors'] * 40;
				$desti['totalPagar'] -= 80;
			}*/

			/*if($idEquip == 50 && $_SESSION['USUARIO']['rol'] == 1){
				$desti['Jugadors'] -= 1;
				$desti['totalFitxes'] = $desti['Jugadors'] * 40;
				$desti['totalPagar'] -= 80;
			}*/
		}
	}

	public function mostraJugadors($arrJugadors){
		$i = 1;
		$cap1 = '';$cap2 = '';$email = ''; $tel = '';$fitxaTipus='';
		foreach ($arrJugadors as $jugador){
		if($jugador['isEntrenador'] == 1){ $fitxaTipus = 'Entrenador'; }else{ $fitxaTipus = 'Jugador'; }
		if($i == 1){$cap1 = '-Primer Capita';$email =$jugador['email'];$tel = $jugador['telefon'];}else{$cap1 = '';$email = '-'; $tel = '-';}
		if($i == 2){ $cap2 = '-Segon Capita';$email =$jugador['email'];$tel = $jugador['telefon'];}else{$cap2 = '';}
			echo "<tr><td>jugador ".$i.$cap1.$cap2."</td><td>".$jugador['dni']."</td><td>".$jugador['nomJugador']." ".$jugador['cognomsJugador']."</td><td>".calcularEdad($jugador['dataNeixament'])."</td><td>".$email."</td><td>".$tel."</td><td>".$fitxaTipus."</td></tr>";
			$i++;
			//<td><img style='width:50px; height:50px;' src='".$jugador['foto']."'></td>
		}
	}
	
	public function crearEquipo($nomEquip,$usuari,$pass){
		$sql = "INSERT INTO equip(nomEquip,idDivisio,actiu) VALUES ('" . $nomEquip. "',5, 'ACT')";
		$this->db->query($sql);
		$idEquip = $this->db->insert_id();//mysql_insert_id();
		$sqlUser = "INSERT INTO usuari(nomUser,pass,idEquip,rol, actiu) VALUES ('" . $usuari. "','" . $pass. "','" . $idEquip. "','2', 'ACT') ";
		$this->db->query($sqlUser);
		/*mkdir("../images/".$idEquip."/", 0777);
		mkdir("../images/".$idEquip."/foto/", 0777);
		mkdir("../images/".$idEquip."/dni/", 0777);*/
		$rutaBase = 'public/equipos/images/'.$idEquip;
		//echo $rutaBase;//die();
		mkdir($rutaBase,0777, TRUE);
		$ruta = 'public/equipos/images/'.$idEquip;
		//chmod($ruta,0777);
		mkdir($ruta.'/foto',0777, TRUE);
		//chmod($ruta.'/foto',0777);
		mkdir($ruta.'/dni', 0777, TRUE);
		//chmod($ruta.'/dni',0777);
	}

	public function getIfEquipAmbPistaJoc($idEquip){
		$ret = true;
		$sql = "SELECT * FROM pistaJoc inner join equip on equip.idEquip = pistaJoc.idEquip WHERE pistaJoc.idEquip = '".$idEquip."' and equip.actiu = 'ACT' ";
		$result = $this->db->query($sql);
		$consulta = $this->db->query($sql);
			if($consulta->num_rows() > 0){
				return true;
			} else {
				return false;
			}
		//echo "numero de pistas".$ret;
		}

	public function insertJugador($idEquip,$dni,$nom,$cognoms,$email,$dorsal,$dataNeixament,$poblacio,$telefon,$isPrimerCapita,$isSegonCapita,$isEntrenador,$foto,$dniAdj,$dataIntroduccio){
		
			$sql = "INSERT INTO jugador(nomJugador,cognomsJugador,dni,email,telefon,dataNeixament,poblacio,dorsal,isPrimerCapita,isSegonCapita,isEntrenador,foto,dniAdj,idEquip,dataIntroduccio,actiu) 
			VALUES ('" . $nom . "', '" . $cognoms . "', '" . $dni . "', '" . $email . "', '" . $telefon . "','" . $dataNeixament . "','" . $poblacio . "','" . $dorsal . "', '" . $isPrimerCapita . "', '" . $isSegonCapita . "','" . $isEntrenador. "', '" . $foto . "','" . $dniAdj . "','" . $idEquip ."','" . $dataIntroduccio ."','ACT')";
			$this->db->query($sql);
			//echo"Entra Correctament a INSERT ".$dni."</br>";
			//echo "<script language='JavaScript'>alert('Dni no correcte');</script>";
		}

		public function setPreferenciaHorari($preferencia,$idEquip){
		$sql = "UPDATE equip SET preferenciaPartit='".$preferencia."' WHERE equip.idEquip = '".$idEquip."' ";
		$this->db->query($sql);
		//echo"entra Correctament en Horari</br>";
	}

	public function setDivisio($divisio,$idEquip){
		
		$sql = "UPDATE equip SET idDivisio='".($divisio-1)."' WHERE equip.idEquip = '".$idEquip."' ";
		$this->db->query($sql);
		//echo"entra Correctament en Divisio</br>";
	}

	public function setColorSamarreta($colorSamarreta,$idEquip){
		
		$sql = "UPDATE equip SET colorSamarreta='".$colorSamarreta."' WHERE equip.idEquip = '".$idEquip."' ";
		$this->db->query($sql);
		//echo"entra Correctament en Samarreta</br>";
	}

	public function setColorPantalo($colorPantalo,$idEquip){
		
		$sql = "UPDATE equip SET colorPantalo='".$colorPantalo."' WHERE equip.idEquip = '".$idEquip."' ";
		$this->db->query($sql);
		//echo"entra Correctament en Pantalo</br>";
	}

	public function setDadesEquip($divisio, $samarreta, $pantalo, $idEquip){

		$sql = "UPDATE equip SET idDivisio='".($divisio-1)."', colorSamarreta='".$samarreta."' , colorPantalo='".$pantalo."' WHERE equip.idEquip = '".$idEquip."' ";
		$this->db->query($sql);
	}

	public function setCapitans($primer, $segon, $idEquip){
		$sql = "UPDATE jugador set isPrimerCapita = 0 where idEquip = '".$idEquip."' ";
		$this->db->query($sql);
		
		$sql2 = "UPDATE jugador set isSegonCapita = 0 where idEquip = '".$idEquip."' ";
		$this->db->query($sql2);
		
		$sql3 = "UPDATE jugador set isPrimerCapita = 1 where idEquip = '".$idEquip."' and idJugador = '".$primer."' ";
		$this->db->query($sql3);
		
		$sql4 = "UPDATE jugador set isSegonCapita = 1 where idEquip = '".$idEquip."' and idJugador = '".$segon."' ";
		$this->db->query($sql4);

	}

	public function equipAmbPistaJoc($idEquip){
		$sql = "SELECT count(*) as num FROM pistaJoc 
				inner join equip on equip.idEquip = pistaJoc.idEquip 
				WHERE pistaJoc.idEquip = '".$idEquip."' and equip.actiu = 'ACT' ";
		$result = $this->db->query($sql)->row();
		if ($result->num > 0){
			$ret = true;
		}else{
			$ret = false;
		}
		//echo "numero de pistas".$ret;
		return $ret;
	}

	public function insertDadesPistaJoc($idEquip, $nomPista,$horariPista,$poblacioPista,$direccioPista) {
		if ($this->equipAmbPistaJoc($idEquip)) {
			$sql = "UPDATE pistaJoc SET nomPista='".$nomPista."' WHERE pistaJoc.idEquip = '".$idEquip."' ";
			$this->db->query($sql);
			$sql = "UPDATE pistaJoc SET horariPista='".$horariPista."' WHERE pistaJoc.idEquip = '".$idEquip."' ";
			$this->db->query($sql);
			$sql = "UPDATE pistaJoc SET direccioPista='".$direccioPista."' WHERE pistaJoc.idEquip = '".$idEquip."' ";
			$this->db->query($sql);
			$sql = "UPDATE pistaJoc SET poblacioPista='".$poblacioPista."' WHERE pistaJoc.idEquip = '".$idEquip."' ";
			$this->db->query($sql);
		} else {
			$sql = "INSERT INTO pistaJoc(nomPista,horariPista,poblacioPista, direccioPista, idEquip) VALUES ('".$nomPista."','".$horariPista."', '".$poblacioPista."', '".$direccioPista."','".$idEquip."') ";
			$this->db->query($sql);
		}

			
	}

	public function getDadesRebut($idEquip){
		$inscripcio = '';$esportivitat = 'NO';$pistaPropia = 'NO';$descompteEquipNou = 'NO';
		$idDivisio = $this->getIdDivsiobyIdEquip($idEquip);
		$pistaJoc = $this->getIdPistaJocbyIdEquip($idEquip);
		$pref = $this->getPreferenciaByIdEquip($idEquip);
		if ($idDivisio->idDivisio == 1 || $idDivisio->idDivisio == 2 || $idDivisio->idDivisio == 5){
			$inscripcio = 998;
		}elseif ($idDivisio->idDivisio == 3 || $idDivisio->idDivisio == 4){
			if (is_object($pistaJoc)){
				$inscripcio = 720;
				$pistaPropia = 'SI';
			}else{
				$inscripcio = 850;
			}
		}else{
			$inscripcio = 0;
		}


		if ($idEquip == 34 || $idEquip == 98 || $idEquip == 32 || $idEquip == 51){
			$esportivitat = 'SI (25%)';
			$inscripcio -= ($inscripcio * 0.25);
		}elseif ($idEquip == 105 || $idEquip == 30){
			$esportivitat = 'SI (12.5%)';
			$inscripcio -= ($inscripcio * 0.125);
		}


		if ($pref->preferenciaPartit != NULL || $pref->preferenciaPartit != ''){
		     $preferencia = 'SI (70€)';
		     $inscripcio += 70;
		}
		if($idEquip == 55 ) {
			$descompteEquipNou = 'SI(-90€)';
			$inscripcio -= 90;
		}
		/*if($idEquip == 39) {
			$descompteEquipNou = 'SI(-180€)';
			$inscripcio -= 180;
		}*/
		$dades = array();
		//$idUser = mysql_fetch_row ( mysql_query ( 'select usuari.idUser from usuari where usuari.nomUser ="' . ($user) . '" ' ) );
		$sql = 'select equip.idEquip, equip.nomEquip, count(jugador.idJugador) as Jugadors, (count(jugador.idJugador)*55) as totalFitxes, ((count(jugador.idJugador)*55)+'.$inscripcio.') as totalPagar, equip.isPagat from equip 
		inner join divisio on divisio.idDivisio = equip.idDivisio 
		inner join jugador on jugador.idEquip = equip.idEquip 
		WHERE divisio.idDivisio = "' . $idDivisio->idDivisio. '" and equip.idEquip = "' . $idEquip. '" and equip.actiu = "ACT"  and jugador.actiu = "ACT" ';
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
		$consulta['descompte'] = $descompteEquipNou;
		return $consulta;
	}

	public function getJugadorsExportExcel() {
		$sql = 'select jugador.idJugador as idJugador, jugador.nomJugador as nom, jugador.cognomsJugador as cognoms, jugador.dni as dni, jugador.dataNeixament as dataNeixament, equip.nomEquip as nomEquip from jugador
		inner join equip on jugador.idEquip = equip.idEquip 
		WHERE equip.actiu = "ACT"  and jugador.actiu = "ACT" ';
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
		return $consulta;
	}

	public function getEquipsPerDivisioExportExcel($idDivisio){
		$sql = 'SELECT equip.idEquip, equip.nomEquip, jugador.nomJugador, jugador.cognomsJugador, jugador.email, jugador.telefon, equip.preferenciaPartit, pistaJoc.direccioPista,pistaJoc.horariPista,pistaJoc.poblacioPista, equip.observacions from equip 
		inner join divisio on divisio.idDivisio = equip.idDivisio 
		left join jugador on jugador.idEquip = equip.idEquip 
		left join pistaJoc on pistaJoc.idEquip = equip.idEquip
		WHERE divisio.idDivisio = "' . $idDivisio . '" and equip.actiu = "ACT" and jugador.isPrimerCapita = 1 group by equip.nomEquip order by equip.nomEquip ASC';
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
		return $consulta;
	}

	public function addJugadorsTemporadaAnterior($id) {
		$data = date('Y-m-d H:i:s');
		$sql = "UPDATE jugador SET actiu = 'ACT' WHERE jugador.idJugador = '".$id."' ";
		$this->db->query($sql);
		$sql2 = "UPDATE jugador SET dataIntroduccio = '".$data."' WHERE jugador.idJugador = '".$id."' ";
		$this->db->query($sql2);
	}
	
	public function activateEquipTemporadaAnterior($id) {
		$data = date('Y-m-d H:i:s');
		$sql = "UPDATE equip SET actiu = 'ACT' WHERE equip.idEquip = '".$id."' ";
		$this->db->query($sql);
		$sql2 = "UPDATE usuari SET actiu = 'ACT' WHERE usuari.idEquip = '".$id."' ";
		$this->db->query($sql2);

	}

	public function addObservacio($idEquip,$observacio) {
		$sql = "UPDATE equip set observacions = '".$observacio."' where idEquip =  '".$idEquip."' ";
		$this->db->query($sql);
	}	

	public function getLastJugadors(){
		$sql = "SELECT jugador.idJugador, jugador.nomJugador, jugador.cognomsJugador, jugador.dni, jugador.email, jugador.telefon, jugador.dataIntroduccio, equip.nomEquip, equip.idEquip
				from jugador 
				inner join equip on equip.idEquip = jugador.idEquip
				where jugador.actiu = 'ACT' 
				order by jugador.dataIntroduccio desc limit 50";

		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getEsportivitat(){
		$sql = "SELECT esportivitat.idEsportivitat as idEsportivitat , esportivitat.idJugador as idJugador, jugador.nomJugador as nom, jugador.cognomsJugador as cognoms, esportivitat.idEquip as idEquip, equip.nomEquip, 
				divisio.nomDivisio, sum(esportivitat.tecniques) as tecniques, 
				sum(esportivitat.antiesportives) as antiEsportives, sum(esportivitat.desqualificant) as desqualificant, sum(esportivitat.banqueta) as banqueta
				from esportivitat
				inner join jugador on jugador.idJugador = esportivitat.idJugador
				inner join equip on equip.idEquip = esportivitat.idEquip
				inner join divisio on divisio.idDivisio = equip.idDivisio
				inner join temporada on temporada.idTemporada = esportivitat.idTemporada
				where esportivitat.estat = 'ACT' and jugador.actiu = 'ACT' and equip.actiu = 'ACT' and temporada.idTemporada = 2 group by esportivitat.idJugador ";
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getEsportivitatPerEquips(){
		$sql = "SELECT  esportivitat.idEquip as idEquip, equip.nomEquip, 
				divisio.nomDivisio, sum(esportivitat.tecniques) as tecniques, 
				sum(esportivitat.antiesportives) as antiEsportives, sum(esportivitat.desqualificant) as desqualificant, sum(esportivitat.banqueta) as banqueta,
				sum(esportivitat.tecniques+esportivitat.antiesportives+esportivitat.desqualificant + esportivitat.banqueta) as total
				from esportivitat
				left join jugador on jugador.idJugador = esportivitat.idJugador
				inner join equip on equip.idEquip = esportivitat.idEquip
				inner join divisio on divisio.idDivisio = equip.idDivisio
				inner join temporada on temporada.idTemporada = esportivitat.idTemporada
				where esportivitat.estat = 'ACT' and equip.actiu = 'ACT' and temporada.idTemporada = 2 group by esportivitat.idEquip ";
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getEsportivitatByIdJugador($idJugador){
		$sql = "SELECT esportivitat.idEsportivitat as idEsportivitat , esportivitat.idJugador as idJugador, jugador.nomJugador as nom, jugador.cognomsJugador as cognoms, esportivitat.idEquip as idEquip, equip.nomEquip, 
				divisio.nomDivisio, sum(esportivitat.tecniques) as tecniques, 
				sum(esportivitat.antiesportives) as antiEsportives, sum(esportivitat.desqualificant) as desqualificant
				from esportivitat
				inner join jugador on jugador.idJugador = esportivitat.idJugador
				inner join equip on equip.idEquip = esportivitat.idEquip
				inner join divisio on divisio.idDivisio = equip.idDivisio
				where esportivitat.estat = 'ACT' and jugador.idJugador = '".$idJugador."' and equip.actiu = 'ACT' ";
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function getEsportivitatPerDates($idJugador) {
		$sql = "SELECT esportivitat.idEsportivitat as idEsportivitat , esportivitat.idJugador as idJugador, jugador.nomJugador as nom, jugador.cognomsJugador as cognoms, esportivitat.idEquip as idEquip, equip.nomEquip, 
				divisio.nomDivisio, esportivitat.tecniques as tecniques, esportivitat.antiesportives as antiEsportives, esportivitat.desqualificant as desqualificant, esportivitat.dataPartit
				from esportivitat
				inner join jugador on jugador.idJugador = esportivitat.idJugador
				inner join equip on equip.idEquip = esportivitat.idEquip
				inner join divisio on divisio.idDivisio = equip.idDivisio
				where esportivitat.estat = 'ACT' and jugador.idJugador = '".$idJugador."' and equip.actiu = 'ACT' ";
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}

	}

	public function setSancio($idEquip,$idJugador,$data,$sancio,$quantitat){
		$tecniques = 0;
		$antiesportives = 0;
		$desqualificant = 0;
		$banqueta = 0;
		switch ($sancio) {
			case '1':
				$tecniques = $quantitat; 
				break;
			case '2':
				$antiesportives = $quantitat;
				break;
			case '3':
				$desqualificant = $quantitat;
				break;
			case 'banqueta':
				$banqueta = $quantitat;
				break;
		}
		$sql = "INSERT INTO esportivitat(idJugador,idEquip,dataPartit,estat,tecniques,antiesportives,desqualificant, banqueta, idTemporada) 
				VALUES ('" .$idJugador. "','".$idEquip."','".$data."' ,'ACT', '".$tecniques."','".$antiesportives."','".$desqualificant."','".$banqueta."', '2')";
		$this->db->query($sql);
	}

	public function setTecnicaBanqueta($tecniques, $data, $idEquip) {
		$sql = "INSERT INTO tecniquesBanqueta(tecniques,dataPartit,idEquip, idTemporada) 
				VALUES ('" .$tecniques. "','".$data."','".$idEquip."' , '2' )";
		$this->db->query($sql);	
	}

	public function getInfoJugador($idJugador) {
		$sql = "SELECT jugador.idJugador, jugador.nomJugador, jugador.cognomsJugador, jugador.dni, jugador.dataNeixament, jugador.poblacio, jugador.email, jugador.telefon, jugador.foto, jugador.dorsal,jugador.idEquip, 
				jugador.observacionsEsportivitat, equip.nomEquip, divisio.nomDivisio FROM `jugador`
				inner join equip on equip.idEquip = jugador.idEquip
				inner join divisio on divisio.idDivisio = equip.idDivisio
				where jugador.idJugador = '".$idJugador."' ";
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function setObservacionsEsportivitat($idJugador, $observacions) {
		$sql ="UPDATE jugador set observacionsEsportivitat = '".$observacions."' WHERE idJugador = '".$idJugador."' ";

		$this->db->query($sql);
	}

	public function eliminarSancio($idEsportivitat, $idJugador) {
		$sql = "DELETE FROM esportivitat WHERE idJugador = '".$idJugador."' and idEsportivitat = '".$idEsportivitat."' ";

		$this->db->query($sql);
	}

	public function obtenerVotosJugadores($idDivisio){
		
		$sql = "SELECT votacio.idJugador, votacio.votos, concat(jugador.nomJugador,' ',jugador.cognomsJugador) as jugador , equip.nomEquip
				from votacio
				inner join jugador on jugador.idJugador = votacio.idJugador
				inner join equip on equip.idEquip = jugador.idEquip
				WHERE jugador.actiu = 'ACT' and votacio.actiu = 'ACT'  and votacio.idDivisio = ".$idDivisio." order by votacio.votos DESC " ;
		
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
		
	}

	public function checkIfJugadorHasVotacio($idJugador){
	
		$sql = 'SELECT * from votacio where actiu= "ACT" and idJugador = '.$idJugador ;
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}

	}

	public function getAllVotacioByIdEquip($idEquip){
		
		$sql = 'SELECT * from votacioEquips	WHERE idEquip =  '.$idEquip;
		$consulta = $this->db->query($sql);
		if($consulta->num_rows() > 0){
			return $consulta->result();
		} else {
			return null;
		}
	}

	public function updateVotacioJugador($idJugador, $votos){
		
		$sql = "UPDATE votacio SET votos='".$votos."' WHERE idJugador = '".$idJugador."' ";
		$this->db->query($sql);	
	}

	public function createVotacioJugador($idJugador, $idDivisio){
		
		$sql = "insert into  votacio values('".$idJugador."' , ".$idDivisio." , 1, 'ACT')";
		$this->db->query($sql);
	}

	public function updateVotsCapitaEquip($idEquip, $num, $idJugador,$idDivisio,$idCapita){
		
		$sql = "INSERT INTO votacioEquips(idEquip,idCapita,idDivisio,numEquip,idVotEquip) VALUES ('".$idEquip."','".$idCapita."','".$idDivisio."','".$num."','".$idJugador."' )";
		//$sql = "UPDATE votacioEquips SET numEquip ='".$num."', idVotEquip = '".$idJugador."' WHERE idEquip = '".$idEquip."' ";
		$this->db->query($sql);	
	}

	public function updateVotsCapitaRestaEquip1($idEquip, $num, $idJugador){
		
		$sql = "UPDATE votacioEquips SET numRestaEquips ='".$num."', idVotNotEquip1 = '".$idJugador."' WHERE idEquip = '".$idEquip."' ";
		$this->db->query($sql);	
	}

	public function updatetable(){
		$q = 'SHOW COLUMNS FROM votacioEquips';
		$consulta = $this->db->query($q);
		if($consulta->num_rows() > 0){
			echo "<pre>";print_r($consulta->result());echo "</pre>";
		}
		$sql = "DELETE FROM votacio";
		$this->db->query($sql);

		$sql = "DELETE FROM votacioEquips";
		$this->db->query($sql);

		
		/*$sql= "ALTER TABLE votacioEquips ADD COLUMN idvotNotEquip6 INT(10) AFTER idVotNotEquip5";
		$this->db->query($sql);

		$sql= "ALTER TABLE votacioEquips ADD COLUMN idvotNotEquip7 INT(10) AFTER idVotNotEquip6";
		$this->db->query($sql);

		$sql= "ALTER TABLE votacioEquips ADD COLUMN idvotNotEquip8 INT(10) AFTER idVotNotEquip7";
		$this->db->query($sql);

		$sql= "ALTER TABLE votacioEquips ADD COLUMN idvotNotEquip9 INT(10) AFTER idVotNotEquip8";
		$this->db->query($sql);*/
	}
	public function updateVotsCapitaRestaEquip2($idEquip, $num, $idJugador){
		
		$sql = "UPDATE votacioEquips SET numRestaEquips ='".$num."', idVotNotEquip2 = '".$idJugador."' WHERE idEquip = '".$idEquip."' ";
		$this->db->query($sql);	
	}

	public function updateVotsCapitaRestaEquip3($idEquip, $num, $idJugador){
		
		$sql = "UPDATE votacioEquips SET numRestaEquips ='".$num."', idVotNotEquip3 = '".$idJugador."' WHERE idEquip = '".$idEquip."' ";
		$this->db->query($sql);	
	}

	public function updateVotsCapitaRestaEquip4($idEquip, $num, $idJugador){
		
		$sql = "UPDATE votacioEquips SET numRestaEquips ='".$num."', idVotNotEquip4 = '".$idJugador."' WHERE idEquip = '".$idEquip."' ";
		$this->db->query($sql);	
	}

	public function updateVotsCapitaRestaEquip5($idEquip, $num, $idJugador){
		
		$sql = "UPDATE votacioEquips SET numRestaEquips ='".$num."', idVotNotEquip5 = '".$idJugador."' WHERE idEquip = '".$idEquip."' ";
		$this->db->query($sql);	
	}

	public function updateVotsCapitaRestaEquip6($idEquip, $num, $idJugador){
		
		$sql = "UPDATE votacioEquips SET numRestaEquips ='".$num."', idVotNotEquip6 = '".$idJugador."' WHERE idEquip = '".$idEquip."' ";
		$this->db->query($sql);	
	}

	public function updateVotsCapitaRestaEquip7($idEquip, $num, $idJugador){
		
		$sql = "UPDATE votacioEquips SET numRestaEquips ='".$num."', idVotNotEquip7 = '".$idJugador."' WHERE idEquip = '".$idEquip."' ";
		$this->db->query($sql);	
	}

	public function updateVotsCapitaRestaEquip8($idEquip, $num, $idJugador){
		
		$sql = "UPDATE votacioEquips SET numRestaEquips ='".$num."', idVotNotEquip8 = '".$idJugador."' WHERE idEquip = '".$idEquip."' ";
		$this->db->query($sql);	
	}

	public function updateVotsCapitaRestaEquip9($idEquip, $num, $idJugador){
		
		$sql = "UPDATE votacioEquips SET numRestaEquips ='".$num."', idVotNotEquip9 = '".$idJugador."' WHERE idEquip = '".$idEquip."' ";
		$this->db->query($sql);	
	}

	public function updateDivisio($idEquip,$idDivisio){
		
		$sql = "UPDATE equip SET idDivisio='".$idDivisio."' WHERE equip.idEquip = '".$idEquip."' ";
		$this->db->query($sql);
		//echo"entra Correctament en Divisio</br>";
	}

	public function updateDataNeixament($idJugador, $dataNeixament) {
		$sql ="UPDATE jugador set dataNeixament = '".$dataNeixament."' WHERE idJugador = '".$idJugador."' ";

		$this->db->query($sql);
	}

	public function updateEmail($idJugador, $email) {
		$sql ="UPDATE jugador set email = '".$email."' WHERE idJugador = '".$idJugador."' ";

		$this->db->query($sql);
	}

	public function updateDorsal($idJugador, $dorsal) {
		$sql ="UPDATE jugador set dorsal = '".$dorsal."' WHERE idJugador = '".$idJugador."' ";

		$this->db->query($sql);
	}
}
?>
