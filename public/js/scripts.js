$(document).ready(function() {
		var numRadio = 1;
		//$( ".botons" ).buttonset();
		//$( "#buttons" ).buttonset();
		
		/*$( ".elimina" ).button({
			icons: {
				primary: "ui-icon-closethick"
			},
			text: false
		});*/
		
		$("#delete").click(function() {			
			// eliminem tot el contingut 
			$( "div[id='pagina'] > div[class='cos ui-buttonset']" ).remove();
			
		});
		
		$("#afegir").click(function() {
			//numRadio++;
			var jugInsc = retornaJugadorsInscrits();
			// retorna el valor del select
			var numCols = retorna();
			numRadio = parseInt(numRadio) + parseInt(jugInsc);
			for (i = 1; i <= numCols; i++){
				if (numRadio == 1){
					$( '#pagina' ).append("<div class='cos'>"+
							"<fieldset class'jug'><legend class='red'>Jugador "+numRadio+" PRIMER CAPITA</legend>"+
							"<label for='dni'>DNI:</label><input title=' 11111111H(lletra majuscula)' pattern='[0-9]{8}[A-Z]{1}' required='yes' type='text' size=9 maxlength='9' name='dni"+numRadio+"' id='dni"+numRadio+"' placeholder='11111111H'>"+
							"<label for='nom'>Nom:</label><input required='yes' type='text' name='nom"+numRadio+"' id='nom"+numRadio+"' >"+
							"<label for='cognoms'>Cognoms:</label><input required='yes' type='text' name='cognoms"+numRadio+"' id='cognoms"+numRadio+"' >"+
							"<label for='email'>Email:</label><input required='yes' type='email' name='email"+numRadio+"' id='email"+numRadio+"' >"+
							"<label for='telefon'>Telefon:</label><input title=' 666666666' required='yes' type='text' size=9 maxlength='9' pattern='[0-9]{9}' name='telefon"+numRadio+"' id='telefon"+numRadio+"' >"+
							"<label for='primerCapita'></label><input type='hidden' name='primer"+numRadio+"' id='primer"+numRadio+"' value='1' >"+
							"<label for='segonCapita'></label><input type='hidden' name='segon"+numRadio+"' id='segon"+numRadio+"' value='0' >"+
							"<label for='dorsal'>Dorsal:</label><input type='text' size=2 maxlength='2' name='dorsal"+numRadio+"' id='dorsal"+numRadio+"' >"+
							"<label for='isEntrenador'>Marca per constar com entrenador   <input class='checkbox' style='display:inline;' type='checkbox' name='entrenador"+numRadio+"' id='entrenador"+numRadio+"' value='1'></label></br>"+
							"<label for='dataNeixament'>DataNeixament:</label><input title='dd/mm/yyyy' maxlength='10' required='yes' size=11 type='text' pattern='[0-9]{2}/[0-9]{2}/[0-9]{4}' name='dataNeixament"+numRadio+"' id='dataNeixament"+numRadio+"' placeholder='dd/mm/yyyy'></br>"+
							"<label for='poblacio'>Poblacio:</label><input required='yes' type='text' name='poblacio"+numRadio+"' id='poblacio"+numRadio+"'></br>"+
							"<label for='fotoCarnet'>Foto</label><input required='yes' type='file' name='foto"+numRadio+"' id='foto"+numRadio+"'>"+
							"<label for='fotoDni'>Adjuntar DNI</label><input required='yes' type='file' name='fotoDni"+numRadio+"' id='fotoDni"+numRadio+"'>"+
							"<input type='hidden' name='numJug' id='numJug' value='"+numCols+"'>"+
							"<input type='hidden' name='numJugInsc' id='numJugInsc' value='"+jugInsc+"'>"+
							/*"<div class='col1_1' >"+
								"columna"+numRadio+
							"</div>"+*/
							/*"<div class='botons'>"+
								"<button class='elimina'>Elimina Fila!</button>"+
							"</div>"+*/
						"</fieldset></div>");
				}
				if (numRadio == 2){
					$( '#pagina' ).append("<div class='cos'>"+
							"<fieldset class'jug'><legend class='red'>Jugador "+numRadio+" SEGON CAPITA</legend>"+
							"<label for='dni'>DNI:</label><input title=' 11111111H(lletra majuscula)' pattern='[0-9]{8}[A-Z]{1}' required='yes' type='text' size=9 maxlength='9' name='dni"+numRadio+"' id='dni"+numRadio+"' placeholder='11111111H'>"+
							"<label for='nom'>Nom:</label><input required='yes' type='text' name='nom"+numRadio+"' id='nom"+numRadio+"' >"+
							"<label for='cognoms'>Cognoms:</label><input required='yes' type='text' name='cognoms"+numRadio+"' id='cognoms"+numRadio+"' >"+
							"<label for='email'>Email:</label><input required='yes' type='email' name='email"+numRadio+"' id='email"+numRadio+"' >"+
							"<label for='telefon'>Telefon:</label><input title=' 666666666' required='yes' type='text' size=9 maxlength='9' pattern='[0-9]{9}' name='telefon"+numRadio+"' id='telefon"+numRadio+"' >"+
							"<label for='primerCapita'></label><input type='hidden' name='primer"+numRadio+"' id='primer"+numRadio+"' value='0' >"+
							"<label for='segonCapita'></label><input type='hidden' name='segon"+numRadio+"' id='segon"+numRadio+"' value='1' >"+
							"<label for='dorsal'>Dorsal:</label><input type='text' size=2 maxlength='2' name='dorsal"+numRadio+"' id='dorsal"+numRadio+"' >"+
							"<label for='isEntrenador'>Marca per constar com entrenador   <input class='checkbox' style='display:inline;' type='checkbox' name='entrenador"+numRadio+"' id='entrenador"+numRadio+"' value='1'></label></br>"+
							"<label for='dataNeixament'>DataNeixament:</label><input title='dd/mm/yyyy' maxlength='10' required='yes' size=11 type='text' pattern='[0-9]{2}/[0-9]{2}/[0-9]{4}' name='dataNeixament"+numRadio+"' id='dataNeixament"+numRadio+"' placeholder='dd/mm/yyyy'></br>"+
							"<label for='poblacio'>Poblacio:</label><input required='yes' type='text' name='poblacio"+numRadio+"' id='poblacio"+numRadio+"'></br>"+
							"<label for='fotoCarnet'>Foto</label><input required='yes' type='file' name='foto"+numRadio+"' id='foto"+numRadio+"'>"+
							"<label for='fotoDni'>Adjuntar DNI</label><input required='yes' type='file' name='fotoDni"+numRadio+"' id='fotoDni"+numRadio+"'>"+
							"<input type='hidden' name='numJug' id='numJug' value='"+numCols+"'>"+
							"<input type='hidden' name='numJugInsc' id='numJugInsc' value='"+jugInsc+"'>"+
							/*"<div class='col1_1' >"+
								"columna"+numRadio+
							"</div>"+*/
							/*"<div class='botons'>"+
								"<button class='elimina'>Elimina Fila!</button>"+
							"</div>"+*/
						"</fieldset></div>");
				}
				if (numRadio >= 3){
			//alert(jugInsc);
				$( '#pagina' ).append("<div class='cos'>"+
										"<fieldset class'jug'><legend class='red'>Jugador "+numRadio+"</legend>"+
										"<label for='dni'>DNI:</label><input title=' 11111111H(lletra majuscula)' pattern='[0-9]{8}[A-Z]{1}' required='yes' type='text' size=9 maxlength='9' name='dni"+numRadio+"' id='dni"+numRadio+"' placeholder='11111111H'>"+
										"<label for='nom'>Nom:</label><input required='yes' type='text' name='nom"+numRadio+"' id='nom"+numRadio+"' >"+
										"<label for='cognoms'>Cognoms:</label><input required='yes' type='text' name='cognoms"+numRadio+"' id='cognoms"+numRadio+"' >"+
										"<label for='email'></label><input type='hidden' name='email"+numRadio+"' id='email"+numRadio+"' >"+
										"<label for='telefon'></label><input type='hidden' name='telefon"+numRadio+"' id='telefon"+numRadio+"'>"+
										"<label for='primerCapita'></label><input type='hidden' name='primer"+numRadio+"' id='primer"+numRadio+"' value='0' >"+
										"<label for='segonCapita'></label><input type='hidden' name='segon"+numRadio+"' id='segon"+numRadio+"' value='0' >"+
										"<label for='dorsal'>Dorsal:</label><input type='text' size=2 maxlength='2' name='dorsal"+numRadio+"' id='dorsal"+numRadio+"' >"+
									"<label for='isEntrenador'>Marca per constar com entrenador   <input class='checkbox' style='display:inline;' type='checkbox' name='entrenador"+numRadio+"' id='entrenador"+numRadio+"' value='1'></label></br>"+
										"<label for='dataNeixament'>DataNeixament:</label><input title='dd/mm/yyyy' maxlength='10' required='yes' size=11 type='text' pattern='[0-9]{2}/[0-9]{2}/[0-9]{4}' name='dataNeixament"+numRadio+"' id='dataNeixament"+numRadio+"' placeholder='dd/mm/yyyy'>"+
								"<label for='poblacio'>Poblacio:</label><input required='yes' type='text' name='poblacio"+numRadio+"' id='poblacio"+numRadio+"'></br>"+
								"<label for='fotoCarnet'>Foto</label><input required='yes' type='file' name='foto"+numRadio+"' id='foto"+numRadio+"'>"+
								"<label for='fotoDni'>Adjuntar DNI</label><input required='yes' type='file' name='fotoDni"+numRadio+"' id='fotoDni"+numRadio+"'>"+
								"<input type='hidden' name='numJug' id='numJug' value='"+numCols+"'>"+
								"<input type='hidden' name='numJugInsc' id='numJugInsc' value='"+jugInsc+"'>"+
								/*"<div class='col1_1' >"+
									"columna"+numRadio+
										"</div>"+*/
										/*"<div class='botons'>"+
											"<button class='elimina'>Elimina Fila!</button>"+
										"</div>"+*/
									"</fieldset></div>");
											
				}
				$( ".cos" ).buttonset();
				numRadio++;
			}			
		    // controlem el nº de columnes a afegir
			/*if(numCols == 1) {				
				$( '#pagina' ).append("<div class='cos'><div class='col1_1' >columna</div><div class='limpio'></div><div class='botons'><button class='elimina'>Elimina Fila!</button><input onclick='canvia(this)' type='radio' id='radio1_"+numRadio+"' name='botons"+numRadio+"' checked='checked'><label for='radio1_"+numRadio+"'>1</label><input type='radio' id='radio2_"+numRadio+"' name='botons"+numRadio+"'><label for='radio2_"+numRadio+"'>2</label><input type='radio' id='radio3_"+numRadio+"' name='botons"+numRadio+"'><label for='radio3_"+numRadio+"'>3</label></div></div>");
				
				$( ".cos" ).buttonset();
				
			} else if(numCols == 2) {
				$( '#pagina' ).append("<div class='cos'><div class='col1_2' >columna</div><div class='col2_2' >columna</div><div class='limpio'></div><div class='botons'><button class='elimina'>Elimina Fila!</button><input type='radio' id='radio1_"+numRadio+"' name='botons"+numRadio+"'><label for='radio1_"+numRadio+"'>1</label><input type='radio' id='radio2_"+numRadio+"' name='botons"+numRadio+"' checked='checked'><label for='radio2_"+numRadio+"'>2</label><input type='radio' id='radio3_"+numRadio+"' name='botons"+numRadio+"'><label for='radio3_"+numRadio+"'>3</label></div></div>");
				$( ".cos" ).buttonset();

			} else if(numCols == 3) {
				$( '#pagina' ).append("<div class='cos'><div class='col1_3' >columna</div><div class='col2_3' >columna</div><div class='col3_3' >columna</div><div class='limpio'></div><div class='botons'><button class='elimina'>Elimina Fila!</button><input type='radio' id='radio1_"+numRadio+"' name='botons"+numRadio+"' checked='checked'><label for='radio1_"+numRadio+"'>1</label><input type='radio' id='radio2_"+numRadio+"' name='botons"+numRadio+"'><label for='radio2_"+numRadio+"'>2</label><input type='radio' id='radio3_"+numRadio+"' name='botons"+numRadio+"' checked='checked'><label for='radio3_"+numRadio+"'>3</label></div></div>");
				$( ".cos" ).buttonset();
			}*/
			
			$( ".elimina" ).click(function() {
				$( this.parentNode.parentNode ).remove();
			});
			
			$( ".elimina" ).button({
				icons: {
					primary: "ui-icon-closethick"
					},
				text: false
			});
			
			// id que tinguin radio1_
			$( "input[id*='radio1_']" ).click(function() {
				var div = this.parentNode.parentNode;
				// li donem la classe col1_1 (1 columna)				
				$ ( div.firstChild ).attr('class', 'col1_1');
				$ ( div.firstChild ).css("display", "block");
				// assignem a format el valor del següent div
				var format = $(div.firstChild).next();
				// si trobem el div amb classe limpio, ja hem recorregut tot el div
				while (format.attr('class') != 'limpio'){
					format.slideUp();
					format = format.next();
				}

			});
			
			// id que tinguin radio2_
			$( "input[id*='radio2_']" ).click(function() {
	
				var div = this.parentNode.parentNode;
				$ ( div.firstChild ).attr('class', 'col1_2');
				$ ( div.firstChild ).css("display", "block");
				
				var format = $(div.firstChild).next();
				
				
				if ( format.attr('class') == 'limpio' ){
					$ (format).before( "<div class='col2_2' >columna</div>" );					
				}
				$ (format).css("display", "block");
				
				// mirem com esta definida la següent columna, si és de 3 la passem a 2
				if (format.attr('class') == 'col2_3'){
					// La posem a classe de 2 columnes
					$ (format).attr('class', 'col2_2');
					// I amaguem la següent
					$ (format.next()).slideUp();
				}
				
			});
			
			// id que tinguin radio3_
			$( "input[id*='radio3_']" ).click(function() {
				
				var div = this.parentNode.parentNode;
				// primer posem la columna classe col1_3
				$ ( div.firstChild ).attr('class', 'col1_3');
				$ ( div.firstChild ).css("display", "block");
				var format = $(div.firstChild).next();
				// si s'ha acabat el div, llavors abans del div limpio creem una columna classe col2_3
				// i si no és el div limpio apliquem la classe col2_3
				if ( format.attr('class') == 'limpio' ){
					$ (format).before( "<div class='col2_3' >columna</div>" );					
				} else {
					$ (format).attr('class', 'col2_3');
				}
				$ (format).css("display", "block");
				// fem el mateix que el if anterior pero per la classe col3_3
				if ( format.next().attr('class') == 'limpio' ){
					$ (format.next()).before( "<div class='col3_3' >columna</div>" );	
				}
				format.next().css("display", "block");
				
			});
			
		});
		
		$( ".elimina" ).click(function() {			
			$( this.parentNode.parentNode ).remove();			
		});
		// botó mostra codi
		$( "#codi" ).click(function() {
			$( "#mostraCodi" ).dialog();
			var html = $( "html" ).html();
			$( "#mostraCodi" ).text(html);
		});
		
});
	
	// per saber el nº columnes seleccionades
function retorna(){
	var numCol = document.getElementById("columnes");
	var opcio = columnes.options[numCol.selectedIndex].value;
	return opcio;
}
function retornaJugadorsInscrits(){
	var numCol = document.getElementById("jugadorsInscrits").value;
	//var opcio = jugadorsInscrits.value;
	return numCol;
}